for dataset in "2004" "2005"; do
    for overlapCount in "0" "10" "20" "30" "40"; do
        mkdir "multigraph_model_ace${year}.${overlapCount}_overlap"
        expDir="multigraph_model_ace${year}.${overlapCount}_overlap" algo="MULTIGRAPH" useSpecificIndicator="false" trainPath="data/ACE${year}/train.${overlapCount}_overlap.data" devPath="data/ACE${year}/dev.data" testPath="data/ACE${year}/dev.data" l2Opts="0.0 0.001 0.01 0.1 1.0" features="words,word_ngram,pos_tag,pos_tag_ngram,wordtype,wordtype_ngram,bow,orthographic,mention_penalty" halfWindowSize="3" ngramSize="4" halfBowSize="5" time bash run_multiple.bash
    done
done

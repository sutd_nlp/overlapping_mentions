# 01 Feb 2017
# To calculate the overlapping entities accuracies for ACE and GENIA
if [ -z ${noreg+x} ]; then
    noreg=""
else
    noreg="-noreg"
fi

echo "ACE2004"
echo "Mention Hypergraph"
python count_accuracy_overlapping.py ../data/ACE2004/test.data orig_model_ace2004${noreg}.result/data_ACE2004_testdata.result
echo
echo "Multigraph Model"
python count_accuracy_overlapping.py ../data/ACE2004/test.data collapsed_model_ace2004${noreg}.result/data_ACE2004_testdata.result

echo
echo "==="
echo "ACE2005"
echo "Mention Hypergraph"
python count_accuracy_overlapping.py ../data/ACE2005/test.data orig_model_ace2005${noreg}.result/data_ACE2005_testdata.result
echo
echo "Multigraph Model"
python count_accuracy_overlapping.py ../data/ACE2005/test.data collapsed_model_ace2005${noreg}.result/data_ACE2005_testdata.result

echo
echo "==="
echo "GENIA"
echo "Mention Hypergraph"
python count_accuracy_overlapping.py ../data/GENIA/test.data orig_model_genia${noreg}.result/data_GENIA_testdata.result
echo
echo "Multigraph Model"
python count_accuracy_overlapping.py ../data/GENIA/test.data collapsed_model_genia${noreg}.result/data_GENIA_testdata.result

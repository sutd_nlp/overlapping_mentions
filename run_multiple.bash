trap exit SIGINT
if [ -z ${expDir+x} ]; then
    expDir="multigraph_model"
fi
if [ -z ${l2Opts+x} ]; then
    l2Opts=(0.0 0.001 0.01 0.1 1.0)
else
    l2Opts=(${l2Opts})
fi
if [ -z ${trainPath+x} ]; then
    echo "Please specify training data path with trainPath=<path> before the command"
    exit 1
fi
if [ -z ${devPath+x} ]; then
    echo "Please specify development path with devPath=<path> before the command"
    exit 1
fi
if [ -z ${testPath+x} ]; then
    echo "Please specify test data path with testPath=<path> before the command"
    exit 1
fi
if [ -z ${algo+x} ]; then
    echo "Please specify the algorithm: COLLAPSED_SHARED or HEAD_SHARED"
    exit 1
fi
if [ -z ${hasPOS+x} ]; then
    hasPOS="true"
fi
if [ -z ${halfWindowSize+x} ]; then
    halfWindowSize="2"
fi
if [ -z ${ngramSize+x} ]; then
    ngramSize="4"
fi
if [ -z ${halfBowSize+x} ]; then
    halfBowSize="5"
fi
if [ -z ${useSpecificIndicator+x} ]; then
    useSpecificIndicator="true"
fi
if [ -z ${brownPathOpts+x} ]; then
    brownPathOpts=("")
else
    brownPathOpts=(${brownPathOpts})
fi
if [ -z ${maxIter+x} ]; then
    maxIter="2000"
fi
if [ -z ${useBILOU+x} ]; then
    useBILOU="false"
fi
if [ -z ${useSSVMOpts+x} ]; then
    useSSVMOpts=(false)
else
    useSSVMOpts=(${useSSVMOpts})
fi
if [ -z ${ignoreOverlaps+x} ]; then
    ignoreOverlaps="false"
fi
for l2 in ${l2Opts[@]}; do
    for brownPath in "${brownPathOpts[@]}"; do
        for useSSVM in "${useSSVMOpts[@]}"; do
            if [ -z ${features+x} ]; then
                if [ "${brownPath}" == "" ]; then
                    #echo "Case 1"
                    expDir="${expDir}" useSpecificIndicator="${useSpecificIndicator}" algo="${algo}" trainPath="${trainPath}" devPath="${devPath}" testPath="${testPath}" l2="${l2}" hasPOS="${hasPOS}" halfWindowSize="${halfWindowSize}" ngramSize="${ngramSize}" halfBowSize="${halfBowSize}" maxIter="${maxIter}" useBILOU="${useBILOU}" useSSVM="${useSSVM}" ignoreOverlaps="${ignoreOverlaps}" time bash run_mention_experiment.bash "$@"
                else
                    #echo "Case 2"
                    expDir="${expDir}" useSpecificIndicator="${useSpecificIndicator}" algo="${algo}" trainPath="${trainPath}" devPath="${devPath}" testPath="${testPath}" brownPath=${brownPath} l2="${l2}" hasPOS="${hasPOS}" halfWindowSize="${halfWindowSize}" ngramSize="${ngramSize}" halfBowSize="${halfBowSize}" maxIter="${maxIter}" useBILOU="${useBILOU}" useSSVM="${useSSVM}" ignoreOverlaps="${ignoreOverlaps}" time bash run_mention_experiment.bash "$@"
                fi
            else
                if [ "${brownPath}" == "" ]; then
                    #echo "Case 3"
                    expDir="${expDir}" useSpecificIndicator="${useSpecificIndicator}" algo="${algo}" trainPath="${trainPath}" devPath="${devPath}" testPath="${testPath}" l2="${l2}" hasPOS="${hasPOS}" features="${features}" halfWindowSize="${halfWindowSize}" ngramSize="${ngramSize}" halfBowSize="${halfBowSize}" maxIter="${maxIter}" useBILOU="${useBILOU}" useSSVM="${useSSVM}" ignoreOverlaps="${ignoreOverlaps}" time bash run_mention_experiment.bash "$@"
                else
                    #echo "Case 4"
                    expDir="${expDir}" useSpecificIndicator="${useSpecificIndicator}" algo="${algo}" trainPath="${trainPath}" devPath="${devPath}" testPath="${testPath}" brownPath="${brownPath}" l2="${l2}" hasPOS="${hasPOS}" features="${features}" halfWindowSize="${halfWindowSize}" ngramSize="${ngramSize}" halfBowSize="${halfBowSize}" maxIter="${maxIter}" useBILOU="${useBILOU}" useSSVM="${useSSVM}" ignoreOverlaps="${ignoreOverlaps}" time bash run_mention_experiment.bash "$@"
                fi
            fi
        done
    done
done

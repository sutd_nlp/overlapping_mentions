if [ -z ${outDir+x} ]; then
    outDir="evaluation_result"
fi
echo -n "Linear CRF (single chain)"
expDir="linear_model_conll" outDir="${outDir}" algo="LINEAR_CRF" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
echo
echo -n "Linear CRF (multi chains)"
expDir="paralinear_model_conll.nooverlap" outDir="${outDir}" algo="PARALINEAR" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
echo
echo -n "Tang et al. (2013)"
expDir="paralinear_model_conll.fixed" outDir="${outDir}" algo="PARALINEAR" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
echo
echo -n "Mention Hypergraph"
expDir="hypergraph_model_conll" outDir="${outDir}" algo="MENTION_HYPERGRAPH" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
echo
echo -n "State-based MS"
expDir="multilinear_model_conll" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
echo
echo -n "Edge-based MS"
expDir="multigraph_model_conll" outDir="${outDir}" algo="MULTIGRAPH" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
#echo
#echo -n "Multilinear No Transition"
#expDir="multilinear_notrans_model_conll" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash
#echo
#echo -n "Paralinear"
#expDir="paralinear_model_conll" outDir="${outDir}" algo="PARALINEAR" trainPath="data/CONLL2003/train.data" testPath="data/CONLL2003/test.data" bash get_results.bash

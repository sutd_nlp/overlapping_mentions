# To create the training, development, and test split from GENIA corpus
# The file GENIAcorpus3.02.merged.fixed.xml comes from the GENIA POS annotation
# at http://www.geniaproject.org/genia-corpus/pos-annotation
# The original corpus has been modified to fix some issues on the inconsistent
# "lex" values (e.g., make plurals into singular, remove unnecessary '_', add 
# missing '*', etc.) so that the script would be able to process it correctly.
# NOTE: The resulting folder will be about 124MB in size
# NOTE: It will take around 60s to run the script

folder_name="GENIA_processed"
mkdir "${folder_name}"
python genia_xml_to_inline.py GENIAcorpus3.02.merged.fixed.xml "${folder_name}" > genia_conversion.log
for split in "train" "dev" "test"; do
    cp ${folder_name}/${split}.tok.5types.no_disc.data ${split}.data
done

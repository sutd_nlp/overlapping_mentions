# Make sure to run this in "data" directory
for split in "train" "dev" "test"; do
    java -cp ../target/experiment-overlapping-1.0-SNAPSHOT.jar com.statnlp.experiment.overlapping.OEUtil getStats -sourceDir GENIA/${split}.tok.5types.no_disc.data -annDir . -no_examples -no_sections -instanceFilter all_instances -dataIsRaw false > stats/GENIA_${split}.stat
    #java -cp ../target/experiment-overlapping-1.0-SNAPSHOT.jar com.statnlp.experiment.overlapping.OEUtil getStats -sourceDir GENIA/${split}_withpos_oversplit.data -annDir . -no_examples -no_sections -instanceFilter all_instances -dataIsRaw false > stats/GENIA_${split}_oversplit.stat
    #for data in "ACE2004" "ACE2005" "CONLL2003"; do
    #    java -cp ../target/experiment-overlapping-1.0-SNAPSHOT.jar com.statnlp.experiment.overlapping.OEUtil getStats -sourceDir ${data}/${split}.data -annDir . -no_examples -no_sections -instanceFilter all_instances -dataIsRaw false > stats/${data}_${split}.stat
    #done
done
#for split in "80train" "20train" "train" "dev" "test"; do
#    java -cp ../target/experiment-overlapping-1.0-SNAPSHOT.jar com.statnlp.experiment.overlapping.OEUtil getStats -sourceDir semeval-2014-task-7/data/${split} -annDir semeval-2014-task-7/data/$split.gold -no_examples -no_sections -instanceFilter contain_discontiguous -dataIsRaw true > stats/${split}.stat
#done

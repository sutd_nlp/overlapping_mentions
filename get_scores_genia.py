# -*- coding: utf-8 -*-
"""
09 Jan 2017
Extract scores from .runlog files for GENIA experiments
"""

# Import statements
import sys
import re
from itertools import product

def main():
    outdir = sys.argv[1]
    expdir = sys.argv[2]
    algo = sys.argv[3]
    train_path = sys.argv[4]
    test_path = sys.argv[5]
    l2_opts = sys.argv[6:]
    train_name = train_path[train_path.rfind('/')+1:train_path.find('.')]
    test_name = test_path[test_path.rfind('/')+1:]
    scores_dev = {}
    scores_test = {}
    training_infos = {}
    testing_infos = {}
    for l2, n_cluster in product(l2_opts, ['100', '1000']):
        train_file = '{}/{}-{}-l2-{}-cluster-c{}-sizes_245.runlog'.format(expdir, algo, train_name, l2, n_cluster)
        try:
            l2_cluster = '{}_{}'.format(l2, n_cluster)
            scores_dev[l2_cluster] = {}
            scores_test[l2_cluster] = {}
            run_name = '{}-{}-l2-{}-cluster-c{}-sizes_245-run_on-{}'.format(algo, train_name, l2, n_cluster, test_name)
            logfile_opt = '{}/{}-mp_opt.runlog'.format(expdir, run_name)
            orig_mp = None
            opt_mp = None
            has_opt_mp = True
            try:
                with open(logfile_opt, 'r') as infile:
                    cur_mp = None
                    scores = scores_dev[l2_cluster]
                    has_seen_overall_score_line = False
                    for line in infile:
                        if 'Trying mention penalty ' in line:
                            cur_mp = float(line.split(' ')[-1])
                        if cur_mp is not None and line.startswith('Overall P:') and (scores == scores_dev[l2_cluster] or has_seen_overall_score_line):
                            score = re.split('[ \t]+', re.sub('[,%]', '', line))
                            score = [float(score[2]), float(score[4]), float(score[6])]
                            if scores == scores_dev[l2_cluster]:
                                scores[cur_mp] = score
                            else:
                                scores['opt'] = score
                        if line.startswith('Replacing mention penalty weight'):
                            has_opt_mp = True
                            scores = scores_test[l2_cluster]
                            has_seen_overall_score_line = False
                            orig_mp = float(line.split(' ')[5])
                            opt_mp = float(line.split(' ')[14])
                            scores_dev[l2_cluster]['orig_mp'] = orig_mp
                            scores_dev[l2_cluster]['opt_mp'] = opt_mp
                        if '### Overall score ###' in line:
                            has_seen_overall_score_line = True
            except:
                has_opt_mp = False
            if not has_opt_mp:
                logfile = train_file
                with open(logfile, 'r') as infile:
                    has_seen_overall_score_line = False
                    for line in infile:
                        if '### Overall score ###' in line:
                            has_seen_overall_score_line = True
                        if has_seen_overall_score_line and line.startswith('Overall P:'):
                            score = re.sub('[,%]', '', line).split(' ')
                            scores_dev[l2_cluster]['orig_mp'] = ''
                            scores_dev[l2_cluster][''] = [float(score[2]), float(score[4]), float(score[6])]
            try:
                training_info = []
                decode_times = []
                sent_per_sec = 0.0
                word_per_sec = 0.0
                with open(train_file, 'r') as infile:
                    for line in infile:
                        if line.startswith('Iteration '):
                            tokens = re.split('[ \t:=s]+', line.strip())
                            it_num = int(tokens[1])
                            it_obj = float(tokens[3])
                            it_time = float(tokens[5])
                            it_ratio = float(tokens[6])
                            it_tot_time = float(tokens[9])
                            it_dict = {}
                            it_dict['num'] = it_num
                            it_dict['obj'] = it_obj
                            it_dict['time'] = it_time
                            it_dict['ratio'] = it_ratio
                            it_dict['tot_time'] = it_tot_time
                            training_info.append(it_dict)
                        if line.startswith('Decoding time for thread '):
                            tokens = re.split(' ', line.strip())
                            decode_time = float(tokens[6])
                            decode_times.append(decode_time)
			if line.startswith('Number of sentences processed'):
                            tokens = re.split(' ', line.strip())
                            sent_per_sec = float(tokens[6])
                        if line.startswith('Number of words processed'):
                            tokens = re.split(' ', line.strip())
                            word_per_sec = float(tokens[6])
                if l2_cluster == '0.0_100':
                    for info in ['obj', 'time', 'ratio', 'tot_time']:
                        with open('{}/{}_{}-{}.log'.format(outdir, expdir, algo, info), 'w') as outfile:
                            for it_dict in training_info:
                                outfile.write('{},{}\n'.format(it_dict['num'], it_dict[info]))
                training_infos[l2_cluster] = training_info
                testing_infos[l2_cluster] = (sum(decode_times)/len(decode_times), sent_per_sec, word_per_sec)
            except:
                # raise
                pass
            logfile = '{}/{}.runlog'.format(expdir, run_name)
            with open(logfile, 'r') as infile:
                has_seen_overall_score_line = False
                for line in infile:
                    if '### Overall score ###' in line:
                        has_seen_overall_score_line = True
                    if has_seen_overall_score_line and line.startswith('Overall P:'):
                        score = re.sub('[,%]', '', line).split(' ')
                        scores_test[l2_cluster]['orig'] = [float(score[2]), float(score[4]), float(score[6])]
        except:
            continue
        
    # from pprint import pprint
    # pprint(scores_dev)
    # pprint(scores_test)
    best_dev_orig = 0.0
    best_l2_orig = 0.0
    best_l2_orig_mp = 0.0
    best_dev_opt = 0.0
    best_l2_opt = 0.0
    best_l2_opt_mp = 0.0
    for l2 in scores_dev:
        try:
            orig_mp = scores_dev[l2]['orig_mp']
            if 'opt_mp' in scores_dev[l2]:
                opt_mp = scores_dev[l2]['opt_mp']
            if scores_dev[l2][orig_mp][2] > best_dev_orig:
                best_dev_orig = scores_dev[l2][orig_mp][2]
                best_l2_orig = l2
                best_l2_orig_mp = orig_mp
                try:
                    best_dev_opt = scores_dev[l2][opt_mp][2]
                    best_l2_opt = l2
                    best_l2_opt_mp = opt_mp
                except:
                    pass
        except:
            pass
    print()
    print('Test path: {}'.format(test_path))
    print('{:<4s} {:^10s} {:^14s} | {:^14s}'.format('', '', 'DEV', 'TEST'))
    print('{:<4s} {:^10s} {:^4s} {:^4s} {:^4s} | {:^4s} {:^4s} {:^4s}'.format('', 'L2', 'P', 'R', 'F', 'P', 'R', 'F'))
    try:
        print('{0:<4s} {1:<10s} {2[0]:^4.1f} {2[1]:^4.1f} {2[2]:^4.1f} | {3[0]:^4.1f} {3[1]:^4.1f} {3[2]:^4.1f}'.format('Orig', best_l2_orig, scores_dev[best_l2_orig][best_l2_orig_mp], scores_test[best_l2_orig]['orig']))
    except:
        # raise
        pass
    try:
        print('{0:<4s} {1:<10s} {2[0]:^4.1f} {2[1]:^4.1f} {2[2]:^4.1f} | {3[0]:^4.1f} {3[1]:^4.1f} {3[2]:^4.1f}'.format('Opt', best_l2_opt, scores_dev[best_l2_opt][best_l2_opt_mp], scores_test[best_l2_opt]['opt']))
    except:
        # raise
        pass
    if best_l2_orig in training_infos:
        avg_training_time = training_infos[best_l2_orig][-1]['tot_time']/len(training_infos[best_l2_orig])
        print('Training time for the first 5 iterations:')
        print(' '.join('{:.3f}'.format(d['time']) for d in training_infos[best_l2_orig][:5]))
        print('Average training time: {:.3f}s'.format(avg_training_time))
	print('Average decoding time: {:.3f}s'.format(testing_infos[best_l2_orig][0]))
        print('Sentences decoded per second: {:.3f}s'.format(testing_infos[best_l2_orig][1]))
        print('Words decoded per second: {:.3f}s'.format(testing_infos[best_l2_orig][2]))
        # l2, n_cluster = best_l2_orig.split('_')
        # with open('{}/best_models_name.list'.format(outdir), 'a') as outfile:
        #     model_file = '{}/{}-{}-l2-{}-cluster-c{}-sizes_245.model'.format(expdir, algo, train_name, l2, n_cluster)
        #     outfile.write('{}\n'.format(model_file))
    
    # print_mp_curve('opt_best', best_l2_opt, scores_dev[best_l2_opt][best_l2_opt_mp], scores_test[best_l2_opt]['opt'])

if __name__ == '__main__':
    main()


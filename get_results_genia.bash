if [ -z ${outDir+x} ]; then
    outDir="evaluation_result"
fi
echo -n "Linear CRF (single chain)"
expDir="linear_model_genia" outDir="${outDir}" algo="LINEAR_CRF" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
echo
echo -n "Linear CRF (multi chains)"
expDir="paralinear_model_genia.nooverlap" outDir="${outDir}" algo="PARALINEAR" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
echo
echo -n "Tang et al. (2013)"
expDir="paralinear_model_genia.fixed.old" outDir="${outDir}" algo="PARALINEAR" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
echo
echo -n "Mention Hypergraph"
expDir="hypergraph_model_genia" outDir="${outDir}" algo="MENTION_HYPERGRAPH" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
echo
echo -n "State-based MS"
expDir="multilinear_model_genia" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
echo
echo -n "Edge-based MS"
expDir="multigraph_model_genia" outDir="${outDir}" algo="MULTIGRAPH" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
#echo
#echo -n "Multilinear No Transition"
#expDir="multilinear_notrans_model_genia" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia
#echo
#echo -n "Paralinear"
#expDir="paralinear_model_genia" outDir="${outDir}" algo="PARALINEAR" trainPath="data/GENIA/train.data" testPath="data/GENIA/test.data" bash get_results.bash genia

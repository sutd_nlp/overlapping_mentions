if [ -z ${nClusters+x} ]; then
    nClusters=100
fi
outfile="tokenized_genia_combined.txt"
awk 'BEGIN {count=0} {if(count==0) {print $0} count=(count+1)%3}' data/GENIA/train.data > ${outfile}
awk 'BEGIN {count=0} {if(count==0) {print $0} count=(count+1)%3}' data/GENIA/dev.data >> ${outfile}
#awk 'BEGIN {count=0} {if(count==0) {print $0} count=(count+1)%3}' data/GENIA/test.data >> ${outfile}
brown-cluster/wcluster --text ${outfile} --c ${nClusters} --threads 4

mkdir "linear_model_conll"
expDir="linear_model_conll" \
    algo="LINEAR_CRF" \
    useSpecificIndicator="true" \
    trainPath="data/CONLL2003/train.data" \
    devPath="data/CONLL2003/dev.data" \
    testPath="data/CONLL2003/dev.data" \
    l2Opts="0.0 0.001 0.01 0.1 1.0" \
    features="words,word_ngram,pos_tag,pos_tag_ngram,bow,wordshape,orthographic,prefix,suffix,transition" \
    halfWindowSize="2" \
    ngramSize="4" \
    halfBowSize="5" \
    useBILOU="true" \
    ignoreOverlaps="true" \
    time bash run_multiple.bash

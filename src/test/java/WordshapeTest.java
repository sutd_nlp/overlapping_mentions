import java.util.Scanner;

import edu.stanford.nlp.process.WordShapeClassifier;

/**
 * 
 */

/**
 * To test the functionality of word shape from Stanford
 */
public class WordshapeTest {

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		try{
			System.out.print(">>> ");
			while(sc.hasNextLine()){
				String line = sc.nextLine();
				System.out.println("Jenny : "+WordShapeClassifier.wordShape(line, WordShapeClassifier.WORDSHAPEJENNY1));
				System.out.println("Chris4: "+WordShapeClassifier.wordShape(line, WordShapeClassifier.WORDSHAPECHRIS4));
				System.out.print(">>> ");
			}
		} catch (Exception e){
			System.out.println("Exiting...");
		}
		sc.close();
	}

}

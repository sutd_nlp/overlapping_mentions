/**
 * 
 */
package com.statnlp.experiment.overlapping;

import edu.stanford.nlp.util.TypesafeMap.Key;

/**
 * 
 */
public class WordAttributes {
	public static class AllCaps implements Key<String>{}
	public static class AllDigits implements Key<String>{}
	public static class AllAlphanumeric implements Key<String>{}
	public static class AllLowercase implements Key<String>{}
	public static class ContainsDigits implements Key<String>{}
	public static class ContainsDots implements Key<String>{}
	public static class ContainsHyphen implements Key<String>{}
	public static class InitialCaps implements Key<String>{}
	public static class LonelyInitial implements Key<String>{}
	public static class PunctuationMark implements Key<String>{}
	public static class RomanNumber implements Key<String>{}
	public static class SingleCharacter implements Key<String>{}
	public static class URL implements Key<String>{}
	public static class Chunk implements Key<String>{}
}

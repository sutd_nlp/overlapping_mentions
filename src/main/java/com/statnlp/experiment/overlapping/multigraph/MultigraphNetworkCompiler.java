package com.statnlp.experiment.overlapping.multigraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.statnlp.commons.types.Instance;
import com.statnlp.experiment.overlapping.EntitySpan;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.Span;
import com.statnlp.experiment.overlapping.SpanLabel;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphNetworkCompiler.NetworkInterpretation;
import com.statnlp.hybridnetworks.LocalNetworkParam;
import com.statnlp.hybridnetworks.Network;
import com.statnlp.hybridnetworks.NetworkCompiler;
import com.statnlp.hybridnetworks.NetworkException;
import com.statnlp.hybridnetworks.NetworkIDMapper;

import edu.stanford.nlp.ling.CoreLabel;

public class MultigraphNetworkCompiler extends NetworkCompiler {

	private static final long serialVersionUID = -4353692924395630953L;
	public static final boolean DEBUG = false;
	
	public SpanLabel[] _labels;
	public int maxSize = 500;
	public int maxBodyCount = 3;
	public OENetwork unlabeledNetwork;
	public NetworkInterpretation networkInterpretation;
	public Map<Integer, int[]> combinationMap;
	
	public static enum OrigNodeType{
		X_NODE,
		B_NODE,
		O_NODE,
		T_NODE,
		E_NODE,
		A_NODE,
	}
	
	public static enum NodeType{
		COUNT_NODE,
		LEAF_NODE,
		STATE_NODE,
		TYPE_NODE,
		ROOT_NODE,
	}
	
	static {
		NetworkIDMapper.setCapacity(new int[]{1000, 1000, 1000, 1000}); // Node type, position, label ID, count
	}

	public MultigraphNetworkCompiler(SpanLabel[] labels, int maxLength, int maxBodyCount, NetworkInterpretation networkInterpretation){
		this._labels = labels;
		this.maxSize = Math.max(this.maxSize, maxLength);
		this.maxBodyCount = maxBodyCount;
		this.networkInterpretation = networkInterpretation;
		buildCombinationMap();
//		buildUnlabeledOrig();
		buildUnlabeled();
	}
	
	private void buildCombinationMap(){
		combinationMap = new HashMap<Integer, int[]>();
		int numCountNodes = 5*maxBodyCount-2;
		int numStateBits = 2*maxBodyCount-1;
		
		int[] combinationArr = new int[2*maxBodyCount];
		combinationArr[0] = 1;
		for(int i=1; i<combinationArr.length; i++){
			if(i%2 == 1 && i != combinationArr.length-1){
				combinationArr[i] = combinationArr[i-1] + 3;
			} else {
				combinationArr[i] = combinationArr[i-1] + 2;
			}
		}
		// List all possible edge combinations in the original graph
		// Each combination is one edge here
		for(int edgeState=0; edgeState< (1<<numCountNodes); edgeState++){
			int[] edgeStateBin = toBinaryArr(edgeState, numCountNodes);
			int curState = 0;
			int nextState = 0;
			int curStateBitPos = 0;
			for(int i=0; i<edgeStateBin.length; i++){
				if(i == 0){
					nextState |= edgeStateBin[i] << (numStateBits-1);
				} else {
//					System.err.println(String.format("i:%d, curStateBitPos:%d, combArr[%d]=%d", i, curStateBitPos, curStateBitPos, combinationArr[curStateBitPos]));
					if(i >= combinationArr[curStateBitPos]){
						curStateBitPos += 1;
					}
					if(edgeStateBin[i] == 1){
//						System.err.println(String.format("numStateBits:%d", numStateBits));
						curState |= 1 << (numStateBits - curStateBitPos);
						
						int diff = i-combinationArr[curStateBitPos-1];
						if(curStateBitPos == combinationArr.length-1){
							diff += 1;
						}
						int nextStateBitPos = curStateBitPos - diff + 1;
						if(diff < 2){
							nextState |= 1 << (numStateBits - nextStateBitPos);
						}
					}
				}
			}
			combinationMap.put(edgeState, new int[]{curState, nextState});
		}
	}

	@Override
	public OENetwork compile(int networkId, Instance inst, LocalNetworkParam param) {
		OEInstance instance = (OEInstance)inst;
		if(instance.isLabeled()){
			return compileLabeled(networkId, instance, param);
		} else {
			return compileUnlabeled(networkId, instance, param);
		}
	}
	
	private OENetwork compileLabeled(int networkId, OEInstance instance, LocalNetworkParam param){
		OENetwork origNetwork = compileLabeledOrig(networkId, instance, param);
		OENetwork network = new OENetwork(networkId, instance, param, this);
		CoreLabel[] words = instance.getInputTokenized().toArray(new CoreLabel[0]);
		int size = words.length;
		
		long leafNode = toNode_Leaf();
		network.addNode(leafNode);

//		int numCountNodes = 5*maxBodyCount-2;
//		for(int i=0; i<numCountNodes; i++){
//			network.addNode(toNode_Count(i));
//		}
		
		long[] typeNodes = new long[this._labels.length];
		for(int labelIdx=0; labelIdx<this._labels.length; labelIdx++){
			int labelId = this._labels[labelIdx].id;
			long typeNode = toNode_Type(0, size, labelId);
			network.addNode(typeNode);
			long prevNode = leafNode;
			for(int pos=size-1; pos>=0; pos--){
				int curState = 0;
				int[] edgeState = new int[5*maxBodyCount-2];
				if(pos < size-1){
					long nextTNode = toNode_T(pos+1, size, labelId);
					// Get the first node (since this is a hyperedge, there might be multiple nodes per child)
					// of the first child (since this is labeled, there is only one child)
					int child_k = origNetwork.getChildren(origNetwork.getNodeIndex(nextTNode))[0][0];
					if(origNetwork.getNodeArray(child_k)[2] == OrigNodeType.B_NODE.ordinal()){
						edgeState[0] = 1;
					}
				}
				for(int bodyIndex=0; bodyIndex<maxBodyCount; bodyIndex++){
					long curONode = toNode_O(pos, size, bodyIndex, labelId);
					if(origNetwork.getNodeIndex(curONode) >= 0){
						curState |= 1 << (maxBodyCount-bodyIndex-1)*2+1;
						if(pos < size-1){
							int nodeIdx = origNetwork.getNodeIndex(curONode);
							int[] child = origNetwork.getChildren(nodeIdx)[0];
							for(int child_k: child){
								int[] childArr = origNetwork.getNodeArray(child_k);
								if(childArr[2] == OrigNodeType.B_NODE.ordinal()){
									edgeState[5*bodyIndex-1] = 1;
								} else if(childArr[2] == OrigNodeType.O_NODE.ordinal()){
									edgeState[5*bodyIndex] = 1;
								}
							}
						}
					}
					long curBNode = toNode_B(pos, size, bodyIndex, labelId);
					if(origNetwork.getNodeIndex(curBNode) >= 0){
						curState |= 1 << (maxBodyCount-bodyIndex-1)*2;
						if(pos < size-1){
							int nodeIdx = origNetwork.getNodeIndex(curBNode);
							int[] child = origNetwork.getChildren(nodeIdx)[0];
							for(int child_k: child){
								int[] childArr = origNetwork.getNodeArray(child_k);
								if(childArr[2] == OrigNodeType.O_NODE.ordinal()){
									edgeState[5*bodyIndex+1] = 1;
								} else if(childArr[2] == OrigNodeType.B_NODE.ordinal()){
									edgeState[5*bodyIndex+(bodyIndex == maxBodyCount-1 ? 1 : 2)] = 1;
								} else if(childArr[2] == OrigNodeType.X_NODE.ordinal()){
									edgeState[5*bodyIndex+(bodyIndex == maxBodyCount-1 ? 2 : 3)] = 1;
								}
							}
						}
					}
				}
				long curNode = toNode_State(pos, size, labelId, curState);
				network.addNode(curNode);
				if(pos < size-1){
//					network.addEdge(curNode, binsToNodes(prevNode, edgeState));
					network.addEdge(curNode, new long[]{prevNode, -fromBinaryArr(edgeState)-1});
				} else {
					network.addEdge(curNode, new long[]{prevNode});
				}
				prevNode = curNode;
			}
			network.addEdge(typeNode, new long[]{prevNode});
			typeNodes[labelIdx] = typeNode;
		}
		long root = toNode_Root(0, size);
		network.addNode(root);
		network.addEdge(root, typeNodes);
		network.finalizeNetwork();
		
		if(DEBUG){
			OENetwork unlabeled = compileUnlabeled(networkId, instance, param);
			System.out.println(instance);
			System.out.println(network);
			System.out.println("Contained: "+unlabeled.contains(network));
		}
		return network;
	}
	
	private OENetwork compileLabeledOrig(int networkId, OEInstance instance, LocalNetworkParam param){
		OENetwork network = new OENetwork(networkId, instance, param, this);
		CoreLabel[] words = instance.getInputTokenized().toArray(new CoreLabel[0]);
		int size = words.length;
		
		long xNode = toNode_X();
		network.addNode(xNode);
		for(EntitySpan entitySpan: instance.output){
			int labelId = entitySpan.label.id;
			long prevNode = -1;
			int pos = -1;
			for(int spanIdx=0, bodyIdx=0; spanIdx<entitySpan.spans.length; spanIdx++, bodyIdx++){
				Span span = entitySpan.spans[spanIdx];
				if(pos != -1){
					boolean hasSomeONodes = false;
					for(; words[pos].beginPosition()<span.start; pos++){
						long curONode = toNode_O(pos, size, bodyIdx, labelId);
						if(!network.contains(curONode)){
							network.addNode(curONode);
						}
						try{
							network.addEdge(prevNode, new long[]{curONode});
						} catch (NetworkException e){
							// do nothing, edge from previous node to this current O node is already added
						}
						prevNode = curONode;
						hasSomeONodes = true;
					}
					if(!hasSomeONodes){
						bodyIdx--;
					}
				} else {
					for(pos=0; words[pos].endPosition() <= span.start; pos++){}
				}
				for(; pos<size && words[pos].beginPosition()<span.end; pos++){
					long curINode = toNode_B(pos, size, bodyIdx, labelId);
					if(!network.contains(curINode)){
						network.addNode(curINode);
					}
					if(prevNode == -1){
						long tNode = toNode_T(pos, size, labelId);
						if(!network.contains(tNode)){
							network.addNode(tNode);
						}
						try{
							network.addEdge(tNode, new long[]{curINode});
						} catch (NetworkException e){
							// do nothing, edge from T to I already added (two mentions with the same start index)
						}
					} else {
						try{
							network.addEdge(prevNode, new long[]{curINode});
						} catch (NetworkException e){
							// do nothing, edge from prevI to curI already added (overlapping mentions)
						}
					}
					prevNode = curINode;
				}
			}
			try{
				network.addEdge(prevNode, new long[]{xNode});
			} catch (NetworkException e){
				// do nothing, edge from I to X already added (two mentions with the same end index)
			}
		}
		// Post processing
		for(int pos=size-1; pos>=0; pos--){
			long[] tNodes = new long[_labels.length];
			for(int idx=0; idx<_labels.length; idx++){
				SpanLabel label = _labels[idx];
				for(int spanIdx=0; spanIdx<this.maxBodyCount; spanIdx++){
					// Collate multiple edges to a single hyperedge for I node and O node
					long bNode = toNode_B(pos, size, spanIdx, label.id);
					if(network.contains(bNode)){
						ArrayList<long[]> childrenList = network.getChildren_tmp(bNode);
						if(childrenList.size() > 1){
							collateChildren(childrenList);
						}
					}
					if(spanIdx > 0){
						long oNode = toNode_O(pos, size, spanIdx, label.id);
						if(network.contains(oNode)){
							ArrayList<long[]> childrenList = network.getChildren_tmp(oNode);
							if(childrenList.size() > 1){
								collateChildren(childrenList);
							}
						}
					}
				}
				// Add missing T nodes
				long tNode = toNode_T(pos, size, label.id);
				if(!network.contains(tNode)){
					network.addNode(tNode);
					network.addEdge(tNode, new long[]{xNode});
				}
				tNodes[idx] = tNode;
			}
			long eNode = toNode_E(pos, size);
			network.addNode(eNode);
			network.addEdge(eNode, tNodes);
			long aNode = toNode_A(pos, size);
			network.addNode(aNode);
			if(pos < size-1){
				long nextANode = toNode_A(pos+1, size);
				network.addEdge(aNode, new long[]{eNode, nextANode});
			} else {
				network.addEdge(aNode, new long[]{eNode});
			}
		}
		
		network.finalizeNetwork();
		
//		if(DEBUG){
//			CTNetwork unlabeled = compileUnlabeledOrig(networkId, instance, param);
//			System.out.println(instance);
//			System.out.println(network);
//			System.out.println("Contained: "+unlabeled.contains(network));
//		}
		return network;
	}
	
	private void collateChildren(List<long[]> childrenList){
		long[] children = new long[childrenList.size()];
		Iterator<long[]> iter = childrenList.iterator();
		int idx = 0;
//		System.out.println("Collating");
		while(iter.hasNext()){
			long[] child = iter.next();
//			System.out.print("[");
//			for(int i=0; i<child.length; i++){
//				System.out.print(child[i]+" ");
//			}
//			System.out.println("]");
			children[idx] = child[0];
			idx += 1;
		}
//		System.out.println("Result");
		childrenList.clear();
		Arrays.sort(children);
//		printArray(children);
		childrenList.add(children);
	}
	
//	private void printArray(long[] arr){
//		StringBuilder builder = new StringBuilder();
//		builder.append("[");
//		for(long str: arr){
//			if(builder.length() > 1) builder.append(",");
//			builder.append(str);
//		}
//		builder.append("]");
//		System.out.println(builder.toString());
//	}

	private OENetwork compileUnlabeled(int networkId, OEInstance instance, LocalNetworkParam param){
		int size = instance.getInputTokenized().size();
		long root = toNode_Root(0, size);
		long[] allNodes = unlabeledNetwork.getAllNodes();
		int[][][] allChildren = unlabeledNetwork.getAllChildren();
		int root_k = Arrays.binarySearch(allNodes, root);
		int numNodes = root_k+1;
		OENetwork network = new OENetwork(networkId, instance, allNodes, allChildren, param, numNodes, this);
		return network;
	}

//	private CTNetwork compileUnlabeledOrig(int networkId, CTInstance instance, LocalNetworkParam param){
//		int size = instance.getInputTokenized().size();
//		long root = toNode_A(0, size);
//		long[] allNodes = unlabeledNetwork.getAllNodes();
//		int[][][] allChildren = unlabeledNetwork.getAllChildren();
//		int root_k = Arrays.binarySearch(allNodes, root);
//		int numNodes = root_k+1;
//		CTNetwork network = new CTNetwork(networkId, instance, allNodes, allChildren, param, numNodes, this);
//		return network;
//	}
	
	private void buildUnlabeled(){
		System.err.print("Building generic unlabeled tree up to size "+maxSize+"...");
		long startTime = System.currentTimeMillis();
		OENetwork network = new OENetwork();
		int size = maxSize;
//		int numCountNodes = 5*maxBodyCount-2;
//		for(int i=0; i<numCountNodes; i++){
//			network.addNode(toNode_Count(i));
//		}
		long leafNode = toNode_Leaf();
		network.addNode(leafNode);
		for(int pos=size-1; pos>=0; pos--){
			long[] typeNodes = new long[_labels.length];
			for(int labelIdx=0; labelIdx<_labels.length; labelIdx++){
				SpanLabel label = _labels[labelIdx];
				long typeNode = toNode_Type(pos, size, label.id);
				typeNodes[labelIdx] = typeNode;
				network.addNode(typeNode);
				int numStates = (int)Math.round(Math.pow(2, 2*maxBodyCount-1));
				for(int state=0; state<numStates; state++){
					if(pos == size-1){
						boolean hasONode = false;
						for(int mask=1; mask<maxBodyCount; mask++){
							if((state & (1 << 2*mask-1)) != 0){
								hasONode = true;
								break;
							}
						}
						// Do not need to add O-node at the end.
						if(hasONode){
							continue;
						}
					}
					long stateNode = toNode_State(pos, size, label.id, state);
					network.addNode(stateNode);
					if(state == 0 || state == (1 << 2*maxBodyCount-2)){
						// The condition can be rewritten as if(state & ((1<<(2*maxBodyCount-2))-1) == 0)
						// We can only start from the states where only the top-most node is possibly on
						network.addEdge(typeNode, new long[]{stateNode});
					}
					if(pos == size-1){
						// At the end, connect to the leaf node
						network.addEdge(stateNode, new long[]{leafNode});
					}
				}
				if(pos < size-1){
					for(int edgeState: combinationMap.keySet()){
						int[] edge = combinationMap.get(edgeState);
						if(pos == size-2){
							boolean hasONode = false;
							for(int mask=1; mask<maxBodyCount; mask++){
								if((edge[1] & (1 << 2*mask-1)) != 0){
									hasONode = true;
									break;
								}
							}
							// Do not need to add O-node at the end.
							if(hasONode){
								continue;
							}
						}
						long curState = toNode_State(pos, size, label.id, edge[0]);
//						System.err.println(String.format("curState: %d, pos:%d, size:%d, edge[0]:%d", curState, pos, size, edge[0]));
						long nextState = toNode_State(pos+1, size, label.id, edge[1]);
//						System.err.println(String.format("nextState: %d, pos:%d, size:%d, edge[1]:%d", nextState, pos, size, edge[1]));
//						int[] countNodes = toBinaryArr(edgeState, numCountNodes);
//						long[] child = binsToNodes(nextState, countNodes);
						long[] child = new long[]{nextState, -edgeState-1};
						network.addEdge(curState, child);
					}
				}
			}
			long root = toNode_Root(pos, size);
			network.addNode(root);
			network.addEdge(root, typeNodes);
		}
		network.finalizeNetwork();
		
		this.unlabeledNetwork = network;
		
		long endTime = System.currentTimeMillis();
		System.err.println(String.format("Done in %.3fs", (endTime-startTime)/1000.0));
	}

//	private long[] binsToNodes(long nextState, int[] countNodes) {
//		List<Long> childList = new ArrayList<Long>();
//		childList.add(nextState);
//		for(int countNodeIdx=0; countNodeIdx<countNodes.length; countNodeIdx++){
//			if(countNodes[countNodeIdx] == 1){
//				childList.add(toNode_Count(countNodeIdx));
//			}
//		}
//		long[] child = new long[childList.size()];
//		for(int i=0; i<childList.size(); i++){
//			child[i] = childList.get(i);
//		}
//		return child;
//	}
	
	protected int[] toBinaryArr(int state, int size){
		int[] result = new int[size];
		for(int i=result.length-1; i>=0; i--){
			result[i] = state%2;
			state >>= 1;
		}
		return result;
	}
	
	protected int fromBinaryArr(int[] arr){
		int result = 0;
		for(int i=0; i<arr.length; i++){
			result |= (arr[i] << (arr.length-i-1));
		}
		return result;
	}
	
//	private void buildUnlabeledOrig(){
//		System.err.print("Building generic unlabeled tree up to size "+maxSize+"...");
//		long startTime = System.currentTimeMillis();
//		CTNetwork network = new CTNetwork();
//		int size = maxSize;
//		
//		long xNode = toNode_X();
//		network.addNode(xNode);
//		for(int pos=size-1; pos>=0; pos--){
//			long[] tNodes = new long[_labels.length];
//			for(int idx=0; idx<_labels.length; idx++){
//				SpanLabel label = _labels[idx];
//				long firstBNode = -1;
//				for(int bodyIdx=0; bodyIdx < this.maxBodyCount; bodyIdx++){
//					long bNode = toNode_B(pos, size, bodyIdx, label.id);
//					network.addNode(bNode);
//					network.addEdge(bNode, new long[]{xNode});
//					long oNode = -1;
//					if(bodyIdx == 0){ // Top-most B-node, this doesn't have O-node
//						firstBNode = bNode;
//					} else if(pos < size-1){ // The O-node
//						oNode = toNode_O(pos, size, bodyIdx, label.id);
//						network.addNode(oNode);
//					}
//					if(pos < size-1){
//						long nextONode = -1;
//						if(bodyIdx > 0){
//							nextONode = toNode_O(pos+1, size, bodyIdx, label.id);
//						}
//						long nextBNode = toNode_B(pos+1, size, bodyIdx, label.id);
//						long nextHighONode = -1;
//						if(bodyIdx+1 < this.maxBodyCount){
//							nextHighONode = toNode_O(pos+1, size, bodyIdx+1, label.id);
//						}
//						network.addEdge(bNode, new long[]{nextBNode});
//						network.addEdge(bNode, new long[]{xNode, nextBNode});
//						if(nextHighONode != -1 && network.contains(nextHighONode)){
//							network.addEdge(bNode, new long[]{nextHighONode});
//							network.addEdge(bNode, new long[]{xNode, nextHighONode});
//							network.addEdge(bNode, new long[]{nextBNode, nextHighONode});
//							network.addEdge(bNode, new long[]{xNode, nextBNode, nextHighONode});
//						}
//						if(oNode != -1){
//							network.addEdge(oNode, new long[]{nextBNode});
//						}
//						if(nextONode != -1 && network.contains(nextONode)){ // then oNode also won't be -1
//							network.addEdge(oNode, new long[]{nextONode});
//							network.addEdge(oNode, new long[]{nextBNode, nextONode});
//						}
//					}
//				}
//				long tNode = toNode_T(pos, size, label.id);
//				network.addNode(tNode);
//				network.addEdge(tNode, new long[]{xNode});
//				network.addEdge(tNode, new long[]{firstBNode});
//				tNodes[idx] = tNode;
//			}
//			long eNode = toNode_E(pos, size);
//			network.addNode(eNode);
//			network.addEdge(eNode, tNodes);
//			long aNode = toNode_A(pos, size);
//			network.addNode(aNode);
//			if(pos < size-1){
//				long nextANode = toNode_A(pos+1, size);
//				network.addEdge(aNode, new long[]{eNode, nextANode});
//			} else {
//				network.addEdge(aNode, new long[]{eNode});
//			}
//		}
//		
//		network.finalizeNetwork();
//		
//		this.unlabeledNetwork = network;
//		
//		long endTime = System.currentTimeMillis();
//		System.err.println(String.format("Done in %.3fs", (endTime-startTime)/1000.0));
//	}
	
	private long toNode_X(){
		return toNode_orig(0, 1, OrigNodeType.X_NODE, 0, 0);
	}
	
	private long toNode_B(int pos, int size, int bodyIndex, int labelId){
		return toNode_orig(pos, size, OrigNodeType.B_NODE, bodyIndex, labelId);
	}

	private long toNode_O(int pos, int size, int bodyIndex, int labelId){
		return toNode_orig(pos, size, OrigNodeType.O_NODE, bodyIndex, labelId);
	}
	
	private long toNode_T(int pos, int size, int labelId){
		return toNode_orig(pos, size, OrigNodeType.T_NODE, 9, labelId);
	}
	
	private long toNode_E(int pos, int size){
		return toNode_orig(pos, size, OrigNodeType.E_NODE, 9, 0);
	}
	
	private long toNode_A(int pos, int size){
		return toNode_orig(pos, size, OrigNodeType.A_NODE, 9, 0);
	}
	
	protected int[] toNodeArr_X(){
		return toNodeArr_orig(0, 1, OrigNodeType.X_NODE, 0, 0);
	}
	
	protected int[] toNodeArr_B(int pos, int size, int bodyIndex, int labelId){
		return toNodeArr_orig(pos, size, OrigNodeType.B_NODE, bodyIndex, labelId);
	}

	protected int[] toNodeArr_O(int pos, int size, int bodyIndex, int labelId){
		return toNodeArr_orig(pos, size, OrigNodeType.O_NODE, bodyIndex, labelId);
	}
	
	protected int[] toNodeArr_T(int pos, int size, int labelId){
		return toNodeArr_orig(pos, size, OrigNodeType.T_NODE, 9, labelId);
	}
	
	protected int[] toNodeArr_E(int pos, int size){
		return toNodeArr_orig(pos, size, OrigNodeType.E_NODE, 9, 0);
	}
	
	protected int[] toNodeArr_A(int pos, int size){
		return toNodeArr_orig(pos, size, OrigNodeType.A_NODE, 9, 0);
	}
	
	private long toNode_orig(int pos, int size, OrigNodeType nodeType, int bodyIndex, int labelId){
		int[] arr = new int[]{size-pos-1, bodyIndex, nodeType.ordinal(), labelId};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private int[] toNodeArr_orig(int pos, int size, OrigNodeType nodeType, int bodyIndex, int labelId){
		return new int[]{size-pos-1, bodyIndex, nodeType.ordinal(), labelId};
	}
	
//	private long toNode_Count(int count){
//		return toNode(NodeType.COUNT_NODE, 0, 1, 0, count);
//	}
	
	private long toNode_Leaf(){
		return toNode(NodeType.LEAF_NODE, 0, 1, 0, 0);
	}
	
	private long toNode_State(int pos, int size, int labelId, int count){
		return toNode(NodeType.STATE_NODE, pos, size, labelId, count);
	}
	
	private long toNode_Type(int pos, int size, int labelId){
		return toNode(NodeType.TYPE_NODE, pos, size, labelId, 0);
	}
	
	private long toNode_Root(int pos, int size){
		return toNode(NodeType.ROOT_NODE, pos, size, 0, 0);
	}
	
	private long toNode(NodeType nodeType, int pos, int size, int labelId, int count){
		int[] arr = new int[]{size-pos-1, nodeType.ordinal(), labelId, count};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private static class JunctionInfo implements Comparable<JunctionInfo>{
		public int highestUnprocessedChildIdx;
		public int startIdx;
		public SpanLabel label;
		public List<OrigNodeType> nodeSeq;
		public int node_k;
		
		public JunctionInfo(int node_k, int highestIdx, int startIdx, SpanLabel label, List<OrigNodeType> nodeSeq){
			this.node_k = node_k;
			this.highestUnprocessedChildIdx = highestIdx;
			this.startIdx = startIdx;
			this.label = label;
			this.nodeSeq = new ArrayList<OrigNodeType>(nodeSeq);
		}
		
		public int hashCode(){
			return 31*Integer.hashCode(node_k) + label.hashCode();
//			return 4*Integer.hashCode(node_k) + 2*Integer.hashCode(startIdx) + label.hashCode();
		}
		
		public boolean equals(Object obj){
			if(obj instanceof JunctionInfo){
				JunctionInfo info = (JunctionInfo)obj;
				if(node_k != info.node_k) return false;
//				if(startIdx != info.startIdx) return false;
				if(label != info.label) return false;
				return true;
			}
			return false;
		}

		@Override
		public int compareTo(JunctionInfo info) {
			if(this.node_k < info.node_k) return -1;
			if(this.node_k > info.node_k) return 1;
//			if(this.startIdx < info.startIdx) return -1;
//			if(this.startIdx > info.startIdx) return 1;
			return this.label.compareTo(info.label);
		}
	}

	private static EntitySpan createEntitySpan(int start, List<OrigNodeType> nodeSeq, List<CoreLabel> inputTokenized, SpanLabel label){
		List<Span> spans = new ArrayList<Span>();
		int startIdx = -1;
		for(int i=0; i<nodeSeq.size(); i++){
			OrigNodeType curNode = nodeSeq.get(i);
			if(curNode == OrigNodeType.O_NODE){
				startIdx = -1;
				continue;
			}
			if(startIdx == -1){
				startIdx = inputTokenized.get(start+i).beginPosition();
			}
			
			if(shouldCreateNewSpan(start, i, nodeSeq, inputTokenized)){
				spans.add(new Span(startIdx, inputTokenized.get(start+i).endPosition()));
				startIdx = -1;
			}
		}
		return new EntitySpan(null, label, spans.toArray(new Span[spans.size()]));
	}
	
	private static boolean shouldCreateNewSpan(int start, int i, List<OrigNodeType> nodeSeq, List<CoreLabel> inputTokenized){
		if(i == nodeSeq.size()-1){
			return true;
		}
		OrigNodeType next = nodeSeq.get(i+1);
		if(next == OrigNodeType.O_NODE){
			return true;
		}
		return false;
	}

	@Override
	public OEInstance decompile(Network net){
		OENetwork network = (OENetwork)net;
		OENetwork networkOrig = new OENetwork(net.getNetworkId(), net.getInstance(), ((OENetwork)net).getParam(), this);
		long rootNode = network.getRoot();
		int rootNode_k = network.getNodeIndex(rootNode);
		int size = network.getNodeArray(rootNode_k)[0]+1;
		int[] typeNodes_k = network.getMaxPath(rootNode_k);
		networkOrig.addNode(toNode_X());
		networkOrig.addNode(toNode_A(0, size));
		networkOrig.addNode(toNode_E(0, size));
		for(int i=1; i<size; i++){
			networkOrig.addNode(toNode_A(i, size));
			networkOrig.addNode(toNode_E(i, size));
			networkOrig.addEdge(toNode_A(i-1, size), new long[]{toNode_E(i-1, size), toNode_A(i, size)});
		}
		networkOrig.addEdge(toNode_A(size-1, size), new long[]{toNode_E(size-1, size)});
		for(int typeNode_k: typeNodes_k){
			int curNode_k = typeNode_k;
			int labelId = network.getNodeArray(typeNode_k)[2];
			int parentState = 0;
			int[] parentStateArr = toBinaryArr(parentState, 2*maxBodyCount-1);
			
			// Get info on children
			int childState = 0;
			int[] childStateArr = null;
			int[] children_k = network.getMaxPath(curNode_k);
			int child_k = children_k[0];
			int[] childArr = network.getNodeArray(child_k);
			childState = childArr[3];
			childStateArr = toBinaryArr(childState, 2*maxBodyCount-1);
			
			// Add the first edge from T-node to either B-node or X-node
			long parentNodeOrig = toNode_T(0, size, labelId);
			long[] childrenNodeOrig = null;
			if(childState >> 2*maxBodyCount-2 == 1){
				childrenNodeOrig = new long[]{toNode_B(0, size, 0, labelId)};
			} else {
				childrenNodeOrig = new long[]{toNode_X()};
			}
//			System.err.println("First T-X or T-B");
//			System.err.println(Arrays.toString(NetworkIDMapper.toHybridNodeArray(parentOrig)));
//			for(long child: childrenNodeOrig){
//				System.err.println("\t"+Arrays.toString(NetworkIDMapper.toHybridNodeArray(child)));
//			}
			networkOrig.addNode(parentNodeOrig);
			for(long childNode: childrenNodeOrig){
				networkOrig.addNode(childNode);
			}
			networkOrig.addEdge(parentNodeOrig, childrenNodeOrig);
			curNode_k = child_k;
			parentState = childState;
			parentStateArr = childStateArr;
			
			// Process the rest
			int pos = 0;
			long leaf = toNode_Leaf();
			int leaf_k = network.getNodeIndex(leaf);
			while(curNode_k != leaf_k){
				children_k = network.getMaxPath(curNode_k);
				child_k = children_k[0];
				childArr = network.getNodeArray(child_k);
				childState = childArr[3];
				childStateArr = toBinaryArr(childState, 2*maxBodyCount-1);

				NodeType childType = NodeType.values()[childArr[1]];
				if(childType == NodeType.LEAF_NODE){
					for(int curStateBitPos=0; curStateBitPos<parentStateArr.length; curStateBitPos+=2){
						if(parentStateArr[curStateBitPos] == 1){
							parentNodeOrig = toNode_B(pos, size, curStateBitPos/2, labelId);
							childrenNodeOrig = new long[]{toNode_X()};
//							System.err.println("B-X");
//							System.err.println(Arrays.toString(NetworkIDMapper.toHybridNodeArray(parentOrig)));
//							for(long child: childrenNodeOrig){
//								System.err.println("\t"+Arrays.toString(NetworkIDMapper.toHybridNodeArray(child)));
//							}
							networkOrig.addNode(parentNodeOrig);
							networkOrig.addEdge(parentNodeOrig, childrenNodeOrig);
						}
					}
					curNode_k = child_k;
					pos += 1;
					continue;
				}

				int[] combinationArr = new int[2*maxBodyCount];
				combinationArr[0] = 1;
				for(int i=1; i<combinationArr.length; i++){
					if(i%2 == 1 && i != combinationArr.length-1){
						combinationArr[i] = combinationArr[i-1] + 3;
					} else {
						combinationArr[i] = combinationArr[i-1] + 2;
					}
				}
				List<Integer> activeEdges = new ArrayList<Integer>();
				int combinationArrIdx = 1;
				boolean hasTtoB = false;
				int[] countNodeArr = toBinaryArr(-children_k[1]-1, 5*maxBodyCount-2);
				for(int i=0; i<countNodeArr.length; i++){
//					int countNodeValue = network.getNodeArray(children_k[i])[3];
					if(countNodeArr[i] == 0){
						continue;
					}
					int countNodeValue = i;
					if(countNodeValue == 0){
						parentNodeOrig = toNode_T(pos+1, size, labelId);
						childrenNodeOrig = new long[]{toNode_B(pos+1, size, 0, labelId)};
//						System.err.println("T-B");
//						System.err.println(Arrays.toString(NetworkIDMapper.toHybridNodeArray(parentOrig)));
//						for(long child: childrenNodeOrig){
//							System.err.println("\t"+Arrays.toString(NetworkIDMapper.toHybridNodeArray(child)));
//						}
						hasTtoB = true;
						networkOrig.addNode(parentNodeOrig);
						networkOrig.addNode(childrenNodeOrig[0]);
						networkOrig.addEdge(parentNodeOrig, childrenNodeOrig);
					} else {
						if(combinationArr[combinationArrIdx] <= countNodeValue && activeEdges.size() > 0){
							// Add features from this edge
							int curStateBitPos = combinationArrIdx-1;
							if(curStateBitPos % 2 == 0){
								parentNodeOrig = toNode_B(pos, size, curStateBitPos/2, labelId);
							} else {
								parentNodeOrig = toNode_O(pos, size, (curStateBitPos+1)/2, labelId);
							}
							childrenNodeOrig = new long[activeEdges.size()];
							for(int edgeIdx=0; edgeIdx<activeEdges.size(); edgeIdx++){
								int edge = activeEdges.get(edgeIdx);
								int diff = edge - combinationArr[combinationArrIdx-1];
								if(combinationArrIdx == combinationArr.length-1){
									diff += 1;
								}
								if(diff == 2){
									childrenNodeOrig[activeEdges.size()-1-edgeIdx] = toNode_X();
								} else {
									int nextStateBitPos = curStateBitPos - diff + 1;
									if(nextStateBitPos % 2 == 0){
										childrenNodeOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = toNode_B(pos+1, size, nextStateBitPos/2, labelId);
									} else {
										childrenNodeOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = toNode_O(pos+1, size, (nextStateBitPos+1)/2, labelId);
									}
								}
							}
//							System.err.println("B-B or B-O or O-B or O-O");
//							System.err.println(Arrays.toString(NetworkIDMapper.toHybridNodeArray(parentOrig)));
//							for(long child: childrenNodeOrig){
//								System.err.println("\t"+Arrays.toString(NetworkIDMapper.toHybridNodeArray(child)));
//							}
							networkOrig.addNode(parentNodeOrig);
							for(long childNode: childrenNodeOrig){
								networkOrig.addNode(childNode);
							}
							networkOrig.addEdge(parentNodeOrig, childrenNodeOrig);
							activeEdges.clear();
						}
						while(combinationArr[combinationArrIdx] <= countNodeValue){
							combinationArrIdx += 1;
						}
						activeEdges.add(countNodeValue);
					}
				}
				if(!hasTtoB){
					parentNodeOrig = toNode_T(pos+1, size, labelId);
					childrenNodeOrig = new long[]{toNode_X()};
//					System.err.println(String.format("pos+1:%d, size:%d, labelId:%d", pos+1, size, labelId));
//					System.err.println("T-X");
//					System.err.println(Arrays.toString(NetworkIDMapper.toHybridNodeArray(parentOrig)));
//					for(long child: childrenNodeOrig){
//						System.err.println("\t"+Arrays.toString(NetworkIDMapper.toHybridNodeArray(child)));
//					}
					networkOrig.addNode(parentNodeOrig);
					networkOrig.addEdge(parentNodeOrig, childrenNodeOrig);
				}
				if(activeEdges.size() > 0){
					// Add features from this edge
					int curStateBitPos = combinationArrIdx-1;
					if(curStateBitPos % 2 == 0){
						parentNodeOrig = toNode_B(pos, size, curStateBitPos/2, labelId);
					} else {
						parentNodeOrig = toNode_O(pos, size, (curStateBitPos+1)/2, labelId);
					}
					childrenNodeOrig = new long[activeEdges.size()];
					for(int edgeIdx=0; edgeIdx<activeEdges.size(); edgeIdx++){
						int edge = activeEdges.get(edgeIdx);
						int diff = edge - combinationArr[combinationArrIdx-1];
						if(combinationArrIdx == combinationArr.length-1){
							diff += 1;
						}
						if(diff == 2){
							childrenNodeOrig[activeEdges.size()-1-edgeIdx] = toNode_X();
						} else {
							int nextStateBitPos = curStateBitPos - diff + 1;
							if(nextStateBitPos % 2 == 0){
								childrenNodeOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = toNode_B(pos+1, size, nextStateBitPos/2, labelId);
							} else {
								childrenNodeOrig[curStateBitPos % 2 == 0 ? activeEdges.size()-1-edgeIdx : edgeIdx] = toNode_O(pos+1, size, (nextStateBitPos+1)/2, labelId);
							}
						}
					}
//					System.err.println("B-B or B-O or O-B or O-O");
//					System.err.println(Arrays.toString(NetworkIDMapper.toHybridNodeArray(parentOrig)));
//					for(long child: childrenNodeOrig){
//						System.err.println("\t"+Arrays.toString(NetworkIDMapper.toHybridNodeArray(child)));
//					}
					networkOrig.addNode(parentNodeOrig);
					for(long childNode: childrenNodeOrig){
						networkOrig.addNode(childNode);
					}
					networkOrig.addEdge(parentNodeOrig, childrenNodeOrig);
				}
				activeEdges.clear();
				if(curNode_k != typeNode_k){
					// Process current state to next state
				}
				curNode_k = child_k;
				pos += 1;
				parentState = childState;
				parentStateArr = childStateArr;
			}
		}
		for(int i=0; i<size; i++){
			long eNode = toNode_E(i, size);
			long[] typeNodes = new long[_labels.length];
			for(int labelIdx=0; labelIdx<_labels.length; labelIdx++){
				typeNodes[labelIdx] = toNode_T(i, size, _labels[labelIdx].id);
			}
			networkOrig.addEdge(eNode, typeNodes);
		}
		networkOrig.finalizeNetwork();
		return decompileOrig(networkOrig);
	}
	
	public OEInstance decompileOrig(OENetwork network) {
		OEInstance result = (OEInstance)network.getInstance().duplicate();
		List<CoreLabel> inputTokenized = result.getInputTokenized();
		int size = inputTokenized.size();
		
		List<EntitySpan> prediction = new ArrayList<EntitySpan>();
		long[] nodes = network.getAllNodes();
		List<JunctionInfo> unfinishedJunctions = new ArrayList<JunctionInfo>();
		Set<JunctionInfo> usedJunctions = new TreeSet<JunctionInfo>();
		long aNode = network.getRoot();
		int aNode_k = Arrays.binarySearch(nodes, aNode);
		for(int pos=0; pos<size; pos++){
			int[] curChildren = network.getChildren(aNode_k)[0];
			int eNode_k = curChildren[0];
			int[] curTNodes = network.getChildren(eNode_k)[0];
			for(int idx=0; idx<curTNodes.length; idx++){
				int tNode_k = curTNodes[idx];
				int[] tNode_arr = network.getNodeArray(tNode_k);
				int labelId = tNode_arr[3];
				SpanLabel label = SpanLabel.get(labelId);
				int[] child = network.getChildren(tNode_k)[0];
				int node_k = child[0]; // either an B-node or an X-node
				int[] node_arr = network.getNodeArray(node_k);
				OrigNodeType nodeType = OrigNodeType.values()[node_arr[2]];
				if(nodeType == OrigNodeType.B_NODE){ // Has a mention starting here
					List<OrigNodeType> nodeSeq = new ArrayList<OrigNodeType>();
					traverseMaxNetwork(network, inputTokenized, prediction, unfinishedJunctions, usedJunctions, nodeSeq, pos, node_k,
							nodeType, label);
				}
			}
			if(curChildren.length == 2){
				aNode_k = curChildren[1];
			}
		}
		int junctionIndex = 0;
		if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
			junctionIndex = unfinishedJunctions.size()-1;
		}
		while(junctionIndex < unfinishedJunctions.size() && junctionIndex >= 0){
			JunctionInfo junction = unfinishedJunctions.get(junctionIndex);
			List<OrigNodeType> nodeSeq = new ArrayList<OrigNodeType>(junction.nodeSeq);
			int start = junction.startIdx;
			int[] child = network.getChildren(junction.node_k)[0];
			int node_k = child[junction.highestUnprocessedChildIdx];
			if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
				junction.highestUnprocessedChildIdx -= 1;
				if(junction.highestUnprocessedChildIdx == -1){
					usedJunctions.add(unfinishedJunctions.remove(junctionIndex));
				}
			}
			int[] node_arr = network.getNodeArray(node_k);
			OrigNodeType nodeType = OrigNodeType.values()[node_arr[2]];
			SpanLabel label = junction.label;
			traverseMaxNetwork(network, inputTokenized, prediction, unfinishedJunctions, usedJunctions, nodeSeq, start, node_k,
					nodeType, label);
			if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
				junctionIndex = unfinishedJunctions.size()-1;
			}
		}
		result.setPrediction(prediction);
		return result;
	}

	private void traverseMaxNetwork(OENetwork network, List<CoreLabel> inputTokenized, List<EntitySpan> prediction,
			List<JunctionInfo> unfinishedJunctions, Set<JunctionInfo> usedJunctions, List<OrigNodeType> nodeSeq, int start, int node_k, OrigNodeType nodeType,
			SpanLabel label) throws RuntimeException {
		int[] child = null;
		int[] node_arr = null;
		while(nodeType != OrigNodeType.X_NODE){
			nodeSeq.add(nodeType);
			child = network.getChildren(node_k)[0];
			if(child.length == 0){
				throw new RuntimeException(String.format("Decoding ends at a leaf node (%s at pos %d) which is not an X-node\nInput: %s",
							nodeType, inputTokenized.size()-network.getNodeArray(node_k)[0]-1, inputTokenized));
			} else if(child.length == 1){
				node_k = child[0];
			} else { // there are more than one children for this edge (overlapping mention)
				node_k = nextChild(unfinishedJunctions, usedJunctions, nodeSeq, start, node_k, label, child);
			}
			node_arr = network.getNodeArray(node_k);
			nodeType = OrigNodeType.values()[node_arr[2]];
		}
		if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
			int unfinishedSize = unfinishedJunctions.size();
			while(unfinishedSize > 0){
				JunctionInfo junction = unfinishedJunctions.get(unfinishedSize-1);
				if(junction.startIdx != start){
					break;
				}
				junction.highestUnprocessedChildIdx -= 1;
				if(junction.highestUnprocessedChildIdx == -1){
					usedJunctions.add(unfinishedJunctions.remove(unfinishedSize-1));
					unfinishedSize -= 1;
					continue;
				}
				break;
			}
		}
		prediction.add(createEntitySpan(start, nodeSeq, inputTokenized, label));
	}

	private int nextChild(List<JunctionInfo> unfinishedJunctions, Set<JunctionInfo> usedJunctions, List<OrigNodeType> nodeSeq, int start, int node_k,
			SpanLabel label, int[] child) {
		int childIdx = -1;
		for(int i=unfinishedJunctions.size()-1; i>=0; i--){
			JunctionInfo info = unfinishedJunctions.get(i);
			if(info.node_k == node_k && (networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH || info.startIdx == start)){
				childIdx = info.highestUnprocessedChildIdx;
				if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
					info.highestUnprocessedChildIdx -= 1;
					if(info.highestUnprocessedChildIdx == -1){
						usedJunctions.add(unfinishedJunctions.remove(i));
					}	
				}
				break;
			}
		}
		if(childIdx == -1){
			int highestUnprocessed = -1;
//			if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
//				highestUnprocessed = child.length-1;
//				JunctionInfo curInfo = new JunctionInfo(node_k, highestUnprocessed, start, label, nodeSeq);
//				unfinishedJunctions.add(curInfo);
//				childIdx = child.length - 1;
//			} else if (networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
//				highestUnprocessed = -1;
//				childIdx = 0;
//			}
			switch(networkInterpretation){
			case GENERATE_ALL:
				highestUnprocessed = child.length-1;
				break;
			case GENERATE_ENOUGH:
				highestUnprocessed = child.length-2;
				break;
			}
			JunctionInfo curInfo = new JunctionInfo(node_k, highestUnprocessed, start, label, nodeSeq);
			if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH && usedJunctions.contains(curInfo)){
				childIdx = 0;
			} else {
				unfinishedJunctions.add(curInfo);
				childIdx = child.length-1;
			}
		}
		node_k = child[childIdx];
		return node_k;
	}

}
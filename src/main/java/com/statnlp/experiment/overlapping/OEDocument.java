package com.statnlp.experiment.overlapping;

import java.io.Serializable;
import java.util.List;

/**
 * Represents a clinical document
 * @author Aldrian Obaja <aldrianobaja.m@gmail.com>
 *
 */
public class OEDocument implements Serializable{
	
	private static final long serialVersionUID = -4698300709681532759L;

	public enum DocumentType{
		DISCHARGE,
		ECG,
		ECHO,
		RADIOLOGY,
		;
	}
	
	public DocumentType docType;
	public String text;
	public List<EntitySpan> annotations;
	public String filename;
	
	public OEDocument(String text, String filename){
		this(text, null, getDocType(text), filename);
	}
	
	public OEDocument(String text, List<EntitySpan> annotations, String filename){
		this(text, annotations, getDocType(text), filename);
	}

	public OEDocument(String text, List<EntitySpan> annotations, DocumentType docType, String filename) {
		this.text = text;
		this.annotations = annotations;
		this.docType = docType;
		this.filename = filename;
	}
	
	public static DocumentType getDocType(String text){
		String[] tokens = text.split("\\|\\|\\|\\|", 5);
		if(tokens.length == 5){
			switch(tokens[3].trim()){
			case "DISCHARGE_SUMMARY":
				return DocumentType.DISCHARGE;
			case "ECG_REPORT":
				return DocumentType.ECG;
			case "ECHO_REPORT":
				return DocumentType.ECHO;
			case "RADIOLOGY_REPORT":
				return DocumentType.RADIOLOGY;
			default:
				System.err.println("Document type cannot be determined automatically, please specify");
				return DocumentType.DISCHARGE;
			}
		} else {
			System.err.println("Document type cannot be determined automatically, please specify");
			return DocumentType.DISCHARGE;
		}
	}
	
	public void printAnnotations(){
		for(EntitySpan span: annotations){
			System.out.println(span.toString(text));
		}
	}
	
	public String toString(){
		return this.filename;
	}

}

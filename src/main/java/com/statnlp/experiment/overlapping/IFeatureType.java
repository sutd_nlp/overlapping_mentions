package com.statnlp.experiment.overlapping;

/**
 * Interface to FeatureType in FeatureManager
 * @author Aldrian Obaja <aldrianobaja.m@gmail.com>
 *
 */	
public interface IFeatureType {
	public void enable();
	public void disable();
	public boolean enabled();
}

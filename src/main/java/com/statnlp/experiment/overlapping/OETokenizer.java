package com.statnlp.experiment.overlapping;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.StringUtils;
import justhalf.nlp.tokenizer.RegexTokenizer;
import justhalf.nlp.tokenizer.StanfordTokenizer;
import justhalf.nlp.tokenizer.Tokenizer;
import justhalf.nlp.tokenizer.WhitespaceTokenizer;

public class OETokenizer implements Serializable {
	
	private static final long serialVersionUID = -2097450154196277909L;

	public static enum TokenizerMethod {
		WHITESPACE,
		REGEX,
		STANFORD,
	}
	
	public static Tokenizer whitespaceTokenizer = null;
	public static Tokenizer regexTokenizer = null;
	public static Tokenizer stanfordTokenizer = null;
	
	public static String[] tokenizeToString(String input, TokenizerMethod method, boolean normalize){
		switch(method){
		case WHITESPACE:
			return tokenize_whitespace_string(input, normalize);
		case REGEX:
			return tokenize_regex_string(input, normalize);
		case STANFORD:
			return tokenize_stanford_string(input, normalize);
		default:
			throw new UnsupportedOperationException("The tokenizing method "+method+" is not recognized");
		}
	}
	
	public static List<CoreLabel> tokenize(String input, TokenizerMethod method, boolean normalize){
		switch(method){
		case WHITESPACE:
			return tokenize_whitespace(input, normalize);
		case REGEX:
			return tokenize_regex(input, normalize);
		case STANFORD:
			return tokenize_stanford(input, normalize);
		default:
			throw new UnsupportedOperationException("The tokenizing method "+method+" is not recognized");
		}
	}
	
	public static List<CoreLabel> tokenize_whitespace(String input, boolean normalize){
		if(whitespaceTokenizer == null){
			synchronized (TokenizerMethod.WHITESPACE){
				if(whitespaceTokenizer == null){
					whitespaceTokenizer = new WhitespaceTokenizer();
				}
			}
		}
		return fixTokenization(whitespaceTokenizer.tokenize(input), normalize);
	}
	
	public static List<CoreLabel> tokenize_regex(String input, boolean normalize){
		if(regexTokenizer == null){
			synchronized (TokenizerMethod.REGEX){
				if(regexTokenizer == null){
					regexTokenizer = new RegexTokenizer();
				}
			}
		}
		return fixTokenization(regexTokenizer.tokenize(input), normalize);
	}
	
	public static List<CoreLabel> tokenize_stanford(String input, boolean normalize){
		if(stanfordTokenizer == null){
			synchronized (TokenizerMethod.STANFORD){
				if(stanfordTokenizer == null){
					stanfordTokenizer = new StanfordTokenizer();
				}
			}
		}
		return fixTokenization(stanfordTokenizer.tokenize(input), normalize);
	}
	
	public static String[] tokenize_whitespace_string(String input, boolean normalize){
		return labelsToStrings(tokenize_whitespace(input, normalize));
	}
	
	public static String[] tokenize_regex_string(String input, boolean normalize){
		return labelsToStrings(tokenize_regex(input, normalize));
	}
	
	public static String[] tokenize_stanford_string(String input, boolean normalize){
		return labelsToStrings(tokenize_regex(input, normalize));
	}
	
	private static String[] labelsToStrings(List<CoreLabel> words){
		String[] result = new String[words.size()];
		for(int i=0; i<result.length; i++){
			result[i] = words.get(i).value();
		}
		return result;
	}
	
	private static List<CoreLabel> fixTokenization(List<CoreLabel> tokens, boolean normalize){
		List<CoreLabel> result = new ArrayList<CoreLabel>();
		for(CoreLabel token: tokens){
			if(result.size() > 0){
				CoreLabel prevToken = result.get(result.size()-1);
				if(prevToken.value().endsWith("[") && token.value().startsWith("*")){
					result.set(result.size()-1, OEUtil.merge(prevToken, token));
				} else if(prevToken.value().endsWith("*") && token.value().startsWith("]")){
					result.set(result.size()-1, OEUtil.merge(prevToken, token));
				} else if(prevToken.value().contains("[*") && !prevToken.value().contains("*]")){ // Use contains instead of endsWith due to "[**2015-10-03**]," case
					result.set(result.size()-1, OEUtil.merge(prevToken, token));
				} else if (prevToken.value().endsWith("'") && token.value().matches("([dDsSmMtT]|ll|LL|re|RE|ve|VE)([^A-Za-z0-9].*)?") && prevToken.endPosition() == token.beginPosition()){
					CoreLabel curToken = OEUtil.merge(prevToken, token);
					result.set(result.size()-1, curToken);
					if(result.size() >= 2){ // Split "can't" into "ca" "n't"
						prevToken = result.get(result.size()-2);
						if (prevToken.value().matches(".*[nN]$") && curToken.value().matches("'[tT]") && prevToken.endPosition() == token.beginPosition()){
							curToken.setBeginPosition(prevToken.endPosition()-1);
							curToken.setValue(prevToken.value().charAt(prevToken.value().length()-1)+curToken.value());
							curToken.setWord(prevToken.value().charAt(prevToken.value().length()-1)+curToken.value());
							curToken.setOriginalText(prevToken.originalText().charAt(prevToken.originalText().length()-1)+curToken.originalText());
							prevToken.setEndPosition(prevToken.endPosition()-1);
							prevToken.setValue(prevToken.value().substring(0, prevToken.value().length()-1));
							prevToken.setWord(prevToken.value().substring(0, prevToken.value().length()-1));
							prevToken.setOriginalText(prevToken.originalText().substring(0, prevToken.originalText().length()-1));
						}
					}
				} else {
					if(token.value().matches("([\"'`][,\\.?!]|[,\\.?!][\"'`]).*")){
						result.addAll(OEUtil.split(token, 1));
					} else {
						result.add(token);
					}
				}
			} else {
				if(token.value().matches("([\"'`][,\\.?!]|[,\\.?!][\"'`]).*")){
					result.addAll(OEUtil.split(token, 1));
				} else {
					result.add(token);
				}
			}
			CoreLabel curToken = result.get(result.size()-1);
			int startIdx = curToken.value().indexOf("[*");
			int endIdx = curToken.value().indexOf("*]");
			if(endIdx >= 0){
				endIdx += 2;
			}
			result.remove(result.size()-1);
			result.addAll(OEUtil.split(curToken, startIdx, endIdx));
		}
		if(normalize){
			for(CoreLabel token: result){
				String normalized = normalizeToken(token.value());
				token.setWord(normalized);
				token.setValue(normalized);
			}
		}
		return result;
	}
	
	public static final String SHORT_DATE = "SHORT_DATE";
	public static final String LONG_DATE = "LONG_DATE";
	public static final String DOCTOR_NAME = "DOCTOR_NAME";
	public static final String HOSPITAL_NAME = "HOSPITAL_NAME";
	public static final String TELEPHONE_NUMBER = "TEL_NO";
	public static final String LOCATION_NAME = "LOCATION_NAME";
	public static final String JOB_NUMBER = "JOB_NUMBER";
	public static final String INFO_TOKEN = "INFO_TOKEN";
	public static final String PERSON_NAME = "PERSON_NAME";
	public static final String DEANONYMIZED_NUMBER = "DEANONYMIZED_NUMBER";
	public static final String DEANONYMIZED_TOKEN = "DEANONYMIZED_TOKEN";
	
	public static String normalizeToken(String token){
		if(token.matches("\\[\\*\\*\\d\\d-\\d\\d\\*\\*\\]")){
			return SHORT_DATE;
		} else if (token.matches("\\[\\*\\*\\d\\d\\d\\d-\\d\\d-\\d\\d\\*\\*\\]")){
			return LONG_DATE;
		} else if (token.matches("(?i)\\[\\*\\*Doctor First Name .*\\*\\*\\]")){
			return DOCTOR_NAME;
		} else if (token.matches("(?i)\\[\\*\\*Hospital .*\\*\\*\\]")){
			return HOSPITAL_NAME;
		} else if (token.matches("(?i)\\[\\*\\*Telephone/Fax .*\\*\\*\\]")){
			return TELEPHONE_NUMBER;
		} else if (token.matches("(?i)\\[\\*\\*(Location|Street Address) .*\\*\\*\\]")){
			return LOCATION_NAME;
		} else if (token.matches("(?i)\\[\\*\\*Job Number .*\\*\\*\\]")){
			return JOB_NUMBER;
		} else if (token.matches("(?i)\\[\\*\\*.*Info.*\\*\\*\\]")){
			return INFO_TOKEN;
		} else if (token.matches("(?i)\\[\\*\\*.*Name.*\\*\\*\\]")){
			return PERSON_NAME;
		} else if (token.matches("(?i)\\[\\*\\*.*Number.*\\*\\*\\]")){
			return DEANONYMIZED_NUMBER;
		} else if (token.matches("\\[\\*.*\\*\\]")){
			return DEANONYMIZED_TOKEN;
		} else {
			return token;
		}
	}
	
	private static enum Argument{
		INPUT(1,
				"The input file to be tokenized. Must also specify -output and -tokenizer",
				"input",
				"inputFile"),
		OUTPUT(1,
				"The output file. Must also specify -input and -tokenizer",
				"output",
				"outputFile"),
		TOKENIZER(1,
				"The tokenizer to be used, either \"regex\" or \"whitespace\"",
				"tokenizer",
				"tokenizerMethod"),
		NO_NORMALIZE(0,
				"Do not normalize the tokens",
				"no_normalize"
				),
		HELP(0,
				"Print this help message",
				"h,help"),
		;
		
		final private int numArgs;
		final private String[] argNames;
		final private String[] names;
		final private String help;
		private Argument(int numArgs, String help, String names, String... argNames){
			this.numArgs = numArgs;
			this.argNames = argNames;
			this.names = names.split(",");
			this.help = help;
		}
		
		/**
		 * Return the Argument which has the specified name
		 * @param name
		 * @return
		 */
		public static Argument argWithName(String name){
			for(Argument argument: Argument.values()){
				for(String argName: argument.names){
					if(argName.equals(name)){
						return argument;
					}
				}
			}
			throw new IllegalArgumentException("Unrecognized argument: "+name);
		}
		
		/**
		 * Print help message
		 */
		private static void printHelp(){
			StringBuilder result = new StringBuilder();
			result.append("Options:\n");
			for(Argument argument: Argument.values()){
				result.append("-"+StringUtils.join(argument.names, " -"));
				result.append(" "+StringUtils.join(argument.argNames, " "));
				result.append("\n");
				if(argument.help != null && argument.help.length() > 0){
					result.append("\t"+argument.help.replaceAll("\n","\n\t")+"\n");
				}
			}
			System.out.println(result.toString());
		}
	}

	public static void main(String[] args) throws IOException{
		String inputFilename = null;
		String outputFilename = null;
		TokenizerMethod tokenizerMethod = null;
		boolean normalize = true;
		int argIndex = 0;
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.length() > 0 && arg.charAt(0) == '-'){
				Argument argument = Argument.argWithName(arg.substring(1));
				switch(argument){
				case INPUT:
					inputFilename = args[argIndex+1];
					break;
				case OUTPUT:
					outputFilename = args[argIndex+1];
					break;
				case TOKENIZER:
					tokenizerMethod = TokenizerMethod.valueOf(args[argIndex+1].toUpperCase());
					break;
				case NO_NORMALIZE:
					normalize = false;
					break;
				case HELP:
					Argument.printHelp();
					System.exit(0);
				}
				argIndex += argument.numArgs+1;
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}
		if(inputFilename != null && outputFilename != null && tokenizerMethod != null){
			Scanner input = new Scanner(new File(inputFilename));
			PrintStream output = new PrintStream(new File(outputFilename));
			System.out.print(String.format("Tokenizing %s into %s using %s...", inputFilename, outputFilename, tokenizerMethod));
			long start = System.currentTimeMillis();
			while(input.hasNextLine()){
				String[] tokens = tokenizeToString(input.nextLine(), tokenizerMethod, normalize);
				output.println(StringUtils.join(tokens, " "));
			}
			long end = System.currentTimeMillis();
			System.out.println(String.format("Done in %.3fs", (end-start)/1000.0));
			input.close();
			output.close();
		} else {
			String[] inputs = new String[]{
					"Lol i mean wah, 又是我做坏人.",
					"Admission Date:  [**2015-03-17**]       Discharge Date:  [**2015-03-24**]",
					};
			for(String input: inputs){
				System.out.printf("Regex\n%s:\n%s\n", input, tokenize_regex(input, normalize));
				System.out.printf("Whitespace\n%s:\n%s\n", input, tokenize_whitespace(input, normalize));
				System.out.printf("Stanford\n%s:\n%s\n", input, tokenize_stanford(input, normalize));
			}
			Scanner sc = new Scanner(System.in);
			while(true){
				System.out.print("Enter text: ");
				String input = sc.nextLine();
				if(input == null){
					break;
				}
				System.out.printf("Regex\n%s:\n%s\n", input, tokenize_regex(input, normalize));
				System.out.printf("Whitespace\n%s:\n%s\n", input, tokenize_whitespace(input, normalize));
				System.out.printf("Stanford\n%s:\n%s\n", input, tokenize_stanford(input, normalize));
			}
			sc.close();
		}
	}
}

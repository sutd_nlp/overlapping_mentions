package com.statnlp.experiment.overlapping;

import java.util.List;

import edu.stanford.nlp.ling.CoreLabel;
import justhalf.nlp.postagger.POSTagger;
import justhalf.nlp.postagger.StanfordPOSTagger;

public class OEPOSTagger {
	
	public static enum POSTaggerMethod {
		STANFORD,
	}
	
	public static POSTagger stanfordPOSTagger;
	
	public static void posTag(List<CoreLabel> words, POSTaggerMethod method){
		switch(method){
		case STANFORD:
			posTag_stanford(words);
			return;
		default:
			throw new UnsupportedOperationException("The tokenizing method "+method+" is not recognized");
		}
	}
	
	public static void posTag_stanford(List<CoreLabel> words){
		if(stanfordPOSTagger == null){
			synchronized (POSTaggerMethod.STANFORD){
				if(stanfordPOSTagger == null){
					stanfordPOSTagger = new StanfordPOSTagger();
				}
			}
		}
		stanfordPOSTagger.tagCoreLabels(words);
	}

}

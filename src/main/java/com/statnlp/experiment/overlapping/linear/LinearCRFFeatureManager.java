/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package com.statnlp.experiment.overlapping.linear;

import static com.statnlp.experiment.overlapping.OEUtil.setupFeatures;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.statnlp.experiment.overlapping.EntitySpan;
import com.statnlp.experiment.overlapping.IFeatureType;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OELemmatizer;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.WordLabel;
import com.statnlp.experiment.overlapping.OELemmatizer.LemmatizerMethod;
import com.statnlp.experiment.overlapping.OEPOSTagger.POSTaggerMethod;
import com.statnlp.experiment.overlapping.WordAttributes.AllAlphanumeric;
import com.statnlp.experiment.overlapping.WordAttributes.AllLowercase;
import com.statnlp.experiment.overlapping.WordAttributes.ContainsDigits;
import com.statnlp.experiment.overlapping.WordAttributes.InitialCaps;
import com.statnlp.experiment.overlapping.WordAttributes.PunctuationMark;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphFeatureManager.ACEFeatureType;
import com.statnlp.experiment.overlapping.linear.LinearCRFNetworkCompiler.NodeType;
import com.statnlp.hybridnetworks.FeatureArray;
import com.statnlp.hybridnetworks.FeatureManager;
import com.statnlp.hybridnetworks.GlobalNetworkParam;
import com.statnlp.hybridnetworks.Network;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.WordShapeClassifier;
import edu.stanford.nlp.util.StringUtils;

/**
 * 
 * @author Aldrian Obaja <aldrianobaja.m@gmail.com>
 *
 */
public class LinearCRFFeatureManager extends FeatureManager{

	private static final long serialVersionUID = -4880581521293400351L;
	
	public int wordHalfWindowSize;
	public int wordNGramSize;
	
	public int bowHalfWindowSize;
	
	public int posHalfWindowSize;
	public int posNGramSize;
	
	public int prefixLength;
	public int suffixLength;
	public boolean wordOnlyLeftWindow;
	public boolean bowOnlyLeftWindow;
	public boolean posOnlyLeftWindow;
	
	public int wordTypeHalfWindowSize;
	public int wordTypeNGramSize;
	
	public boolean isClinicalTask;
	
	public POSTaggerMethod posTaggerMethod;
	public LemmatizerMethod lemmatizerMethod;
	public Map<String, String> brownMap;
	public Map<String, String> umlsSemanticCategory;
	public boolean useSpecificIndicator;
	
	public HashSet<String> _func_words;
	
	public String getNEWordType(CoreLabel word){
		if(word.value().equals("\"") || word.value().equals("\'")){
			return "TYPE_Quote";
		}
		if(_func_words.contains(word.value())){
			return "TYPE_Function";
		}
		if(word.getString(PunctuationMark.class).equals("true")){
			return "TYPE_Punctuation";
		}
		if(word.getString(InitialCaps.class).equals("true")){
			return "TYPE_Capitalized";
		}
		if(word.getString(AllLowercase.class).equals("true")){
			return "TYPE_LowerCase";
		}
		return "TYPE_Other";
	}
	
	public enum FeatureType implements IFeatureType {
		CHEAT(false),
		
		WORDS(true),
		LEXICALIZED_WORDS,
		WORD_NGRAM,
		
		POS_TAG(true),
		POS_TAG_NGRAM,
		
		COMBINED_BOW(true),
		BOW(true),
		
		ORTHOGRAPHIC(true),
		PREFIX(true),
		SUFFIX(true),
		
		WORDSHAPE,
		WORDSHAPE_WINDOW,
		
		WORDTYPE(true),
		WORDTYPE_NGRAM(true),
		
		MENTION_PENALTY(true),
		CONTIGUOUS_MENTION_PENALTY(true),
		DISCONTIGUOUS_MENTION_PENALTY(true),
		
		NOTE_TYPE(true),
		SECTION_NAME(true),
		BROWN_CLUSTER(true),
		SEMANTIC_CATEGORY(true),
		
		TRANSITION(true),
		
		BOUNDARY(true),
		;
		
		private boolean isEnabled;
		
		private FeatureType(){
			this(false);
		}
		
		private FeatureType(boolean isEnabled){
			this.isEnabled = isEnabled;
		}
		
		public void enable(){
			isEnabled = true;
		}
		
		public void disable(){
			isEnabled = false;
		}
		
		public boolean enabled(){
			return isEnabled;
		}
	}
	
	private static enum Argument{
		WORD_HALF_WINDOW_SIZE(1,
				"The half window size for unigram word features",
				"word_half_window_size",
				"n"),
		WORD_NGRAM_SIZE(1,
				"The maximum n-gram size for word features",
				"word_ngram_size",
				"n"),
		WORDTYPE_HALF_WINDOW_SIZE(1,
				"The half window size for unigram wordtype features",
				"wordtype_half_window_size",
				"n"),
		WORDTYPE_NGRAM_SIZE(1,
				"The maximum n-gram size for wordtype features",
				"wordtype_ngram_size",
				"n"),
		BOW_HALF_WINDOW_SIZE(1,
				"The half window size for bag-of-words word features",
				"bow_half_window_size",
				"n"),
		POS_HALF_WINDOW_SIZE(1,
				"The half window size for POS tag features",
				"pos_half_window_size",
				"n"),
		POS_NGRAM_SIZE(1,
				"The maximum n-gram size for POS tag features",
				"pos_ngram_size",
				"n"),
		WORD_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for word features",
				"word_only_left_window"),
		BOW_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for bag-of-words features",
				"bow_only_left_window"),
		POS_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for POS tag features",
				"pos_only_left_window"),
		PREFIX_LENGTH(1,
				"The maximum prefix lengths for word prefix features",
				"prefix_length",
				"n"),
		SUFFIX_LENGTH(1,
				"The maximum suffix lengths for word suffix features",
				"suffix_length",
				"n"),
		HELP(0,
				"Print this help message",
				"h,help"),
		;
		
		final private int numArgs;
		final private String[] argNames;
		final private String[] names;
		final private String help;
		private Argument(int numArgs, String help, String names, String... argNames){
			this.numArgs = numArgs;
			this.argNames = argNames;
			this.names = names.split(",");
			this.help = help;
		}
		
		/**
		 * Return the Argument which has the specified name
		 * @param name
		 * @return
		 */
		public static Argument argWithName(String name){
			for(Argument argument: Argument.values()){
				for(String argName: argument.names){
					if(argName.equals(name)){
						return argument;
					}
				}
			}
			throw new IllegalArgumentException("Unrecognized argument: "+name);
		}
		
		/**
		 * Print help message
		 */
		private static void printHelp(){
			StringBuilder result = new StringBuilder();
			result.append("Options:\n");
			for(Argument argument: Argument.values()){
				result.append("-"+StringUtils.join(argument.names, " -"));
				result.append(" "+StringUtils.join(argument.argNames, " "));
				result.append("\n");
				if(argument.help != null && argument.help.length() > 0){
					result.append("\t"+argument.help.replaceAll("\n","\n\t")+"\n");
				}
			}
			System.out.println(result.toString());
		}
	}

	/**
	 * @param param_g
	 */
	public LinearCRFFeatureManager(GlobalNetworkParam param_g, POSTaggerMethod posTaggerMethod, LemmatizerMethod lemmatizerMethod, Map<String, String> brownMap, Map<String, String> umlsSemanticCategory, String[] features, OEInstance[] trainInstances, boolean useSpecificIndicator, String... args) {
		super(param_g);
		this.isClinicalTask = false;
		this.posTaggerMethod = posTaggerMethod;
		this.lemmatizerMethod = lemmatizerMethod;
		this.brownMap = brownMap;
		this.umlsSemanticCategory = umlsSemanticCategory;
		wordHalfWindowSize = 1;
		wordTypeHalfWindowSize = 1;
		bowHalfWindowSize = 1;
		posHalfWindowSize = 1;
		wordNGramSize = 2;
		wordTypeNGramSize = 2;
		posNGramSize = 2;
		prefixLength = 3;
		suffixLength = 3;
		wordOnlyLeftWindow = false;
		bowOnlyLeftWindow = false;
		posOnlyLeftWindow = false;
		this.useSpecificIndicator = useSpecificIndicator;
		setupFeatures(FeatureType.class, features);
		int argIndex = 0;
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.length() > 0 && arg.charAt(0) == '-'){
				Argument argument = Argument.argWithName(args[argIndex].substring(1));
				switch(argument){
				case WORD_HALF_WINDOW_SIZE:
					wordHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORDTYPE_HALF_WINDOW_SIZE:
					wordTypeHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORD_NGRAM_SIZE:
					wordNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORDTYPE_NGRAM_SIZE:
					wordTypeNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case BOW_HALF_WINDOW_SIZE:
					bowHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case POS_HALF_WINDOW_SIZE:
					posHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case POS_NGRAM_SIZE:
					posNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORD_ONLY_LEFT_WINDOW:
					wordOnlyLeftWindow = true;
					break;
				case BOW_ONLY_LEFT_WINDOW:
					bowOnlyLeftWindow = true;
					break;
				case POS_ONLY_LEFT_WINDOW:
					posOnlyLeftWindow = true;
					break;
				case PREFIX_LENGTH:
					prefixLength = Integer.parseInt(args[argIndex+1]);
					break;
				case SUFFIX_LENGTH:
					suffixLength = Integer.parseInt(args[argIndex+1]);
					break;
				case HELP:
					Argument.printHelp();
					System.exit(0);
				}
				argIndex += argument.numArgs+1;
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}
		findFunctionWords(trainInstances);
	}
	
	private void findFunctionWords(OEInstance[] trainInstances){
		HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
		for(OEInstance instance: trainInstances){
			for(EntitySpan entitySpan: instance.output){
				for(CoreLabel word: instance.inputTokenized){
					if(word.beginPosition() < entitySpan.start() || word.endPosition() > entitySpan.end()){
						continue;
					}
					if(!word.getString(AllAlphanumeric.class).equals("true")){
						continue;
					}
					if(!word.getString(AllLowercase.class).equals("true")){
						continue;
					}
					if(word.getString(ContainsDigits.class).equals("true")){
						continue;
					}
					wordCount.put(word.value(), wordCount.getOrDefault(word.value(), 0)+1);
				}
			}
		}
		_func_words = new HashSet<String>();
		for(String word: wordCount.keySet()){
			if(wordCount.get(word) >= 3){
				_func_words.add(word);
			}
		}
	}

	@Override
	protected FeatureArray extract_helper(Network network, int parent_k, int[] children_k) {
		OENetwork net = (OENetwork)network;
		OEInstance instance = (OEInstance)net.getInstance();
		if(!instance.posTagged){
			instance.posTag(posTaggerMethod, false);
		}
		List<CoreLabel> words = instance.getInputTokenized();
		int size = words.size();
		
		int[] parent_arr = network.getNodeArray(parent_k);
		int[][] childrenNodeArr = new int[children_k.length][];
		for(int i=0; i<children_k.length; i++){
			childrenNodeArr[i] = network.getNodeArray(children_k[i]);
		}

		if(isClinicalTask){
			return extractHelperClinical(network, instance, words, size, parent_arr, childrenNodeArr);
		} else {
			return extractHelperACE(network, instance, words, size, parent_arr, childrenNodeArr);
		}
	}

	private FeatureArray extractHelperClinical(Network network, OEInstance instance, List<CoreLabel> words, int size, int[] parent_arr, int[][] childrenNodeArr) {
		FeatureArray features = new FeatureArray(new int[0]);
		List<Integer> boundaryFeatures = new ArrayList<Integer>();

		int pos = parent_arr[0]-1;
		int tag_id = parent_arr[1]-1;
		int nodeType = parent_arr[4];
		
		if(nodeType == NodeType.LEAF.ordinal() || (nodeType == NodeType.ROOT.ordinal() && pos < size-1)){
			return FeatureArray.EMPTY;
		}
		
		GlobalNetworkParam param_g = this._param_g;
		
		int child_tag_id = childrenNodeArr[0][1]-1;
		
		if(FeatureType.CHEAT.enabled()){
			int cheatFeature = param_g.toFeature(network, FeatureType.CHEAT.name(), tag_id+"", Math.abs(instance.getInstanceId())+" "+pos+" "+child_tag_id);
			return new FeatureArray(new int[]{cheatFeature});
		}
		boolean isLeftBoundary = nodeType != NodeType.ROOT.ordinal() && WordLabel.get(tag_id).form.startsWith("B");
		boolean isRightBoundary = nodeType != NodeType.ROOT.ordinal() && WordLabel.get(tag_id).form.equals("O") && (child_tag_id == -1 || !WordLabel.get(child_tag_id).form.equals("O"));
		// Word window features
		if((FeatureType.LEXICALIZED_WORDS.enabled() || FeatureType.WORDS.enabled()) && nodeType != NodeType.ROOT.ordinal()){
			int wordWindowSize = wordHalfWindowSize*2+1;
			if(wordWindowSize < 0){
				wordWindowSize = 0;
			}
			int[] wordWindowFeatures = new int[wordWindowSize];
			int[] lexWordWindowFeatures = new int[wordWindowSize];
			for(int i=0; i<wordWindowFeatures.length; i++){
				String word = "***";
				int relIdx = (i-wordHalfWindowSize);
				int idx = pos + relIdx;
				if(idx >= 0 && idx < size){
					word = words.get(idx).value();
				}
				if(wordOnlyLeftWindow && idx > pos) continue;
				if(FeatureType.WORDS.enabled()){
					wordWindowFeatures[i] = param_g.toFeature(network, FeatureType.WORDS+":"+relIdx, tag_id+"", word);
				}
				if(FeatureType.LEXICALIZED_WORDS.enabled()){
					lexWordWindowFeatures[i] = param_g.toFeature(network, FeatureType.LEXICALIZED_WORDS+":"+relIdx, tag_id+"", words.get(pos).value()+" "+word);
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.LEXICALIZED_WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", words.get(pos).value()+" "+word));
					}
					if(isRightBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.LEXICALIZED_WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", words.get(pos).value()+" "+word));
					}
				}
			}
			if(FeatureType.WORDS.enabled()){
				features = new FeatureArray(wordWindowFeatures, features);
			}
			if(FeatureType.LEXICALIZED_WORDS.enabled()){
				features = new FeatureArray(lexWordWindowFeatures, features);
			}
		}
		
		// BOW features
		if(FeatureType.BOW.enabled() && nodeType != NodeType.ROOT.ordinal()){
			int bowWindowSize = bowHalfWindowSize*2+1;
			if(bowWindowSize < 0){
				bowWindowSize = 0;
			}
			int[] bowWindowFeatures = new int[bowWindowSize];
			for(int i=0; i<bowWindowFeatures.length; i++){
				String word = "***";
				int relIdx = (i-bowHalfWindowSize);
				int idx = pos + relIdx;
				if(idx >= 0 && idx < size){
					word = words.get(idx).value();
				}
				if(bowOnlyLeftWindow && idx > pos) continue;
				String featureName = FeatureType.BOW.name();
				if(idx < pos){
					featureName += "-before";
				} if(idx > pos){
					featureName += "-after";
				} else {
					continue;
				}
				bowWindowFeatures[i] = param_g.toFeature(network, featureName, tag_id+"", word);
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						featureName = FeatureType.BOW.name();
						if(idx < pos){
							featureName += "-before";
						} if(idx > pos){
							featureName += "-after";
						} else {
							continue;
						}
						featureName += "-"+FeatureType.BOUNDARY+"-LEFT";
						boundaryFeatures.add(param_g.toFeature(network, featureName, tag_id+"", word));
					}
					if(isRightBoundary){
						featureName = FeatureType.BOW.name();
						if(idx < pos){
							featureName += "-before";
						} if(idx > pos){
							featureName += "-after";
						} else {
							continue;
						}
						featureName += "-"+FeatureType.BOUNDARY+"-RIGHT";
						boundaryFeatures.add(param_g.toFeature(network, featureName, tag_id+"", word));
					}
				}
			}
			FeatureArray bowFeatures = new FeatureArray(bowWindowFeatures, features);
			features = bowFeatures;
		}
		
		if(FeatureType.POS_TAG.enabled() && nodeType != NodeType.ROOT.ordinal()){
			int posWindowSize = posHalfWindowSize*2+1;
			if(posWindowSize < 0){
				posWindowSize = 0;
			}
			int[] posWindowFeatures = new int[posWindowSize];
			for(int i=0; i<posWindowFeatures.length; i++){
				String tag = "***";
				int relIdx = (i-posHalfWindowSize);
				int idx = pos + relIdx;
				if(idx >= 0 && idx < size){
					tag = words.get(idx).tag();
				}
				if(posOnlyLeftWindow && idx > pos) continue;
				posWindowFeatures[i] = param_g.toFeature(network, FeatureType.POS_TAG+":"+relIdx, tag_id+"", tag);
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.POS_TAG+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", tag));
					}
					if(isRightBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.POS_TAG+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", tag));
					}
				}
			}
			FeatureArray posFeatures = new FeatureArray(posWindowFeatures, features);
			features = posFeatures;
		}
		
		if(FeatureType.BROWN_CLUSTER.enabled()){
			String brownCluster = getBrownCluster(words.get(pos).value());
			int[] brownClusterFeatures = new int[brownCluster.length()];
			for(int i=0; i<brownClusterFeatures.length; i++){
				brownClusterFeatures[i] = param_g.toFeature(network, FeatureType.BROWN_CLUSTER.name(), tag_id+"", brownCluster.substring(0, i+1));
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.BROWN_CLUSTER.name()+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", brownCluster.substring(0, i+1)));
					}
					if(isRightBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.BROWN_CLUSTER.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", brownCluster.substring(0, i+1)));
					}
				}
			}
			features = new FeatureArray(brownClusterFeatures, features);
		}
		
		if(FeatureType.WORD_NGRAM.enabled()){
			int[] wordNGramFeatures = new int[wordNGramSize*(wordNGramSize+1)/2 - 1];
			for(int ngramSize=2; ngramSize<=wordNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(ngram.length() > 0) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += words.get(idx).value();
						}
					}
					wordNGramFeatures[ngramSize*(ngramSize-1)/2-1+relPos] = param_g.toFeature(network, FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, tag_id+"", ngram);
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							boundaryFeatures.add(param_g.toFeature(network, FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", ngram));
						}
						if(isRightBoundary){
							boundaryFeatures.add(param_g.toFeature(network, FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", ngram));
						}
					}
				}
			}
			features = new FeatureArray(wordNGramFeatures, features);
		}
		
		if(FeatureType.POS_TAG_NGRAM.enabled()){
			int[] posNGramFeatures = new int[posNGramSize*(posNGramSize+1)/2 - 1];
			for(int ngramSize=2; ngramSize<=posNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(idx > pos-ngramSize+relPos+1) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += words.get(idx).tag();
						}
					}
					posNGramFeatures[ngramSize*(ngramSize-1)/2-1+relPos] = param_g.toFeature(network, FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, tag_id+"", ngram);
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							boundaryFeatures.add(param_g.toFeature(network, FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", ngram));
						}
						if(isRightBoundary){
							boundaryFeatures.add(param_g.toFeature(network, FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", ngram));
						}
					}
				}
			}
			features = new FeatureArray(posNGramFeatures, features);
		}
		
		if(FeatureType.ORTHOGRAPHIC.enabled()){
			// Prefix
			String curWord = words.get(pos).value();
			String postag = words.get(pos).tag();
			int[] prefixFeatures = new int[3];
			for(int i=0; i<3; i++){
				String prefix = curWord.substring(0, Math.min(curWord.length(), i+1));
				prefixFeatures[i] = param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-PREFIX", tag_id+"", prefix);
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-PREFIX"+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", prefix));
					}
					if(isRightBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-PREFIX"+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", prefix));
					}
				}
			}
			features = new FeatureArray(prefixFeatures, features);
		
			// Suffix
			int[] suffixFeatures = new int[3];
			for(int i=0; i<3; i++){
				String suffix = curWord.substring(Math.max(0, curWord.length()-i-1), curWord.length());
				suffixFeatures[i] = param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-SUFFIX", tag_id+"", suffix);
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-SUFFIX"+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", suffix));
					}
					if(isRightBoundary){
						boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-SUFFIX"+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", suffix));
					}
				}
			}
			features = new FeatureArray(suffixFeatures, features);
			
			// Capitalization
			boolean isCapitalized = StringUtils.isCapitalized(curWord);
			int capitalizationFeature = param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", tag_id+"", isCapitalized+"");
			if(FeatureType.BOUNDARY.enabled()){
				if(isLeftBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION"+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", isCapitalized+""));
				}
				if(isRightBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION"+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", isCapitalized+""));
				}
			}
			features = new FeatureArray(new int[]{capitalizationFeature}, features);
			
			// Lemma
			String lemma = OELemmatizer.lemmatize(curWord, postag, lemmatizerMethod);
			int lemmaFeature = param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-LEMMA", tag_id+"", lemma);
			if(FeatureType.BOUNDARY.enabled()){
				if(isLeftBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-LEMMA"+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", lemma));
				}
				if(isRightBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.ORTHOGRAPHIC+"-LEMMA"+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", lemma));
				}
			}
			features = new FeatureArray(new int[]{lemmaFeature}, features);
		}
		
		if(FeatureType.NOTE_TYPE.enabled()){
			int noteTypeFeature = param_g.toFeature(network, FeatureType.NOTE_TYPE.name(), tag_id+"", instance.sourceDoc.docType.name());
			if(FeatureType.BOUNDARY.enabled()){
				if(isLeftBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.NOTE_TYPE.name()+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", instance.sourceDoc.docType.name()));
				}
				if(isRightBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.NOTE_TYPE.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", instance.sourceDoc.docType.name()));
				}
			}
			features = new FeatureArray(new int[]{noteTypeFeature}, features);
		}
		
		if(FeatureType.SECTION_NAME.enabled()){
			int sectionNameFeature = param_g.toFeature(network, FeatureType.SECTION_NAME.name(), tag_id+"", instance.sectionName);
			if(FeatureType.BOUNDARY.enabled()){
				if(isLeftBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.SECTION_NAME.name()+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", instance.sectionName));
				}
				if(isRightBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.SECTION_NAME.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", instance.sectionName));
				}
			}
			features = new FeatureArray(new int[]{sectionNameFeature}, features);
		}
		
		if(FeatureType.SEMANTIC_CATEGORY.enabled()){
			String semanticCategory = getUmlsSemanticCategory(words.get(pos).value());
			int semanticCategoryFeature = param_g.toFeature(network, FeatureType.SEMANTIC_CATEGORY.name(), tag_id+"", semanticCategory);
			if(FeatureType.BOUNDARY.enabled()){
				if(isLeftBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.SEMANTIC_CATEGORY.name()+"-"+FeatureType.BOUNDARY+"-LEFT", tag_id+"", semanticCategory));
				}
				if(isRightBoundary){
					boundaryFeatures.add(param_g.toFeature(network, FeatureType.SEMANTIC_CATEGORY.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", tag_id+"", semanticCategory));
				}
			}
			features = new FeatureArray(new int[]{semanticCategoryFeature}, features);
		}
		
		// Label transition feature
		if(FeatureType.TRANSITION.enabled()){
			if(child_tag_id == -1){
				
			} else {
				int transitionFeature = param_g.toFeature(network, FeatureType.TRANSITION.name(), child_tag_id+"-"+tag_id, "");
				features = new FeatureArray(new int[]{transitionFeature}, features);
			}
		}

		int[] featuresInt = new int[boundaryFeatures.size()];
		for(int i=0; i<featuresInt.length; i++){
			featuresInt[i] = boundaryFeatures.get(i);
		}
		features = new FeatureArray(featuresInt, features);
		return features;
	}

	/**
	 * @param network
	 * @param instance
	 * @param words
	 * @param size
	 * @param parent_arr
	 * @param childrenNodeArr
	 * @return
	 */
	private FeatureArray extractHelperACE(Network network, OEInstance instance, List<CoreLabel> words, int size,
			int[] parent_arr, int[][] childrenNodeArr) {
		CoreLabel[] wordsArr = words.toArray(new CoreLabel[words.size()]);
		int pos = parent_arr[0]-1;
		int nodeType = parent_arr[4];
		if(nodeType == NodeType.LEAF.ordinal() || (nodeType == NodeType.ROOT.ordinal() && pos < size-1)){
			return FeatureArray.EMPTY;
		}
		String parentLabel = nodeType == NodeType.ROOT.ordinal() ? "ROOT" : WordLabel.get(parent_arr[1]-1).form;
		
		GlobalNetworkParam param_g = this._param_g;
		
		int childLabelId = childrenNodeArr[0][1]-1;
		int childNodeType = childrenNodeArr[0][4];
		String childLabel = childNodeType == NodeType.LEAF.ordinal() ? "LEAF" : WordLabel.get(childrenNodeArr[0][1]-1).form;
		if(FeatureType.CHEAT.enabled()){
			int cheatFeature = param_g.toFeature(network, FeatureType.CHEAT.name(), parentLabel+"", Math.abs(instance.getInstanceId())+" "+pos+" "+childLabelId);
			return new FeatureArray(new int[]{cheatFeature});
		}
		
		ArrayList<FeatureCandidate> candidates = extractFeatureAtPos(wordsArr, size, pos, parentLabel);
		ArrayList<Integer> features = new ArrayList<Integer>();
		
		if(FeatureType.TRANSITION.enabled()){
			features.add(param_g.toFeature(network, FeatureType.TRANSITION.name(), parentLabel+"-"+childLabel, ""));
		}
		
		for(FeatureCandidate candidate: candidates){
			features.add(param_g.toFeature(network, candidate.type, candidate.output, candidate.input));
		}
		int[] featuresInt = new int[features.size()];
		for(int i=0; i<features.size(); i++){
			featuresInt[i] = features.get(i);
		}
		return new FeatureArray(featuresInt);
	}

	private static class FeatureCandidate{
		public String type;
		public String output;
		public String input;
		
		public FeatureCandidate(String type, String output, String input){
			this.type = type;
			this.output = output;
			this.input = input;
		}
	}

	private ArrayList<FeatureCandidate> extractFeatureAtPos(CoreLabel[] words, int size, int pos, String labelId) {
		ArrayList<FeatureCandidate> featureCandidates = new ArrayList<FeatureCandidate>();
		if(FeatureType.WORDS.enabled()){
			for(int idx=pos-wordHalfWindowSize; idx<=pos+wordHalfWindowSize; idx++){
				String word = "";
				if(idx >= 0 && idx < size){
					word = words[idx].value();
				}
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORD.name()+"["+(idx-pos)+"]", labelId, word));
			}
		}
		if(FeatureType.POS_TAG.enabled()){
			for(int idx=pos-posHalfWindowSize; idx<=pos+posHalfWindowSize; idx++){
				String postag = "";
				if(idx >= 0 && idx < size){
					postag = words[idx].tag();// posTags[idx];
				}
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.POS_TAG.name()+"["+(idx-pos)+"]", labelId, postag));
			}
		}
		if(FeatureType.WORDSHAPE.enabled()){
			for(int idx=pos-1; idx<=pos+1; idx++){
				if(idx < 0 || idx >= size){
					continue;
				}
				String wordshape = WordShapeClassifier.wordShape(words[idx].value(), WordShapeClassifier.WORDSHAPEJENNY1);
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"["+(idx-pos)+"]", labelId, wordshape));
				if(idx < pos+1 && idx+1 < size){
					String nextWordshape = WordShapeClassifier.wordShape(words[idx+1].value(), WordShapeClassifier.WORDSHAPEJENNY1);
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"["+(idx-pos)+","+(idx-pos+1)+"]", labelId, wordshape+"_"+nextWordshape));
				}
				if(idx == pos){
					if(idx-1 >= 0){
						featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORD.name()+"[-1]-"+ACEFeatureType.WORDSHAPE.name()+"[0]", labelId, words[idx-1].value()+"_"+wordshape));
					}
					if(idx+1 < size){
						featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"[0]-"+ACEFeatureType.WORD.name()+"[1]", labelId, wordshape+"_"+words[idx+1].value()));
					}
				}
			}
		}
		if(FeatureType.WORDSHAPE_WINDOW.enabled()){
			for(int idx=pos-wordHalfWindowSize; idx<=pos+wordHalfWindowSize; idx++){
				if(idx < 0 || idx >= size){
					continue;
				}
				String wordshape = WordShapeClassifier.wordShape(words[idx].value(), WordShapeClassifier.WORDSHAPEJENNY1);
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDSHAPE.name()+"["+(idx-pos)+"]", labelId, wordshape));
			}
		}
		if(FeatureType.WORD_NGRAM.enabled()){
			for(int ngramSize=2; ngramSize<=wordNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(ngram.length() > 0) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += words[idx].value();
						}
					}
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORD_NGRAM+"["+ngramSize+","+relPos+"]", labelId, ngram));
				}
			}
		}
		if(FeatureType.POS_TAG_NGRAM.enabled()){
			for(int ngramSize=2; ngramSize<=posNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(idx > pos-ngramSize+relPos+1) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += words[idx].tag(); //posTags[idx];
						}
					}
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.POS_TAG_NGRAM+"["+ngramSize+","+relPos+"]", labelId, ngram));
				}
			}
		}
		if(FeatureType.BOW.enabled()){
			List<String> bowList = new ArrayList<String>();
			for(int idx=pos-bowHalfWindowSize; idx<=pos+bowHalfWindowSize; idx++){
				if(idx >= 0 && idx < size){
					bowList.add(words[idx].value());
				}
			}
			Collections.sort(bowList);
//				String bow = "";
			for(String word: bowList){
//					if(bow.length() > 0) bow += " ";
//					bow += word;
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.BOW.name(), labelId, word));
			}
//				features.add(param_g.toFeature(network, indicator+":"+ACEFeatureType.BOW.name(), labelId, bow));
//				if(childrenNodeArr.length > 1){
//					features.add(param_g.toFeature(network, specificIndicator+":"+ACEFeatureType.BOW.name(), labelId, bow));
//				}
		}
		
		if(FeatureType.WORDTYPE.enabled()){
			for(int idx=pos-wordTypeHalfWindowSize; idx<=pos+wordTypeHalfWindowSize; idx++){
				String word = "";
				if(idx >= 0 && idx < size){
					word = getNEWordType(words[idx]);
				}
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDTYPE.name()+"["+(idx-pos)+"]", labelId, word));
			}
		}
		
		if(FeatureType.WORDTYPE_NGRAM.enabled()){
			for(int ngramSize=2; ngramSize<=wordTypeNGramSize; ngramSize++){
				for(int relPos=0; relPos<ngramSize; relPos++){
					String ngram = "";
					for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
						if(ngram.length() > 0) ngram += " ";
						if(idx >= 0 && idx < size){
							ngram += getNEWordType(words[idx]);
						}
					}
					featureCandidates.add(new FeatureCandidate(ACEFeatureType.WORDTYPE_NGRAM+"["+ngramSize+","+relPos+"]", labelId, ngram));
				}
			}
		}
		
		if(FeatureType.ORTHOGRAPHIC.enabled()){
			for(ACEFeatureType featureType: ACEFeatureType.values()){
				switch(featureType){
				case ALL_CAPS:
				case ALL_DIGITS:
				case ALL_ALPHANUMERIC:
				case ALL_LOWERCASE:
				case CONTAINS_DIGITS:
				case CONTAINS_DOTS:
//					case CONTAINS_HYPHEN:
				case INITIAL_CAPS:
				case LONELY_INITIAL:
				case PUNCTUATION_MARK:
				case ROMAN_NUMBER:
				case SINGLE_CHARACTER:
				case URL:
//						System.err.println(curWord+"["+featureType+"]: "+curWord.get(featureType.cls));
					featureCandidates.add(new FeatureCandidate(featureType.name(), labelId, words[pos].get(featureType.cls)));
					break;
				default:
					break;
				}
			}
		}
		if(FeatureType.PREFIX.enabled()){
			// Prefix
			String curWordString = words[pos].value();
			for(int i=0; i<prefixLength; i++){
				if(i >= curWordString.length()){
					break;
				}
				String prefix = curWordString.substring(0, Math.min(curWordString.length(), i+1));
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.PREFIX+"["+(i+1)+"]", labelId, prefix));
			}
		}

		if(FeatureType.SUFFIX.enabled()){
			// Suffix
			String curWordString = words[pos].value();
			for(int i=0; i<suffixLength; i++){
				if(i >= curWordString.length()){
					break;
				}
				String suffix = curWordString.substring(Math.max(0, curWordString.length()-i-1), curWordString.length());
				featureCandidates.add(new FeatureCandidate(ACEFeatureType.SUFFIX+"["+(i+1)+"]", labelId, suffix));
			}
		}
		
		if(FeatureType.BROWN_CLUSTER.enabled()){
			for(int i=-1; i<=1; i++){
				if(pos+i >= 0 && pos+i < size){
					for(int len: new int[]{4, 6, 9, 15}){
						String cluster = getBrownCluster(words[pos+i].value());
						while(cluster.length() < len){
							cluster += "-";
						}
						cluster = cluster.substring(0, len);
						featureCandidates.add(new FeatureCandidate(ACEFeatureType.BROWN_CLUSTER+"["+i+",len="+len+"]", labelId, cluster));
					}
				}
			}
		}
		return featureCandidates;
	}
	
	private String getBrownCluster(String word){
		if(brownMap == null){
			throw new NullPointerException("Feature requires brown clusters but no brown clusters info is provided");
		}
		String clusterId = brownMap.get(word);
		if(clusterId == null){
			clusterId = "X";
		}
		return clusterId;
	}
	
	private String getUmlsSemanticCategory(String word){
		if(umlsSemanticCategory == null){
			throw new NullPointerException("Feature requires UMLS semantic category but no UMLS semantic category info is provided");
		}
		String semanticCategory = umlsSemanticCategory.get(word);
		if(semanticCategory == null){
			semanticCategory = "General";
		}
		return semanticCategory;
	}
	
	private void writeObject(ObjectOutputStream oos) throws IOException{
		oos.writeObject(posTaggerMethod);
		oos.writeObject(lemmatizerMethod);
		oos.writeObject(brownMap);
		oos.writeObject(umlsSemanticCategory);
		oos.writeObject(_func_words);
		oos.writeInt(wordHalfWindowSize);
		oos.writeInt(wordTypeHalfWindowSize);
		oos.writeInt(bowHalfWindowSize);
		oos.writeInt(posHalfWindowSize);
		oos.writeInt(wordNGramSize);
		oos.writeInt(wordTypeNGramSize);
		oos.writeInt(posNGramSize);
		oos.writeInt(prefixLength);
		oos.writeInt(suffixLength);
		oos.writeBoolean(wordOnlyLeftWindow);
		oos.writeBoolean(bowOnlyLeftWindow);
		oos.writeBoolean(posOnlyLeftWindow);
		oos.writeBoolean(useSpecificIndicator);
		oos.writeInt(FeatureType.values().length);
		for(FeatureType featureType: FeatureType.values()){
			oos.writeObject(featureType.name());
			oos.writeBoolean(featureType.isEnabled);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException{
		posTaggerMethod = (POSTaggerMethod)ois.readObject();
		lemmatizerMethod = (LemmatizerMethod)ois.readObject();
		brownMap = (Map<String, String>)ois.readObject();
		umlsSemanticCategory = (Map<String, String>)ois.readObject();
		_func_words = (HashSet<String>)ois.readObject();
		wordHalfWindowSize = ois.readInt();
		wordTypeHalfWindowSize = ois.readInt();
		bowHalfWindowSize = ois.readInt();
		posHalfWindowSize = ois.readInt();
		wordNGramSize = ois.readInt();
		wordTypeNGramSize = ois.readInt();
		posNGramSize = ois.readInt();
		prefixLength = ois.readInt();
		suffixLength = ois.readInt();
		wordOnlyLeftWindow = ois.readBoolean();
		bowOnlyLeftWindow = ois.readBoolean();
		posOnlyLeftWindow = ois.readBoolean();
		useSpecificIndicator = ois.readBoolean();
		int numFeatureTypes = ois.readInt();
		for(int i=0; i<numFeatureTypes; i++){
			FeatureType featureType = FeatureType.valueOf((String)ois.readObject());
			featureType.isEnabled = ois.readBoolean();
		}
	}

}

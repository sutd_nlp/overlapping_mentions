/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package com.statnlp.experiment.overlapping.linear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.statnlp.commons.types.Instance;
import com.statnlp.experiment.overlapping.EntitySpan;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.OEUtil;
import com.statnlp.experiment.overlapping.SpanLabel;
import com.statnlp.experiment.overlapping.WordLabel;
import com.statnlp.experiment.overlapping.OEUtil.LabelInterpretation;
import com.statnlp.hybridnetworks.LocalNetworkParam;
import com.statnlp.hybridnetworks.Network;
import com.statnlp.hybridnetworks.NetworkCompiler;
import com.statnlp.hybridnetworks.NetworkIDMapper;

public class ParallelLinearCRFNetworkCompiler extends NetworkCompiler{
	
	private static final long serialVersionUID = -3829680998638818730L;
	
	public static final boolean DEBUG = false;
	
	public WordLabel[] _labels;
	public SpanLabel[] _spanLabels;
	public boolean useBILOU;
	public boolean ignoreOverlaps;
	public enum NodeType {LEAF, NODE, TYPE, ROOT};
	private static int MAX_LENGTH = 500;
	
	private transient long[] _allNodes;
	private transient int[][][] _allChildren;
	
	public LabelInterpretation labelInterpretation;
	public int maxSize;
	
	public ParallelLinearCRFNetworkCompiler(WordLabel[] labels, SpanLabel[] spanLabels, int maxLength, LabelInterpretation labelInterpretation, boolean useBILOU, boolean ignoreOverlaps){
		this._labels = labels;
		this._spanLabels = spanLabels;
		this.maxSize = Math.max(MAX_LENGTH, maxLength);
		this.compile_unlabeled_generic();
		this.labelInterpretation = labelInterpretation;
		this.useBILOU = useBILOU;
		this.ignoreOverlaps = ignoreOverlaps;
		compile_unlabeled_generic();
	}
	
	@Override
	public OENetwork compile(int networkId, Instance instance, LocalNetworkParam param) {
		OEInstance inst = (OEInstance) instance;
		if(inst.isLabeled()){
			return this.compile_labeled(networkId, inst, param);
		} else {
			return this.compile_unlabeled(networkId, inst, param);
		}
		
	}
	
	private List<EntitySpan> filterEntities(List<EntitySpan> entities, SpanLabel type){
		List<EntitySpan> result = new ArrayList<EntitySpan>();
		for(EntitySpan entity: entities){
			if(entity.label.equals(type)){
				result.add(entity);
			}
		}
		return result;
	}
	
	private OENetwork compile_labeled(int networkId, OEInstance inst, LocalNetworkParam param){
		OENetwork network = new OENetwork(networkId, inst, param, this);
		List<EntitySpan> outputs = inst.getOutput();
		int size = inst.getInputTokenized().size();
		
		// Add leaf
		long leaf = toNode_leaf();
		network.addNode(leaf);
		
		long prevNode = leaf;
		
		long[] typeNodes = new long[this._spanLabels.length];
		
		for(int spanLabelIdx=0; spanLabelIdx<_spanLabels.length; spanLabelIdx++){
			long typeNode = toNode_type(0, size, this._spanLabels[spanLabelIdx].id);
			network.addNode(typeNode);
			typeNodes[spanLabelIdx] = typeNode;
			prevNode = typeNode;
			List<EntitySpan> filteredOutput = filterEntities(outputs, _spanLabels[spanLabelIdx]);
			List<WordLabel> tokenizedOutput = OEUtil.spansToLabels(filteredOutput, inst.wordSpans, ignoreOverlaps, useBILOU);
			for(int pos=0; pos<size; pos++){
				int wordLabelId = tokenizedOutput.get(pos).id;
				long node = toNode(pos, size, wordLabelId, this._spanLabels[spanLabelIdx].id);
				network.addNode(node);
				network.addEdge(prevNode, new long[]{node});
				prevNode = node;
			}
			network.addEdge(prevNode, new long[]{leaf});
		}
		
		// Add root
		long root = toNode_root(0, size);
		network.addNode(root);
		network.addEdge(root, typeNodes);
		
		network.finalizeNetwork();
		
		if(DEBUG){
			OENetwork unlabeled = compile_unlabeled(networkId, inst, param);
			System.out.println(inst);
			System.out.println(inst.wordSpans);
			System.out.println(outputs);
			for(SpanLabel label: this._spanLabels){
				System.out.println(label);
				System.out.println(OEUtil.spansToLabels(filterEntities(outputs, label), inst.wordSpans, ignoreOverlaps, useBILOU));
			}
			System.out.println(network);
			System.out.println(unlabeled.contains(network));
			System.out.println(unlabeled);
		}
		
		return network;
	}

	private OENetwork compile_unlabeled(int networkId, OEInstance inst, LocalNetworkParam param){
		if(_allNodes == null){
			compile_unlabeled_generic();
		}
		int size = inst.getInputTokenized().size();
		long root = this.toNode_root(0, size);
		
		int pos = Arrays.binarySearch(this._allNodes, root);
		int numNodes = pos+1; // Num nodes should equals to (instanceSize * (numLabels+1)) + 1
//		System.out.println(String.format("Instance size: %d, Labels size: %d, numNodes: %d", size, _labels.size(), numNodes));
		
		OENetwork result = new OENetwork(networkId, inst, this._allNodes, this._allChildren, param, numNodes, this);
		return result;
	}
	
	private void compile_unlabeled_generic(){
		OENetwork network = new OENetwork();
		
		long leaf = this.toNode_leaf();
		network.addNode(leaf);
		
		ArrayList<Long> prevNodes = new ArrayList<Long>();
		ArrayList<Long> currNodes = new ArrayList<Long>();
		
		for(int i=0; i<this._spanLabels.length; i++){
			prevNodes.clear();
			currNodes.clear();
			int labelId = this._spanLabels[i].id;
			long typeNode = toNode_type(0, maxSize, labelId);
			network.addNode(typeNode);
			prevNodes.add(typeNode);
			
			for(int pos = 0; pos <maxSize; pos++){
				for(int tag_idx = 0; tag_idx < this._labels.length; tag_idx++){
					WordLabel wordLabel = this._labels[tag_idx];
					if(!wordLabel.form.matches("(O|[BILU]([DH])?-"+this._spanLabels[i].form+")")){
						continue;
					}
					long node = this.toNode(pos, maxSize, wordLabel.id, labelId);
					currNodes.add(node);
					network.addNode(node);
					for(long prevNode : prevNodes){
//						int[] prevNodeArr = NetworkIDMapper.toHybridNodeArray(prevNode);
//						System.err.println(
//								String.format("Pos: %d, WordLabel: %s, SpanLabel: %s, NodeType: %s",
//											maxSize-prevNodeArr[0]-1,
//											prevNodeArr[1] >= this._labels.length ? "NONE" : WordLabel.get(prevNodeArr[1]),
//											prevNodeArr[2] >= this._spanLabels.length ? "NONE" : SpanLabel.get(prevNodeArr[2]),
//											NodeType.values()[prevNodeArr[4]]));
//						prevNodeArr = NetworkIDMapper.toHybridNodeArray(node);
//						System.err.println(
//								String.format("Pos: %d, WordLabel: %s, SpanLabel: %s, NodeType: %s",
//											maxSize-prevNodeArr[0]-1,
//											prevNodeArr[1] >= this._labels.length ? "NONE" : WordLabel.get(prevNodeArr[1]),
//											prevNodeArr[2] >= this._spanLabels.length ? "NONE" : SpanLabel.get(prevNodeArr[2]),
//											NodeType.values()[prevNodeArr[4]]));
//						System.err.println();
						network.addEdge(prevNode, new long[]{node});
					}
				}
				if(pos < maxSize-1){
					typeNode = toNode_type(pos+1, maxSize, labelId);
					network.addNode(typeNode);
					currNodes.add(typeNode);
				}
				prevNodes = currNodes;
				currNodes = new ArrayList<Long>();
			}
			
			for(long prevNode: prevNodes){
				network.addEdge(prevNode, new long[]{leaf});
			}
		}
		
		for(int pos=0; pos<maxSize; pos++){
			long root = toNode_root(pos, maxSize);
			network.addNode(root);
			long[] typeNodes = new long[this._spanLabels.length];
			for(int i=0; i<typeNodes.length; i++){
				typeNodes[i] = toNode_type(pos, maxSize, this._spanLabels[i].id);
			}
			network.addEdge(root, typeNodes);
		}
		
		network.finalizeNetwork();
		
		this._allNodes = network.getAllNodes();
		this._allChildren = network.getAllChildren();
		
	}
	
	private long toNode_leaf(){
		int[] arr = new int[]{0, 0, 0, 0, NodeType.LEAF.ordinal()};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode(int pos, int size, int tag_id, int span_id){
		int[] arr = new int[]{size-pos-1, tag_id, span_id, 0, NodeType.NODE.ordinal()};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode_type(int pos, int size, int span_id){
		int[] arr = new int[]{size-pos-1, this._labels.length, span_id, 0, NodeType.TYPE.ordinal()};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode_root(int pos, int size){
		int[] arr = new int[]{size-pos-1, this._labels.length, this._spanLabels.length, 0, NodeType.ROOT.ordinal()};
		return NetworkIDMapper.toHybridNodeID(arr);
	}

	
	@Override
	public OEInstance decompile(Network network) {
		OENetwork ctNetwork = (OENetwork)network;
		OEInstance instance = (OEInstance)ctNetwork.getInstance();
		int size = instance.getInputTokenized().size();
		
		OEInstance result = instance.duplicate();
		List<EntitySpan> predictions = new ArrayList<EntitySpan>();
		long superRoot = toNode_root(0, size);
		int rootNode_k = Arrays.binarySearch(_allNodes, superRoot);
		for(int node_k: ctNetwork.getMaxPath(rootNode_k)){
			List<WordLabel> predictionForms = new ArrayList<WordLabel>();
			for(int i=0; i<size; i++){
				int[] children_k = ctNetwork.getMaxPath(node_k);
				int child_k = children_k[0];
				long child = ctNetwork.getNode(child_k);
				int[] child_arr = NetworkIDMapper.toHybridNodeArray(child);
				int wordLabelId = child_arr[1];
//				System.out.println(
//						String.format("Pos: %d, WordLabel: %s, SpanLabel: %s, NodeType: %s",
//								size-child_arr[0]-1,
//								WordLabel.get(wordLabelId),
//								SpanLabel.get(child_arr[2]),
//								NodeType.values()[child_arr[4]]));
				predictionForms.add(WordLabel.get(wordLabelId));
				node_k = child_k;
			}
			predictions.addAll(OEUtil.labelsToSpans(predictionForms, result.wordSpans, result.input, labelInterpretation));
		}
		
		result.setPrediction(predictions);
		
		return result;
	}
	

}

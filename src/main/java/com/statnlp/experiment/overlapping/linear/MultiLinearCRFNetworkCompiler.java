/** Statistical Natural Language Processing System
    Copyright (C) 2014-2015  Lu, Wei

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package com.statnlp.experiment.overlapping.linear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.statnlp.commons.types.Instance;
import com.statnlp.experiment.overlapping.EntitySpan;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.Span;
import com.statnlp.experiment.overlapping.SpanLabel;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphNetworkCompiler.NetworkInterpretation;
import com.statnlp.experiment.overlapping.multigraph.MultigraphNetworkCompiler.OrigNodeType;
import com.statnlp.hybridnetworks.LocalNetworkParam;
import com.statnlp.hybridnetworks.Network;
import com.statnlp.hybridnetworks.NetworkCompiler;
import com.statnlp.hybridnetworks.NetworkIDMapper;

import edu.stanford.nlp.ling.CoreLabel;

public class MultiLinearCRFNetworkCompiler extends NetworkCompiler{
	
	private static final long serialVersionUID = -3829680998638818730L;
	
	static {
		NetworkIDMapper.setCapacity(new int[]{1000, 1000, 1000, 1000}); // Node type, position, label ID, count
	}
	
	public static final boolean DEBUG = false;
	
	public SpanLabel[] _labels;
	public enum NodeType {LEAF, STATE, TYPE, ROOT};
	public enum StateType {
		NONE, START, END, END_START, CONT, CONT_START, CONT_END, CONT_END_START;
		
		public boolean hasStart;
		public boolean hasEnd;
		public boolean hasCont;
		
		private StateType(){
			int state = this.ordinal();
			hasStart = state % 2 == 1;
			hasEnd = (state/2) % 2 == 1;
			hasCont = state >= 4;
		}
		};
	private static int MAX_LENGTH = 500;
	
	private transient long[] _allNodes;
	private transient int[][][] _allChildren;
	
	public NetworkInterpretation networkInterpretation;
	public int maxSize;
	
	public MultiLinearCRFNetworkCompiler(SpanLabel[] labels, int maxLength, NetworkInterpretation networkInterpretation){
		this._labels = labels;
		this.maxSize = Math.max(MAX_LENGTH, maxLength);
		this.compile_unlabled_generic();
		this.networkInterpretation = networkInterpretation;
	}
	
	@Override
	public OENetwork compile(int networkId, Instance instance, LocalNetworkParam param) {
		OEInstance inst = (OEInstance) instance;
		if(inst.isLabeled()){
			return this.compile_labeled(networkId, inst, param);
		} else {
			return this.compile_unlabeled(networkId, inst, param);
		}
		
	}
	
	
	private OENetwork compile_labeled(int networkId, OEInstance inst, LocalNetworkParam param){
		OENetwork network = new OENetwork(networkId, inst, param, this);
		List<EntitySpan> outputs = inst.getOutput();
		List<CoreLabel> words = inst.getInputTokenized();
		int size = words.size();
		
		// Add leaf
		long leaf = toNode_leaf();
		network.addNode(leaf);
		
		long[] typeNodes = new long[this._labels.length];
		for(SpanLabel label: this._labels){
			long typeNode = toNode_type(0, size, label.id);
			network.addNode(typeNode);
			typeNodes[label.id] = typeNode;
		}
		long root = toNode_root(0, size);
		network.addNode(root);
		network.addEdge(root, typeNodes);

		
		int[][] typePosState = new int[this._labels.length][size+1];
		for(int i=0; i<this._labels.length; i++){
			for(int j=0; j<size+1; j++){
				typePosState[this._labels[i].id][j] = 0;
			}
		}
		
		for(EntitySpan span: outputs){
			EntitySpan tokenSpan = toTokenSpan(span, words);
			int labelId = tokenSpan.label.id;
			typePosState[labelId][tokenSpan.start()] |= 1;
			typePosState[labelId][tokenSpan.end()] |= 1 << 1;
			for(int pos=tokenSpan.start()+1; pos<tokenSpan.end(); pos++){
				typePosState[labelId][pos] |= 1 << 2;
			}
		}

		for(int i=0; i<this._labels.length; i++){
			int labelId = this._labels[i].id;
			long prevNode = toNode_type(0, size, labelId);
			network.addNode(prevNode);
			for(int pos=0; pos<size+1; pos++){
				long node = toNode_state(pos, size, labelId, typePosState[labelId][pos]);
				network.addNode(node);
				network.addEdge(prevNode, new long[]{node});
				prevNode = node;
			}
			network.addEdge(prevNode, new long[]{leaf});
		}
		
		network.finalizeNetwork();
		
		if(DEBUG){
			OENetwork unlabeled = compile_unlabeled(networkId, inst, param);
			System.out.println(inst);
			System.out.println(inst.wordSpans);
			System.out.println(outputs);
			System.out.println(network);
			System.out.println(unlabeled.contains(network));
		}
		
		return network;
	}
	
	private static EntitySpan toTokenSpan(EntitySpan span, List<CoreLabel> words){
		int start = -1;
		int end = -1;
		for(int i=0; i<words.size(); i++){
			CoreLabel word = words.get(i);
			if(word.endPosition() > span.start() && start == -1){
				start = i;
			}
			if(word.beginPosition() < span.end()){
				end = i+1;
			}
		}
		if(start == -1 || end == -1){
			System.out.println("Mention ["+span.start()+","+span.end()+"] not found in ["+words.get(0).beginPosition()+","+words.get(words.size()-1).endPosition()+"]");
			System.out.print("[");
			for(CoreLabel token: words){
				System.out.print(token.value()+"("+token.beginPosition()+","+token.endPosition()+") ");
			}
			System.out.println("]");
		}
		EntitySpan tokenSpan = new EntitySpan(span);
		tokenSpan.spans = new Span[]{new Span(start, end)};
		return tokenSpan;
	}

	private OENetwork compile_unlabeled(int networkId, OEInstance inst, LocalNetworkParam param){
		if(_allNodes == null){
			compile_unlabled_generic();
		}
		int size = inst.getInputTokenized().size();
		long root = this.toNode_root(0, size);
		
		int pos = Arrays.binarySearch(this._allNodes, root);
		int numNodes = pos+1; // Num nodes should equals to (instanceSize * (numLabels+1)) + 1
//		System.out.println(String.format("Instance size: %d, Labels size: %d, numNodes: %d", size, _labels.size(), numNodes));
		
		return new OENetwork(networkId, inst, this._allNodes, this._allChildren, param, numNodes, this);
		
	}
	
	private void compile_unlabled_generic(){
		OENetwork network = new OENetwork();
		
		long leaf = this.toNode_leaf();
		network.addNode(leaf);
		int size = maxSize;
		
		for(int pos = 0; pos <maxSize; pos++){
			long[] typeNodes = new long[this._labels.length];
			for(int i = 0; i < this._labels.length; i++){
				int labelId = this._labels[i].id;
				for(StateType state: StateType.values()){
					long node = toNode_state(pos, size, labelId, state.ordinal());
					if(!network.contains(node)){
						network.addNode(node);
					}
					StateType[] nextStates = null;
					switch(state){
					case NONE:
					case END:
						nextStates = new StateType[]{StateType.NONE, StateType.START};
						break;
					default:
						nextStates = new StateType[]{StateType.END, StateType.END_START, StateType.CONT, StateType.CONT_START, StateType.CONT_END, StateType.CONT_END_START};
					}
					for(StateType nextState: nextStates){
						if(pos+1 == size && (nextState != StateType.NONE && nextState != StateType.END)){
							continue;
						}
						long nextNode = toNode_state(pos+1, size, labelId, nextState.ordinal());
						if(!network.contains(nextNode)){
							network.addNode(nextNode);
						}
						network.addEdge(node, new long[]{nextNode});
					}
				}
				long[] startStates = new long[]{toNode_state(pos, size, labelId, StateType.NONE.ordinal()), toNode_state(pos, size, labelId, StateType.START.ordinal())};
				long typeNode = toNode_type(pos, size, labelId);
				network.addNode(typeNode);
				typeNodes[labelId] = typeNode;
				for(long startState: startStates){
					network.addEdge(typeNode, new long[]{startState});
				}
			}
			long root = toNode_root(pos, size);
			network.addNode(root);
			network.addEdge(root, typeNodes);
		}
		for(int i = 0; i < this._labels.length; i++){
			int labelId = this._labels[i].id;
			network.addEdge(toNode_state(size, size, labelId, StateType.NONE.ordinal()), new long[]{leaf});
			network.addEdge(toNode_state(size, size, labelId, StateType.END.ordinal()), new long[]{leaf});
		}
		
		network.finalizeNetwork();
		
		this._allNodes = network.getAllNodes();
		this._allChildren = network.getAllChildren();
		
	}
	
	private long toNode_leaf(){
		int[] arr = new int[]{0, 0, 0, 0};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode_state(int pos, int size, int tag_id, int state){
		int[] arr = new int[]{size-pos, tag_id, NodeType.STATE.ordinal(), state};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode_type(int pos, int size, int tag_id){
		int[] arr = new int[]{size-pos, tag_id, NodeType.TYPE.ordinal(), StateType.values().length};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private long toNode_root(int pos, int size){
		int [] arr = new int[]{size-pos, this._labels.length, NodeType.ROOT.ordinal(), StateType.values().length};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	@Override
	public OEInstance decompile(Network net) {
		OENetwork network = (OENetwork)net;
		OENetwork networkOrig = new OENetwork(net.getNetworkId(), net.getInstance(), ((OENetwork)net).getParam(), this);
		long rootNode = network.getRoot();
		int rootNode_k = network.getNodeIndex(rootNode);
		int size = network.getNodeArray(rootNode_k)[0];
		int[] typeNodes_k = network.getMaxPath(rootNode_k);
		networkOrig.addNode(toNode_X());
		networkOrig.addNode(toNode_A(0, size));
		networkOrig.addNode(toNode_E(0, size));
		for(int i=1; i<size; i++){
			networkOrig.addNode(toNode_A(i, size));
			networkOrig.addNode(toNode_E(i, size));
			networkOrig.addEdge(toNode_A(i-1, size), new long[]{toNode_E(i-1, size), toNode_A(i, size)});
		}
		networkOrig.addEdge(toNode_A(size-1, size), new long[]{toNode_E(size-1, size)});
		for(int typeNode_k: typeNodes_k){
			int[] typeNodeArr = network.getNodeArray(typeNode_k);
			int labelId = typeNodeArr[1];
			int curNode_k = typeNode_k;
			for(int pos=0; pos<size+1; pos++){
				curNode_k = network.getMaxPath(curNode_k)[0];
				int[] stateNodeArr = network.getNodeArray(curNode_k);
				StateType state = StateType.values()[stateNodeArr[3]];
				long typeNode = toNode_T(pos, size, labelId);
				long bNode = toNode_B(pos, size, 0, labelId);
				long prevBNode = toNode_B(pos-1, size, 0, labelId);
				if(pos < size){
					networkOrig.addNode(typeNode);
				}
				if(state.hasStart){
					if(!networkOrig.contains(bNode)){
						networkOrig.addNode(bNode);
					}
					networkOrig.addEdge(typeNode, new long[]{bNode});
				} else if(pos < size){
					networkOrig.addEdge(typeNode, new long[]{toNode_X()});
				}
				if(state.hasCont){
					if(!networkOrig.contains(bNode)){
						networkOrig.addNode(bNode);
					}
					if(!networkOrig.contains(prevBNode)){
						networkOrig.addNode(prevBNode);
					}
					if(state.hasEnd){
						networkOrig.addEdge(prevBNode, new long[]{toNode_X(), bNode});
					} else {
						networkOrig.addEdge(prevBNode, new long[]{bNode});
					}
				} 
				if(state.hasEnd){
					if(!networkOrig.contains(prevBNode)){
						networkOrig.addNode(prevBNode);
					}
					networkOrig.addEdge(prevBNode, new long[]{toNode_X()});
				}
			}
		}
		for(int pos=0; pos<size; pos++){
			long[] typeNodes = new long[this._labels.length];
			for(int i=0; i<this._labels.length; i++){
				int labelId = this._labels[i].id;
				typeNodes[labelId] = toNode_T(pos, size, labelId);
			}
			networkOrig.addEdge(toNode_E(pos, size), typeNodes);
		}
		networkOrig.finalizeNetwork();
		try{
			return decompileOrig(networkOrig);
		} catch (Exception e){
			System.out.println(network.getInstance());
			System.out.println(networkOrig);
			throw e;
		}
	}

	
	private long toNode_X(){
		return toNode_orig(0, 1, OrigNodeType.X_NODE, 0, 0);
	}
	
	private long toNode_B(int pos, int size, int bodyIndex, int labelId){
		return toNode_orig(pos, size, OrigNodeType.B_NODE, bodyIndex, labelId);
	}

	@SuppressWarnings("unused")
	private long toNode_O(int pos, int size, int bodyIndex, int labelId){
		return toNode_orig(pos, size, OrigNodeType.O_NODE, bodyIndex, labelId);
	}
	
	private long toNode_T(int pos, int size, int labelId){
		return toNode_orig(pos, size, OrigNodeType.T_NODE, 9, labelId);
	}
	
	private long toNode_E(int pos, int size){
		return toNode_orig(pos, size, OrigNodeType.E_NODE, 9, 0);
	}
	
	private long toNode_A(int pos, int size){
		return toNode_orig(pos, size, OrigNodeType.A_NODE, 9, 0);
	}
	
	protected int[] toNodeArr_X(){
		return toNodeArr_orig(0, 1, OrigNodeType.X_NODE, 0, 0);
	}
	
	protected int[] toNodeArr_B(int pos, int size, int bodyIndex, int labelId){
		return toNodeArr_orig(pos, size, OrigNodeType.B_NODE, bodyIndex, labelId);
	}

	protected int[] toNodeArr_O(int pos, int size, int bodyIndex, int labelId){
		return toNodeArr_orig(pos, size, OrigNodeType.O_NODE, bodyIndex, labelId);
	}
	
	protected int[] toNodeArr_T(int pos, int size, int labelId){
		return toNodeArr_orig(pos, size, OrigNodeType.T_NODE, 9, labelId);
	}
	
	protected int[] toNodeArr_E(int pos, int size){
		return toNodeArr_orig(pos, size, OrigNodeType.E_NODE, 9, 0);
	}
	
	protected int[] toNodeArr_A(int pos, int size){
		return toNodeArr_orig(pos, size, OrigNodeType.A_NODE, 9, 0);
	}
	
	private long toNode_orig(int pos, int size, OrigNodeType nodeType, int bodyIndex, int labelId){
		int[] arr = new int[]{size-pos-1, bodyIndex, nodeType.ordinal(), labelId};
		return NetworkIDMapper.toHybridNodeID(arr);
	}
	
	private int[] toNodeArr_orig(int pos, int size, OrigNodeType nodeType, int bodyIndex, int labelId){
		return new int[]{size-pos-1, bodyIndex, nodeType.ordinal(), labelId};
	}
	
	private static class JunctionInfo implements Comparable<JunctionInfo>{
		public int highestUnprocessedChildIdx;
		public int startIdx;
		public SpanLabel label;
		public List<OrigNodeType> nodeSeq;
		public int node_k;
		
		public JunctionInfo(int node_k, int highestIdx, int startIdx, SpanLabel label, List<OrigNodeType> nodeSeq){
			this.node_k = node_k;
			this.highestUnprocessedChildIdx = highestIdx;
			this.startIdx = startIdx;
			this.label = label;
			this.nodeSeq = new ArrayList<OrigNodeType>(nodeSeq);
		}
		
		public int hashCode(){
			return 31*Integer.hashCode(node_k) + label.hashCode();
//			return 4*Integer.hashCode(node_k) + 2*Integer.hashCode(startIdx) + label.hashCode();
		}
		
		public boolean equals(Object obj){
			if(obj instanceof JunctionInfo){
				JunctionInfo info = (JunctionInfo)obj;
				if(node_k != info.node_k) return false;
//				if(startIdx != info.startIdx) return false;
				if(label != info.label) return false;
				return true;
			}
			return false;
		}

		@Override
		public int compareTo(JunctionInfo info) {
			if(this.node_k < info.node_k) return -1;
			if(this.node_k > info.node_k) return 1;
//			if(this.startIdx < info.startIdx) return -1;
//			if(this.startIdx > info.startIdx) return 1;
			return this.label.compareTo(info.label);
		}
	}

	private static EntitySpan createEntitySpan(int start, List<OrigNodeType> nodeSeq, List<CoreLabel> inputTokenized, SpanLabel label){
		List<Span> spans = new ArrayList<Span>();
		int startIdx = -1;
		for(int i=0; i<nodeSeq.size(); i++){
			OrigNodeType curNode = nodeSeq.get(i);
			if(curNode == OrigNodeType.O_NODE){
				startIdx = -1;
				continue;
			}
			if(startIdx == -1){
				startIdx = inputTokenized.get(start+i).beginPosition();
			}
			
			if(shouldCreateNewSpan(start, i, nodeSeq, inputTokenized)){
				spans.add(new Span(startIdx, inputTokenized.get(start+i).endPosition()));
				startIdx = -1;
			}
		}
		return new EntitySpan(null, label, spans.toArray(new Span[spans.size()]));
	}
	
	private static boolean shouldCreateNewSpan(int start, int i, List<OrigNodeType> nodeSeq, List<CoreLabel> inputTokenized){
		if(i == nodeSeq.size()-1){
			return true;
		}
		OrigNodeType next = nodeSeq.get(i+1);
		if(next == OrigNodeType.O_NODE){
			return true;
		}
		return false;
	}
	
	public OEInstance decompileOrig(OENetwork network) {
		OEInstance result = (OEInstance)network.getInstance().duplicate();
		List<CoreLabel> inputTokenized = result.getInputTokenized();
		int size = inputTokenized.size();
		
		List<EntitySpan> prediction = new ArrayList<EntitySpan>();
		long[] nodes = network.getAllNodes();
		List<JunctionInfo> unfinishedJunctions = new ArrayList<JunctionInfo>();
		Set<JunctionInfo> usedJunctions = new TreeSet<JunctionInfo>();
		long aNode = network.getRoot();
		int aNode_k = Arrays.binarySearch(nodes, aNode);
		for(int pos=0; pos<size; pos++){
			int[] curChildren = network.getChildren(aNode_k)[0];
			int eNode_k = curChildren[0];
			int[] curTNodes = network.getChildren(eNode_k)[0];
			for(int idx=0; idx<curTNodes.length; idx++){
				int tNode_k = curTNodes[idx];
				int[] tNode_arr = network.getNodeArray(tNode_k);
				int labelId = tNode_arr[3];
				SpanLabel label = SpanLabel.get(labelId);
				int[] child = network.getChildren(tNode_k)[0];
				int node_k = child[0]; // either an B-node or an X-node
				int[] node_arr = network.getNodeArray(node_k);
				OrigNodeType nodeType = OrigNodeType.values()[node_arr[2]];
				if(nodeType == OrigNodeType.B_NODE){ // Has a mention starting here
					List<OrigNodeType> nodeSeq = new ArrayList<OrigNodeType>();
					traverseMaxNetwork(network, inputTokenized, prediction, unfinishedJunctions, usedJunctions, nodeSeq, pos, node_k,
							nodeType, label);
				}
			}
			if(curChildren.length == 2){
				aNode_k = curChildren[1];
			}
		}
		int junctionIndex = 0;
		if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
			junctionIndex = unfinishedJunctions.size()-1;
		}
		while(junctionIndex < unfinishedJunctions.size() && junctionIndex >= 0){
			JunctionInfo junction = unfinishedJunctions.get(junctionIndex);
			List<OrigNodeType> nodeSeq = new ArrayList<OrigNodeType>(junction.nodeSeq);
			int start = junction.startIdx;
			int[] child = network.getChildren(junction.node_k)[0];
			int node_k = child[junction.highestUnprocessedChildIdx];
			if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
				junction.highestUnprocessedChildIdx -= 1;
				if(junction.highestUnprocessedChildIdx == -1){
					usedJunctions.add(unfinishedJunctions.remove(junctionIndex));
				}
			}
			int[] node_arr = network.getNodeArray(node_k);
			OrigNodeType nodeType = OrigNodeType.values()[node_arr[2]];
			SpanLabel label = junction.label;
			traverseMaxNetwork(network, inputTokenized, prediction, unfinishedJunctions, usedJunctions, nodeSeq, start, node_k,
					nodeType, label);
			if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
				junctionIndex = unfinishedJunctions.size()-1;
			}
		}
		result.setPrediction(prediction);
		return result;
	}

	private void traverseMaxNetwork(OENetwork network, List<CoreLabel> inputTokenized, List<EntitySpan> prediction,
			List<JunctionInfo> unfinishedJunctions, Set<JunctionInfo> usedJunctions, List<OrigNodeType> nodeSeq, int start, int node_k, OrigNodeType nodeType,
			SpanLabel label) throws RuntimeException {
		int[] child = null;
		int[] node_arr = null;
		while(nodeType != OrigNodeType.X_NODE){
			nodeSeq.add(nodeType);
			child = network.getChildren(node_k)[0];
			if(child.length == 0){
				throw new RuntimeException(String.format("Decoding ends at a leaf node (%s at pos %d) which is not an X-node\nInput: %s",
							nodeType, inputTokenized.size()-network.getNodeArray(node_k)[0]-1, inputTokenized));
			} else if(child.length == 1){
				node_k = child[0];
			} else { // there are more than one children for this edge (overlapping mention)
				node_k = nextChild(unfinishedJunctions, usedJunctions, nodeSeq, start, node_k, label, child);
			}
			node_arr = network.getNodeArray(node_k);
			nodeType = OrigNodeType.values()[node_arr[2]];
		}
		if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
			int unfinishedSize = unfinishedJunctions.size();
			while(unfinishedSize > 0){
				JunctionInfo junction = unfinishedJunctions.get(unfinishedSize-1);
				if(junction.startIdx != start){
					break;
				}
				junction.highestUnprocessedChildIdx -= 1;
				if(junction.highestUnprocessedChildIdx == -1){
					usedJunctions.add(unfinishedJunctions.remove(unfinishedSize-1));
					unfinishedSize -= 1;
					continue;
				}
				break;
			}
		}
		prediction.add(createEntitySpan(start, nodeSeq, inputTokenized, label));
	}

	private int nextChild(List<JunctionInfo> unfinishedJunctions, Set<JunctionInfo> usedJunctions, List<OrigNodeType> nodeSeq, int start, int node_k,
			SpanLabel label, int[] child) {
		int childIdx = -1;
		for(int i=unfinishedJunctions.size()-1; i>=0; i--){
			JunctionInfo info = unfinishedJunctions.get(i);
			if(info.node_k == node_k && (networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH || info.startIdx == start)){
				childIdx = info.highestUnprocessedChildIdx;
				if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
					info.highestUnprocessedChildIdx -= 1;
					if(info.highestUnprocessedChildIdx == -1){
						usedJunctions.add(unfinishedJunctions.remove(i));
					}	
				}
				break;
			}
		}
		if(childIdx == -1){
			int highestUnprocessed = -1;
//			if(networkInterpretation == NetworkInterpretation.GENERATE_ALL){
//				highestUnprocessed = child.length-1;
//				JunctionInfo curInfo = new JunctionInfo(node_k, highestUnprocessed, start, label, nodeSeq);
//				unfinishedJunctions.add(curInfo);
//				childIdx = child.length - 1;
//			} else if (networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH){
//				highestUnprocessed = -1;
//				childIdx = 0;
//			}
			switch(networkInterpretation){
			case GENERATE_ALL:
				highestUnprocessed = child.length-1;
				break;
			case GENERATE_ENOUGH:
				highestUnprocessed = child.length-2;
				break;
			}
			JunctionInfo curInfo = new JunctionInfo(node_k, highestUnprocessed, start, label, nodeSeq);
			if(networkInterpretation == NetworkInterpretation.GENERATE_ENOUGH && usedJunctions.contains(curInfo)){
				childIdx = 0;
			} else {
				unfinishedJunctions.add(curInfo);
				childIdx = child.length-1;
			}
		}
		node_k = child[childIdx];
		return node_k;
	}

}

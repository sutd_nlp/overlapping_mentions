package com.statnlp.experiment.overlapping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreLabel;
import justhalf.nlp.sentencesplitter.NLP4JSentenceSplitter;
import justhalf.nlp.sentencesplitter.SentenceSplitter;
import justhalf.nlp.sentencesplitter.StanfordSentenceSplitter;

public class OESplitter implements Serializable {

	private static final long serialVersionUID = 4273065141127434565L;
	
	public static Pattern sectionSeparatorPattern = Pattern.compile(
			"(?<=.{10}:)\\W*\\n\\W*|" // Split after a colon at the end of a line (usually section name)
			+ "(?<=(\\n|^)[-A-Za-z ]{12,60}:)[ \\t]+(?![^\\n]*:)|" // Split after a colon if from the beginning
																   // of line it looks like a section name.
																   // Minimum 10 chars because "Abd: soft" case
			+ "\\n\\W*\\n(?![^.\\n]{1,20}[\\.\\n])|" // Split on empty line, unless the next line is short
			+ "[ \\t]*\\n[ \\t]*(?=[-A-Za-z ]{1,60}:)|" // Split on new line if the next line is a section name
			+ "[ \\t]*\\n[ \\t]*(?=\\[\\*\\*\\d\\d\\d\\d-\\d\\d-\\d\\d\\*\\*\\])|" // Split on new line if next line starts with date
			+ "(?<=\\[\\*\\*\\d{4}-\\d{2}-\\d{2}\\*\\*\\] @ \\d{4})[ \\t]*\\n[ \\t]*" // Split on new line if next line starts with date
			);

	public static enum SplitterMethod {
		STANFORD,
		NLP4J,
	}
	
	public static SentenceSplitter stanfordSplitter = null;
	public static SentenceSplitter nlp4jSplitter = null;
	
	public static List<CoreLabel> split(String input, SplitterMethod method){
		switch(method){
		case STANFORD:
			return split_stanford(input);
		case NLP4J:
			return split_nlp4j(input);
		default:
			throw new UnsupportedOperationException("The splitting method "+method+" is not recognized");
		}
	}
	
	private static List<CoreLabel> split_stanford(String input){
		if(stanfordSplitter == null){
			synchronized (SplitterMethod.STANFORD){
				if(stanfordSplitter == null){
					stanfordSplitter = new StanfordSentenceSplitter();
				}
			}
		}
		List<CoreLabel> sentences = stanfordSplitter.split(input);
		sentences = fixSplitting(sentences);
		return sentences;
	}
	
	private static List<CoreLabel> split_nlp4j(String input){
		if(nlp4jSplitter == null){
			synchronized (SplitterMethod.NLP4J){
				if(nlp4jSplitter == null){
					nlp4jSplitter = new NLP4JSentenceSplitter();
				}
			}
		}
		List<CoreLabel> sentences = nlp4jSplitter.split(input);
		sentences = fixSplitting(sentences);
		return sentences;
	}
	
	private static List<CoreLabel> fixSplitting(List<CoreLabel> sentences){
		List<CoreLabel> result = new ArrayList<CoreLabel>();
		for(CoreLabel sentence: sentences){
			String text = sentence.value();
			Matcher matcher = sectionSeparatorPattern.matcher(text);
			int lastIndex = 0;
			int beginPos = sentence.beginPosition();
			while(matcher.find()){
				int start = matcher.start();
				int end = matcher.end();
				String separator = text.substring(start, end);
				
				CoreLabel firstPart = new CoreLabel(sentence);
				CoreLabel secondPart = new CoreLabel(sentence);
				
				firstPart.setEndPosition(beginPos+start);
				String firstPartText = text.substring(lastIndex,start);
				firstPart.setWord(firstPartText);
				firstPart.setValue(firstPartText);
				firstPart.setOriginalText(firstPartText);
				firstPart.setAfter(separator);
				
				secondPart.setBeginPosition(beginPos+end);
				String secondPartText = text.substring(end);
				secondPart.setWord(secondPartText);
				secondPart.setValue(secondPartText);
				secondPart.setOriginalText(secondPartText);
				secondPart.setBefore(separator);
				add(result, firstPart);
				sentence = secondPart;
				lastIndex = end;
			}
			add(result, sentence);
		}
		return result;
	}
	
	private static void add(List<CoreLabel> result, CoreLabel sentence){
		if(result.size() == 0){
			result.add(sentence);
			return;
		}
		int size = result.size();
		CoreLabel prevSent = result.get(size-1);
		boolean endsWithColon = prevSent.value().endsWith(":");
		String prevText = prevSent.value();
		if((prevText.endsWith("?") && prevSent.after().length() == 0)
				|| (prevText.startsWith("PERICARDIUM") && !prevSent.after().contains("\n"))
				|| prevText.matches("(?s).*(\\?[^\\?]+\\?|resp\\.|elev\\.|bilat\\.|TEMP\\.|pulm\\.)")
				|| (endsWithColon && prevText.contains("X-Ray"))
				|| (prevText.matches("(?is)allerg(y|ies).*") && !prevSent.after().contains("\n\n"))
				|| (prevText.matches("(?i)abd.*") && !prevSent.after().contains("\n") && !sentence.value().matches("[^:]{0,20}:"))
				|| (prevSent.after().length() == 0)
				){
			result.set(size-1, OEUtil.merge(prevSent, sentence));
		} else {
			result.add(sentence);
		}
	}

}

package com.statnlp.experiment.overlapping;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.statnlp.example.base.BaseInstance;
import com.statnlp.experiment.overlapping.OEPOSTagger.POSTaggerMethod;
import com.statnlp.experiment.overlapping.OETokenizer.TokenizerMethod;
import com.statnlp.experiment.overlapping.OEUtil.LabelInterpretation;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.StringUtils;

public class OEInstance extends BaseInstance<OEInstance, String, List<EntitySpan>> {
	
	private static final long serialVersionUID = -5851804574658479260L;
	public OEDocument sourceDoc;
	public Span sourceSpan;
	public String sectionName;
	
	public List<Span> wordSpans;
	public List<CoreLabel> inputTokenized;
	public List<WordLabel> outputTokenized;
	public List<WordLabel> predictionTokenized;
	
	public boolean posTagged;
	
	public OEInstance(int instanceId, String input, List<EntitySpan> output, OEDocument sourceDoc, Span sourceSpan, String sectionType){
		this(instanceId, 1.0, input, output, sourceDoc, sourceSpan, sectionType);
	}
	
	public OEInstance(int instanceId, double weight) {
		this(instanceId, weight, (String)null, (List<EntitySpan>)null, (OEDocument)null, (Span)null, (String)null);
	}
	
	public OEInstance(int instanceId, double weight, String input, List<EntitySpan> output, OEDocument sourceDoc, Span sourceSpan, String sectionName){
		super(instanceId, weight);
		this.input = input;
		this.output = output;
		this.sourceDoc = sourceDoc;
		this.sourceSpan = sourceSpan;
		this.sectionName = sectionName;
		this.posTagged = false;
	}
	
	public OEInstance duplicate(){
		OEInstance result = super.duplicate();
		result.wordSpans = this.wordSpans;
		result.inputTokenized = this.inputTokenized == null ? null : new ArrayList<CoreLabel>(this.inputTokenized);
		result.outputTokenized = this.outputTokenized == null ? null : new ArrayList<WordLabel>(this.outputTokenized);
		result.predictionTokenized = this.predictionTokenized == null ? null : new ArrayList<WordLabel>(this.predictionTokenized);
		result.sourceDoc = this.sourceDoc;
		result.sectionName = this.sectionName;
		result.sourceSpan = this.sourceSpan;
		result.posTagged = this.posTagged;
		return result;
	}
	
	public String duplicateInput(){
		return input == null ? null : new String(input);
	}
	
	public List<EntitySpan> duplicateOutput(){
		return output == null ? null : new ArrayList<EntitySpan>(output);
	}

	public List<EntitySpan> duplicatePrediction(){
		return prediction == null ? null : new ArrayList<EntitySpan>(prediction);
	}
	
	public void posTag(POSTaggerMethod posTaggerMethod, boolean force){
		if(force || !posTagged){
			OEPOSTagger.posTag(inputTokenized, posTaggerMethod);
			posTagged = true;
		}
	}
	
	public List<CoreLabel> getInputTokenized(){
		if(inputTokenized == null){
			throw new RuntimeException("Input not yet tokenized. Please specify TokenizerMethod");
		}
		return inputTokenized;
	}
	
	/**
	 * Return the tokenized input if available, or calculate using the specified parameters
	 * @param tokenizerMethod
	 * @param force Whether to force re-calculation using the specified parameters, overriding existing tokenization
	 * @return
	 */
	public List<CoreLabel> getInputTokenized(TokenizerMethod tokenizerMethod, boolean force, boolean normalize){
		if(inputTokenized == null || force){
			inputTokenized = OETokenizer.tokenize(input, tokenizerMethod, normalize);
		}
		return inputTokenized;
	}
	
	public List<WordLabel> getOutputTokenized(){
		if(outputTokenized == null){
			throw new RuntimeException("Output not yet tokenized. Please specify TokenizerMethod");
		}
		return outputTokenized;
	}
	
	/**
	 * Return the tokenized output if available, or calculate using the specified parameters
	 * @param tokenizerMethod
	 * @param force Whether to force re-calculation using the specified parameters, overriding existing tokenization
	 * @return
	 */
	public List<WordLabel> getOutputTokenized(TokenizerMethod tokenizerMethod, boolean force, boolean normalize, boolean ignoreOverlaps, boolean useBILOU){
		if(outputTokenized == null || force){
			wordSpans = getWordSpans(tokenizerMethod, force, normalize);
			outputTokenized = OEUtil.spansToLabels(output, wordSpans, ignoreOverlaps, useBILOU);
		}
		return outputTokenized;
	}
	
	private List<Span> getWordSpans(TokenizerMethod tokenizerMethod, boolean force, boolean normalize){
		if(wordSpans == null || force){
			inputTokenized = getInputTokenized(tokenizerMethod, force, normalize);
			wordSpans = new ArrayList<Span>();
			for(CoreLabel word: inputTokenized){
				wordSpans.add(new Span(word.beginPosition(), word.endPosition()));
			}
		}
		return wordSpans;
	}
	
	public void setPredictionTokenized(List<WordLabel> predictionTokenized, LabelInterpretation labelInterpretation){
		this.predictionTokenized = predictionTokenized;
		if(predictionTokenized == null){
			this.prediction = null;
		} else {
			this.prediction = OEUtil.labelsToSpans(predictionTokenized, wordSpans, input, labelInterpretation);
		}
	}

	@Override
	public int size() {
		return getInputTokenized().size();
	}
	
	public String toCoNLLString(String[]... additionalFeatures){
		return OEUtil.toCoNLLString(inputTokenized, outputTokenized, predictionTokenized, additionalFeatures);
	}
	
	public String toSemEvalString(){
		StringBuilder builder = new StringBuilder();
		String filename = new File(sourceDoc.filename).getName();
		for(EntitySpan entitySpan: prediction){
			if(builder.length() > 0){
				builder.append("\n");
			}
			String cui = entitySpan.cui;
			if(cui == null){
				cui = "CUI-less";
			}
			builder.append(String.format("%s||%s||%s", filename, entitySpan.label.form, cui));
			for(Span span: entitySpan.spans){
				builder.append("||"+(sourceSpan.start+span.start));
				builder.append("||"+(sourceSpan.start+span.end));
			}
		}
		return builder.toString();
	}
	
	public String toStandoffString(){
		StringBuilder builder = new StringBuilder();
		builder.append(StringUtils.join(inputTokenized, " "));
		builder.append('\n');
		for(CoreLabel word: inputTokenized){
			builder.append(word.tag()+" ");
		}
		builder.append('\n');
		boolean first = true;
		for(EntitySpan entitySpan: prediction){
			if(!first){
				builder.append("|");
			}
			first = false;
			int startWord = -1;
			int endWord = -1;
			for(int i=0; i<inputTokenized.size(); i++){
				CoreLabel word = inputTokenized.get(i);
				if(startWord == -1 && word.beginPosition() >= entitySpan.start()){
					startWord = i;
				}
				if(word.endPosition() <= entitySpan.end()){
					endWord = i+1;
				} else {
					break;
				}
			}
			builder.append(String.format("%d,%d,%d,%d %s", startWord, endWord, startWord, endWord, entitySpan.label.form));
		}
		return builder.toString();
	}

	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append(getInstanceId()+":");
		builder.append(input);
		if(hasOutput()){
			builder.append("\n");
			boolean first = true;
			for(EntitySpan span: output){
				if(!first){
					builder.append("|");
				}
				first = false;
				builder.append(String.format("%s \"%s\"", span, span.getText(input)));
			}
		}
		return builder.toString();
	}
}

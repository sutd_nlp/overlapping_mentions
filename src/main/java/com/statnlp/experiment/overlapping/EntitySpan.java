package com.statnlp.experiment.overlapping;

import java.io.Serializable;
import java.util.Arrays;

public class EntitySpan implements Comparable<EntitySpan>, Serializable{
	
	private static final long serialVersionUID = -3442499301550024781L;
	public SpanLabel label;
	public Span[] spans;
	public String cui;
	
	public EntitySpan(EntitySpan span){
		this.label = span.label;
		this.spans = new Span[span.spans.length];
		for(int i=0; i<this.spans.length; i++){
			this.spans[i] = new Span(span.spans[i].start, span.spans[i].end);
		}
		this.cui = span.cui;
	}
	
	public EntitySpan(String cui, SpanLabel label, Span[] spans){
		this.cui = cui;
		this.label = label;
		this.spans = spans;
	}
	
	public EntitySpan(String cui, SpanLabel label, int[] startEnd){
		this.cui = cui;
		this.label = label;
		spans = new Span[startEnd.length/2];
		for(int i=0; i<startEnd.length; i+=2){
			spans[i/2] = new Span(startEnd[i], startEnd[i+1]);
		}
	}
	
	public String getText(String text){
		StringBuilder result = new StringBuilder();
		for(Span span: spans){
			if(result.length() > 0){
				result.append(" ... ");
			}
			result.append(span.getText(text));
		}
		return result.toString();
	}
	
	public int start(){
		return spans[0].start;
	}
	
	public int end(){
		return spans[spans.length-1].end;
	}
	
	public boolean overlapsWith(EntitySpan entitySpan){
		for(Span thisSpan: spans){
			for(Span otherSpan: entitySpan.spans){
				if(thisSpan.overlaps(otherSpan)){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean equals(Object o){
		if(o instanceof EntitySpan){
			EntitySpan s = (EntitySpan)o;
			if(!Arrays.equals(spans, s.spans)) return false;
			return label.equals(s.label);
		}
		return false;
	}

	@Override
	public int compareTo(EntitySpan o) {
		if(spans.length < o.spans.length){
			return -1;
		} else if(spans.length > o.spans.length){
			return 1;
		}
		for(int i=0; i<spans.length; i++){
			int result = spans[i].compareTo(o.spans[i]);
			if(result != 0) return result;
		}
		if(label == null){
			if(o.label == null){
				return 0;
			}
			return -1;
		} else {
			if(o.label == null){
				return 1;
			}
			int result = label.compareTo(o.label);
			if(result != 0) return result;
		}
		if(cui == null){
			if(o.cui == null){
				return 0;
			}
			return -1;
		} else {
			if(o.cui == null){
				return 1;
			}
			return cui.compareTo(o.cui);
		}
	}
	
	public String toString(String text){
		try{
			return this.toString()+"("+label+"): "+this.getText(text);
		} catch (Exception e){
			System.err.println(this.toString());
			return this.toString()+"("+label+"): *NULL*";
		}
	}
	
	public String toString(){
		StringBuilder result = new StringBuilder();
		for(Span span: spans){
			if(result.length() > 0){
				result.append(",");
			}
			result.append(span.toString());
		}
//		result.append(" "+label);
		return result.toString();
	}

}

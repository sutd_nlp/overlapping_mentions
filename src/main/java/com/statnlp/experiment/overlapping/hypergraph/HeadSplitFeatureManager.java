package com.statnlp.experiment.overlapping.hypergraph;

import static com.statnlp.experiment.overlapping.OEUtil.setupFeatures;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.statnlp.experiment.overlapping.IFeatureType;
import com.statnlp.experiment.overlapping.OEInstance;
import com.statnlp.experiment.overlapping.OELemmatizer;
import com.statnlp.experiment.overlapping.OENetwork;
import com.statnlp.experiment.overlapping.OELemmatizer.LemmatizerMethod;
import com.statnlp.experiment.overlapping.OEPOSTagger.POSTaggerMethod;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphNetworkCompiler.NodeType;
import com.statnlp.hybridnetworks.FeatureArray;
import com.statnlp.hybridnetworks.FeatureManager;
import com.statnlp.hybridnetworks.GlobalNetworkParam;
import com.statnlp.hybridnetworks.Network;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.StringUtils;
import edu.stanford.nlp.util.TypesafeMap;

public class HeadSplitFeatureManager extends FeatureManager {

	private static final long serialVersionUID = 1359679442997276807L;
	
	public int wordHalfWindowSize;
	public int bowHalfWindowSize;
	public int posHalfWindowSize;
	public int wordNGramSize;
	public int posNGramSize;
	public int prefixLength;
	public int suffixLength;
	public boolean wordOnlyLeftWindow;
	public boolean bowOnlyLeftWindow;
	public boolean posOnlyLeftWindow;
	
	public POSTaggerMethod posTaggerMethod;
	public LemmatizerMethod lemmatizerMethod;
	public Map<String, String> brownMap;
	public Map<String, String> umlsSemanticCategory;
	
	public static class Attribute implements TypesafeMap.Key<Map<String, String>>{}
	
	public enum FeatureType implements IFeatureType{
		CHEAT(false),
		
		WORDS(true),
		LEXICALIZED_WORDS,
		WORD_NGRAM,
		
		POS_TAG(true),
		POS_TAG_NGRAM,
		
		COMBINED_BOW(true),
		BOW(true),
		
		ORTHOGRAPHIC(true),
//		ALL_CAPS,
//		ALL_DIGITS,
//		ALL_ALPHANUMERIC,
//		CONTAINS_DIGITS,
//		CONTAINS_DOTS,
//		CONTAINS_HYPHEN,
//		INITIAL_CAPS,
//		LONELY_INITIAL,
//		PUNCTUATION_MARK,
//		ROMAN_NUMBER,
//		SINGLE_CHARACTER,
//		URL,
		
		MENTION_PENALTY(true),
		CONTIGUOUS_MENTION_PENALTY(true),
		DISCONTIGUOUS_MENTION_PENALTY(true),
		
		NOTE_TYPE(true),
		SECTION_NAME(true),
		BROWN_CLUSTER(true),
		SEMANTIC_CATEGORY(true),
		
		TRANSITION(true),
		
		BOUNDARY(true),
		;
		
		private boolean isEnabled = false;
		
		private FeatureType(){
			this(false);
		}
		
		private FeatureType(boolean enabled){
			this.isEnabled = enabled;
		}

		@Override
		public void enable() {
			this.isEnabled = true;
		}

		@Override
		public void disable() {
			this.isEnabled = false;
		}

		@Override
		public boolean enabled() {
			return this.isEnabled;
		}
	}
	
	private static enum Argument{
		WORD_HALF_WINDOW_SIZE(1,
				"The half window size for unigram word features",
				"word_half_window_size",
				"n"),
		WORD_NGRAM_SIZE(1,
				"The maximum n-gram size for word features",
				"word_ngram_size",
				"n"),
		BOW_HALF_WINDOW_SIZE(1,
				"The half window size for bag-of-words word features",
				"bow_half_window_size",
				"n"),
		POS_HALF_WINDOW_SIZE(1,
				"The half window size for POS tag features",
				"pos_half_window_size",
				"n"),
		POS_NGRAM_SIZE(1,
				"The maximum n-gram size for POS tag features",
				"pos_ngram_size",
				"n"),
		WORD_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for word features",
				"word_only_left_window"),
		BOW_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for bag-of-words features",
				"bow_only_left_window"),
		POS_ONLY_LEFT_WINDOW(0,
				"Whether to use only the left window for POS tag features",
				"pos_only_left_window"),
		PREFIX_LENGTH(1,
				"The maximum prefix lengths for word prefix features",
				"prefix_length",
				"n"),
		SUFFIX_LENGTH(1,
				"The maximum suffix lengths for word suffix features",
				"suffix_length",
				"n"),
		HELP(0,
				"Print this help message",
				"h,help"),
		;
		
		final private int numArgs;
		final private String[] argNames;
		final private String[] names;
		final private String help;
		private Argument(int numArgs, String help, String names, String... argNames){
			this.numArgs = numArgs;
			this.argNames = argNames;
			this.names = names.split(",");
			this.help = help;
		}
		
		/**
		 * Return the Argument which has the specified name
		 * @param name
		 * @return
		 */
		public static Argument argWithName(String name){
			for(Argument argument: Argument.values()){
				for(String argName: argument.names){
					if(argName.equals(name)){
						return argument;
					}
				}
			}
			throw new IllegalArgumentException("Unrecognized argument: "+name);
		}
		
		/**
		 * Print help message
		 */
		private static void printHelp(){
			StringBuilder result = new StringBuilder();
			result.append("Options:\n");
			for(Argument argument: Argument.values()){
				result.append("-"+StringUtils.join(argument.names, " -"));
				result.append(" "+StringUtils.join(argument.argNames, " "));
				result.append("\n");
				if(argument.help != null && argument.help.length() > 0){
					result.append("\t"+argument.help.replaceAll("\n","\n\t")+"\n");
				}
			}
			System.out.println(result.toString());
		}
	}

	public HeadSplitFeatureManager(GlobalNetworkParam param_g, POSTaggerMethod posTaggerMethod, LemmatizerMethod lemmatizerMethod, Map<String, String> brownMap, Map<String, String> umlsSemanticCategory, String[] features, OEInstance[] trainInstances, String... args) {
		super(param_g);
		this.posTaggerMethod = posTaggerMethod;
		this.lemmatizerMethod = lemmatizerMethod;
		this.brownMap = brownMap;
		this.umlsSemanticCategory = umlsSemanticCategory;
		wordHalfWindowSize = 1;
		bowHalfWindowSize = 1;
		posHalfWindowSize = 1;
		wordNGramSize = 2;
		posNGramSize = 2;
		prefixLength = 3;
		suffixLength = 3;
		wordOnlyLeftWindow = false;
		bowOnlyLeftWindow = false;
		posOnlyLeftWindow = false;
		setupFeatures(FeatureType.class, features);
		int argIndex = 0;
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.length() > 0 && arg.charAt(0) == '-'){
				Argument argument = Argument.argWithName(args[argIndex].substring(1));
				switch(argument){
				case WORD_HALF_WINDOW_SIZE:
					wordHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORD_NGRAM_SIZE:
					wordNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case BOW_HALF_WINDOW_SIZE:
					bowHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case POS_HALF_WINDOW_SIZE:
					posHalfWindowSize = Integer.parseInt(args[argIndex+1]);
					break;
				case POS_NGRAM_SIZE:
					posNGramSize = Integer.parseInt(args[argIndex+1]);
					break;
				case WORD_ONLY_LEFT_WINDOW:
					wordOnlyLeftWindow = true;
					break;
				case BOW_ONLY_LEFT_WINDOW:
					bowOnlyLeftWindow = true;
					break;
				case POS_ONLY_LEFT_WINDOW:
					posOnlyLeftWindow = true;
					break;
				case PREFIX_LENGTH:
					prefixLength = Integer.parseInt(args[argIndex+1]);
					break;
				case SUFFIX_LENGTH:
					suffixLength = Integer.parseInt(args[argIndex+1]);
					break;
				case HELP:
					Argument.printHelp();
					System.exit(0);
				}
				argIndex += argument.numArgs+1;
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}
	}

	@Override
	protected FeatureArray extract_helper(Network net, int parent_k, int[] children_k) {
		OENetwork network = (OENetwork)net;
		OEInstance instance = (OEInstance)network.getInstance();
		CoreLabel[] words = instance.getInputTokenized().toArray(new CoreLabel[0]);
		if(!instance.posTagged){
			instance.posTag(posTaggerMethod, false);
		}
		int size = words.length;
		
		int[] parent_arr = network.getNodeArray(parent_k);
		int pos = size-parent_arr[0]-1;
		int splitIndex = parent_arr[1];
		int bodyIdx = parent_arr[2];
		NodeType nodeType = NodeType.values()[parent_arr[3]];
		String labelId = String.valueOf(parent_arr[4]);
		
		if(nodeType == NodeType.X_NODE || nodeType == NodeType.A_NODE || nodeType == NodeType.E_NODE){
			return FeatureArray.EMPTY;
		}

		CoreLabel curWord = words[pos];
		String curWordString = curWord.value();
		String postag = curWord.tag();
		List<Integer> features = new ArrayList<Integer>();
		
		GlobalNetworkParam param_g = this._param_g;

		String childListStr = "";
		for(int child_k: children_k){
			int[] childNodeArray = network.getNodeArray(child_k);
			childListStr += "-"+NodeType.values()[childNodeArray[3]]+"["+childNodeArray[1]+","+childNodeArray[2]+"]";
		}
		
		if(FeatureType.CHEAT.enabled()){
			int cheatFeature = param_g.toFeature(network, FeatureType.CHEAT.name(), labelId, Math.abs(instance.getInstanceId())+" "+pos+" "+nodeType+"["+splitIndex+","+bodyIdx+"] "+childListStr);
			return new FeatureArray(new int[]{cheatFeature});
		}
		
		if(FeatureType.TRANSITION.enabled()){
			features.add(param_g.toFeature(network, FeatureType.TRANSITION.name(), labelId, nodeType.name()+"-"+childListStr));
		}
		
		boolean createMoreSpecificFeatures = (children_k.length > 1);
		for(int child_k: children_k){
			int[] childNodeArr = network.getNodeArray(child_k);
			int childSplitIndex = childNodeArr[1];
			int childBodyIdx = childNodeArr[2];
			NodeType childNodeType = NodeType.values()[childNodeArr[3]];
			
			boolean isLeftBoundary = false;
			boolean isRightBoundary = false;
			if(nodeType == NodeType.T_NODE && childNodeType == NodeType.B_NODE){
				if(FeatureType.MENTION_PENALTY.enabled()){
					features.add(param_g.toFeature(network, FeatureType.MENTION_PENALTY.name(), "", ""));
				}
				if(FeatureType.CONTIGUOUS_MENTION_PENALTY.enabled() && splitIndex == 0){
					features.add(param_g.toFeature(network, FeatureType.CONTIGUOUS_MENTION_PENALTY.name(), "", ""));
				}
				if(FeatureType.DISCONTIGUOUS_MENTION_PENALTY.enabled() && splitIndex > 0){
					features.add(param_g.toFeature(network, FeatureType.DISCONTIGUOUS_MENTION_PENALTY.name(), "", ""));
				}
			}
			if(nodeType == NodeType.T_NODE && childNodeType == NodeType.B_NODE){
				isLeftBoundary = true;
			}
			if(nodeType == NodeType.B_NODE && childNodeType == NodeType.X_NODE){
				isRightBoundary = true;
			}
			
			String generalIndicator = nodeType+"-"+childNodeType;
			String specificIndicator = nodeType.name()+"["+splitIndex+","+bodyIdx+"]-"+childNodeType.name()+"["+childSplitIndex+","+childBodyIdx+"]";
			
			if(FeatureType.WORDS.enabled() || FeatureType.LEXICALIZED_WORDS.enabled()){
				int wordWindowSize = wordHalfWindowSize*2+1;
				if(wordWindowSize < 0){
					wordWindowSize = 0;
				}
				for(int i=0; i<wordWindowSize; i++){
					String word = "***";
					int relIdx = (i-wordHalfWindowSize);
					int idx = pos + relIdx;
					if(idx >= 0 && idx < size){
						word = words[idx].value();
					}
					if(wordOnlyLeftWindow && idx > pos) continue;
					if(FeatureType.WORDS.enabled()){
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.WORDS+":"+relIdx, labelId, word));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORDS+":"+relIdx, labelId, word));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.WORDS+":"+relIdx, labelId, word));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, word));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, word));
							}
						}
					}
					if(FeatureType.LEXICALIZED_WORDS.enabled()){
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx, labelId, curWord.value()+" "+word));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx, labelId, curWord.value()+" "+word));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.LEXICALIZED_WORDS+":"+relIdx, labelId, curWord.value()+" "+word));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, curWord.value()+" "+word));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.LEXICALIZED_WORDS+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, curWord.value()+" "+word));
							}
						}
					}
				}
			}
			
			if(FeatureType.POS_TAG.enabled()){
				int posWindowSize = posHalfWindowSize*2+1;
				if(posWindowSize < 0){
					posWindowSize = 0;
				}
				for(int i=0; i<posWindowSize; i++){
					String tag = "***";
					int relIdx = (i-posHalfWindowSize);
					int idx = pos + relIdx;
					if(idx >= 0 && idx < size){
						tag = words[idx].tag();
					}
					if(posOnlyLeftWindow && idx > pos) continue;
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.POS_TAG+":"+relIdx, labelId, tag));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG+":"+relIdx, labelId, tag));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.POS_TAG+":"+relIdx, labelId, tag));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG+":"+relIdx+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, tag));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG+":"+relIdx+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, tag));
						}
					}
				}
			}
			
			if(FeatureType.WORD_NGRAM.enabled()){
				for(int ngramSize=2; ngramSize<=wordNGramSize; ngramSize++){
					for(int relPos=0; relPos<ngramSize; relPos++){
						String ngram = "";
						for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
							if(ngram.length() > 0) ngram += " ";
							if(idx >= 0 && idx < size){
								ngram += words[idx];
							}
						}
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, ngram));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.WORD_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, ngram));
							}
						}
					}
				}
			}
			
			if(FeatureType.POS_TAG_NGRAM.enabled()){
				for(int ngramSize=2; ngramSize<=posNGramSize; ngramSize++){
					for(int relPos=0; relPos<ngramSize; relPos++){
						String ngram = "";
						for(int idx=pos-ngramSize+relPos+1; idx<pos+relPos+1; idx++){
							if(idx > pos-ngramSize+relPos+1) ngram += " ";
							if(idx >= 0 && idx < size){
								ngram += words[idx].tag();
							}
						}
						features.add(param_g.toFeature(network, generalIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						if(createMoreSpecificFeatures){
							features.add(param_g.toFeature(network, childListStr+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos, labelId, ngram));
						}
						if(FeatureType.BOUNDARY.enabled()){
							if(isLeftBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, ngram));
							}
							if(isRightBoundary){
								features.add(param_g.toFeature(network, specificIndicator+FeatureType.POS_TAG_NGRAM+" "+ngramSize+" "+relPos+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, ngram));
							}
						}
					}
				}
			}
			
			if(FeatureType.COMBINED_BOW.enabled()){
				List<String> bowList = new ArrayList<String>();
				for(int idx=pos-bowHalfWindowSize; idx<=pos+bowHalfWindowSize; idx++){
					if(idx >= 0 && idx < size){
						bowList.add(words[idx].value());
					}
				}
				Collections.sort(bowList);
				String bow = "";
				for(String word: bowList){
					if(bow.length() > 0) bow += " ";
					bow += word;
				}
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.COMBINED_BOW.name(), labelId, bow));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.COMBINED_BOW.name(), labelId, bow));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.COMBINED_BOW.name(), labelId, bow));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.COMBINED_BOW.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, bow));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.COMBINED_BOW.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, bow));
					}
				}
			}
			
			if(FeatureType.BOW.enabled()){
				int bowWindowSize = bowHalfWindowSize*2+1;
				if(bowWindowSize < 0){
					bowWindowSize = 0;
				}
				for(int i=0; i<bowWindowSize; i++){
					String word = "***";
					int relIdx = (i-bowHalfWindowSize);
					int idx = pos + relIdx;
					if(idx >= 0 && idx < size){
						word = words[idx].value();
					}
					if(bowOnlyLeftWindow && idx > pos) continue;
					String featureName = generalIndicator+FeatureType.BOW.name();
					if(idx < pos){
						featureName += "-before";
					} if(idx > pos){
						featureName += "-after";
					} else {
						continue;
					}
					features.add(param_g.toFeature(network, featureName, labelId, word));
					featureName = specificIndicator+FeatureType.BOW.name();
					if(idx < pos){
						featureName += "-before";
					} if(idx > pos){
						featureName += "-after";
					} else {
						continue;
					}
					features.add(param_g.toFeature(network, featureName, labelId, word));
					if(createMoreSpecificFeatures){
						featureName = childListStr+FeatureType.BOW.name();
						if(idx < pos){
							featureName += "-before";
						} if(idx > pos){
							featureName += "-after";
						} else {
							continue;
						}
						features.add(param_g.toFeature(network, featureName, labelId, word));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							featureName = specificIndicator+FeatureType.BOW.name()+"-"+FeatureType.BOUNDARY+"-LEFT";
							if(idx < pos){
								featureName += "-before";
							} if(idx > pos){
								featureName += "-after";
							} else {
								continue;
							}
							features.add(param_g.toFeature(network, featureName, labelId, word));
						}
						if(isRightBoundary){
							featureName = specificIndicator+FeatureType.BOW.name()+"-"+FeatureType.BOUNDARY+"-RIGHT";
							if(idx < pos){
								featureName += "-before";
							} if(idx > pos){
								featureName += "-after";
							} else {
								continue;
							}
							features.add(param_g.toFeature(network, featureName, labelId, word));
						}
					}
				}
			}
			
			if(FeatureType.ORTHOGRAPHIC.enabled()){
//				for(FeatureType featureType: FeatureType.values()){
//					switch(featureType){
//					case ALL_CAPS:
//					case ALL_DIGITS:
//					case ALL_ALPHANUMERIC:
//					case CONTAINS_DIGITS:
//					case CONTAINS_HYPHEN:
//					case INITIAL_CAPS:
//					case LONELY_INITIAL:
//					case PUNCTUATION_MARK:
//					case ROMAN_NUMBER:
//					case SINGLE_CHARACTER:
//					case URL:
//						features.add(param_g.toFeature(generalIndicator+featureType.name(), labelId, curWord.get(Attribute.class).get(featureType.name())));
//					default:
//						break;
//					}
//				}
				// Prefix
				for(int i=0; i<3; i++){
					String prefix = curWordString.substring(0, Math.min(curWordString.length(), i+1));
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX", labelId, prefix));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX", labelId, prefix));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-PREFIX", labelId, prefix));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, prefix));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-PREFIX"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, prefix));
						}
					}
				}
			
				// Suffix
				for(int i=0; i<3; i++){
					String suffix = curWordString.substring(Math.max(0, curWordString.length()-i-1), curWordString.length());
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX", labelId, suffix));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX", labelId, suffix));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-SUFFIX", labelId, suffix));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, suffix));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-SUFFIX"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, suffix));
						}
					}
				}
				
				// Capitalization
				boolean isCapitalized = StringUtils.isCapitalized(curWordString);
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", labelId, isCapitalized+""));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", labelId, isCapitalized+""));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION", labelId, isCapitalized+""));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, isCapitalized+""));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-CAPITALIZATION"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, isCapitalized+""));
					}
				}
				
				// Lemma
				String lemma = OELemmatizer.lemmatize(curWordString, postag, lemmatizerMethod);
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA", labelId, lemma));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA", labelId, lemma));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.ORTHOGRAPHIC+"-LEMMA", labelId, lemma));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA"+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, lemma));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.ORTHOGRAPHIC+"-LEMMA"+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, lemma));
					}
				}
			}
			
			if(FeatureType.BROWN_CLUSTER.enabled()){
				String brownCluster = getBrownCluster(curWord.value());
				for(int i=0; i<brownCluster.length(); i++){
					String brownFeatureString = brownCluster.substring(0, i+1);
					features.add(param_g.toFeature(network, generalIndicator+FeatureType.BROWN_CLUSTER.name(), labelId, brownFeatureString));
					features.add(param_g.toFeature(network, specificIndicator+FeatureType.BROWN_CLUSTER.name(), labelId, brownFeatureString));
					if(createMoreSpecificFeatures){
						features.add(param_g.toFeature(network, childListStr+FeatureType.BROWN_CLUSTER.name(), labelId, brownFeatureString));
					}
					if(FeatureType.BOUNDARY.enabled()){
						if(isLeftBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.BROWN_CLUSTER.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, brownFeatureString));
						}
						if(isRightBoundary){
							features.add(param_g.toFeature(network, specificIndicator+FeatureType.BROWN_CLUSTER.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, brownFeatureString));
						}
					}
				}
			}

			if(FeatureType.NOTE_TYPE.enabled()){
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.NOTE_TYPE.name(), labelId, instance.sourceDoc.docType.name()));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.NOTE_TYPE.name(), labelId, instance.sourceDoc.docType.name()));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.NOTE_TYPE.name(), labelId, instance.sourceDoc.docType.name()));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.NOTE_TYPE.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, instance.sourceDoc.docType.name()));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.NOTE_TYPE.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, instance.sourceDoc.docType.name()));
					}
				}
			}
			
			if(FeatureType.SECTION_NAME.enabled()){
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.SECTION_NAME.name(), labelId, instance.sectionName));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.SECTION_NAME.name(), labelId, instance.sectionName));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.SECTION_NAME.name(), labelId, instance.sectionName));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SECTION_NAME.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, instance.sectionName));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SECTION_NAME.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, instance.sectionName));
					}
				}
			}
			
			if(FeatureType.SEMANTIC_CATEGORY.enabled()){
				String semanticCategory = getUmlsSemanticCategory(curWord.value());
				features.add(param_g.toFeature(network, generalIndicator+FeatureType.SEMANTIC_CATEGORY.name(), labelId, semanticCategory));
				features.add(param_g.toFeature(network, specificIndicator+FeatureType.SEMANTIC_CATEGORY.name(), labelId, semanticCategory));
				if(createMoreSpecificFeatures){
					features.add(param_g.toFeature(network, childListStr+FeatureType.SEMANTIC_CATEGORY.name(), labelId, semanticCategory));
				}
				if(FeatureType.BOUNDARY.enabled()){
					if(isLeftBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SEMANTIC_CATEGORY.name()+"-"+FeatureType.BOUNDARY+"-LEFT", labelId, semanticCategory));
					}
					if(isRightBoundary){
						features.add(param_g.toFeature(network, specificIndicator+FeatureType.SEMANTIC_CATEGORY.name()+"-"+FeatureType.BOUNDARY+"-RIGHT", labelId, semanticCategory));
					}
				}
			}
			
			createMoreSpecificFeatures = false;
		}
		
		int[] featuresInt = new int[features.size()];
		for(int i=0; i<featuresInt.length; i++){
			featuresInt[i] = features.get(i);
		}
		FeatureArray result = new FeatureArray(featuresInt);
		return result;
	}
	
	private String getBrownCluster(String word){
		if(brownMap == null){
			throw new NullPointerException("Feature requires brown clusters but no brown clusters info is provided");
		}
		String clusterId = brownMap.get(word);
		if(clusterId == null){
			clusterId = "X";
		}
		return clusterId;
	}
	
	private String getUmlsSemanticCategory(String word){
		if(umlsSemanticCategory == null){
			throw new NullPointerException("Feature requires UMLS semantic category but no UMLS semantic category info is provided");
		}
		String semanticCategory = umlsSemanticCategory.get(word);
		if(semanticCategory == null){
			semanticCategory = "General";
		}
		return semanticCategory;
	}
	
	private void writeObject(ObjectOutputStream oos) throws IOException{
		oos.writeObject(posTaggerMethod);
		oos.writeObject(lemmatizerMethod);
		oos.writeObject(brownMap);
		oos.writeObject(umlsSemanticCategory);
		oos.writeInt(wordHalfWindowSize);
		oos.writeInt(bowHalfWindowSize);
		oos.writeInt(posHalfWindowSize);
		oos.writeInt(wordNGramSize);
		oos.writeInt(posNGramSize);
		oos.writeInt(prefixLength);
		oos.writeInt(suffixLength);
		oos.writeBoolean(wordOnlyLeftWindow);
		oos.writeBoolean(bowOnlyLeftWindow);
		oos.writeBoolean(posOnlyLeftWindow);
		for(FeatureType featureType: FeatureType.values()){
			oos.writeBoolean(featureType.isEnabled);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException{
		posTaggerMethod = (POSTaggerMethod)ois.readObject();
		lemmatizerMethod = (LemmatizerMethod)ois.readObject();
		brownMap = (Map<String, String>)ois.readObject();
		umlsSemanticCategory = (Map<String, String>)ois.readObject();
		wordHalfWindowSize = ois.readInt();
		bowHalfWindowSize = ois.readInt();
		posHalfWindowSize = ois.readInt();
		wordNGramSize = ois.readInt();
		posNGramSize = ois.readInt();
		prefixLength = ois.readInt();
		suffixLength = ois.readInt();
		wordOnlyLeftWindow = ois.readBoolean();
		bowOnlyLeftWindow = ois.readBoolean();
		posOnlyLeftWindow = ois.readBoolean();
		for(FeatureType featureType: FeatureType.values()){
			featureType.isEnabled = ois.readBoolean();
		}
	}

}

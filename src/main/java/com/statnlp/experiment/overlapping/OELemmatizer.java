package com.statnlp.experiment.overlapping;

import justhalf.nlp.lemmatizer.Lemmatizer;
import justhalf.nlp.lemmatizer.NLP4JLemmatizer;

public class OELemmatizer {
	
	public static Lemmatizer nlp4jLemmatizer;
	
	public static enum LemmatizerMethod{
		NLP4J,
	}
	
	public static String lemmatize(String lemma, String pos, LemmatizerMethod method){
		switch(method){
		case NLP4J:
			return lemmatize_nlp4j(lemma, pos);
		default:
			throw new UnsupportedOperationException("The lemmatizer method "+method+" is not recognized");
		}
	}
	
	public static String lemmatize_nlp4j(String word, String pos){
		if(nlp4jLemmatizer == null){
			synchronized (LemmatizerMethod.NLP4J){
				if(nlp4jLemmatizer == null){
					nlp4jLemmatizer = new NLP4JLemmatizer();
				}
			}
		}
		word = edu.emory.mathcs.nlp.common.util.StringUtils.toSimplifiedForm(word);
		return nlp4jLemmatizer.lemmatize(word, pos);
	}

}

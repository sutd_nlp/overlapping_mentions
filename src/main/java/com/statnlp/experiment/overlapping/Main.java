package com.statnlp.experiment.overlapping;

import static com.statnlp.experiment.overlapping.OEUtil.print;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.statnlp.commons.ml.opt.OptimizerFactory;
import com.statnlp.commons.types.Instance;
import com.statnlp.experiment.overlapping.OEEvaluator.Statistics;
import com.statnlp.experiment.overlapping.OELemmatizer.LemmatizerMethod;
import com.statnlp.experiment.overlapping.OEPOSTagger.POSTaggerMethod;
import com.statnlp.experiment.overlapping.OESplitter.SplitterMethod;
import com.statnlp.experiment.overlapping.OETokenizer.TokenizerMethod;
import com.statnlp.experiment.overlapping.OEUtil.InstanceFilter;
import com.statnlp.experiment.overlapping.OEUtil.LabelInterpretation;
import com.statnlp.experiment.overlapping.hypergraph.HeadSplitFeatureManager;
import com.statnlp.experiment.overlapping.hypergraph.HeadSplitNetworkCompiler;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphFeatureManager;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphNetworkCompiler;
import com.statnlp.experiment.overlapping.hypergraph.MentionHypergraphNetworkCompiler.NetworkInterpretation;
import com.statnlp.experiment.overlapping.linear.LinearCRFFeatureManager;
import com.statnlp.experiment.overlapping.linear.LinearCRFNetworkCompiler;
import com.statnlp.experiment.overlapping.linear.MultiLinearCRFFeatureManager;
import com.statnlp.experiment.overlapping.linear.MultiLinearCRFNetworkCompiler;
import com.statnlp.experiment.overlapping.linear.ParallelLinearCRFFeatureManager;
import com.statnlp.experiment.overlapping.linear.ParallelLinearCRFNetworkCompiler;
import com.statnlp.experiment.overlapping.multigraph.MultigraphFeatureManager;
import com.statnlp.experiment.overlapping.multigraph.MultigraphNetworkCompiler;
import com.statnlp.hybridnetworks.DiscriminativeNetworkModel;
import com.statnlp.hybridnetworks.FeatureManager;
import com.statnlp.hybridnetworks.GenerativeNetworkModel;
import com.statnlp.hybridnetworks.GlobalNetworkParam;
import com.statnlp.hybridnetworks.NetworkCompiler;
import com.statnlp.hybridnetworks.NetworkConfig;
import com.statnlp.hybridnetworks.NetworkModel;
import com.statnlp.hybridnetworks.NetworkConfig.ModelType;

public class Main {
	
	public static final String DEFAULT_RESULT_DIR = "_prediction_output";

	public static enum Algorithm {
		LINEAR_CRF(true, false),
		MENTION_HYPERGRAPH(true, true),
		MENTION_HYPERGRAPH_SPLIT(true, true),
		MULTIGRAPH(true, true),
		MULTILINEAR(true, false),
		PARALINEAR(true, false),
		;

		private boolean requireTokenized = false;
		private boolean edgeBased = false;

		private Algorithm(boolean requireTokenized, boolean edgeBased){
			this.requireTokenized = requireTokenized;
			this.edgeBased = edgeBased;
		}

		public boolean requireTokenized(){
			return requireTokenized;
		}
		
		public boolean isEdgeBased(){
			return edgeBased;
		}

		public static String helpString(){
			return "Please specify the algorithm from the following choices:\n"
					+ "\t-LINEAR_CRF\n"
					+ "\t-MENTION_HYPERGRAPH\n"
					+ "\t-MENTION_HYPERGRAPH_SPLIT\n"
					+ "\t-MULTIGRAPH\n"
					+ "\t-MULTILINEAR\n"
					+ "\t-PARALINEAR"
					;
		}
	}
	
	public static final class UMLSCategory {
		public String code;
		public String name;
		public String[] codeHier;
		
		public UMLSCategory(String code, String name){
			this.code = code;
			this.name = name;
			if(name == null || code == null){
				throw new NullPointerException("Code and name cannot be null object");
			}
			code = code.charAt(0)+"."+code.substring(1);
			this.codeHier = code.split("\\.");
			for(int i=1; i<codeHier.length; i++){
				codeHier[i] = codeHier[i] + "." + codeHier[i-1];
			}
		}
		
		public String getCodeHierAt(int level){
			if(codeHier.length <= level){
				return codeHier[codeHier.length-1];
			}
			return codeHier[level];
		}
		
		public String toString(){
			return this.code+" "+this.name;
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, NoSuchFieldException, SecurityException, InterruptedException, IllegalArgumentException, IllegalAccessException{
//		Random random = new Random(31);
//		double[] values = new double[100000];
//		for(int i=0; i<values.length; i++){
//			values[i] = random.nextDouble()/2e-100;
//		}
//		int N=(int)1e8;
//		double res;
//		long t_start = System.nanoTime();
//		for(int i=0; i<N; i++){
//			res = Math.log1p(values[i % values.length]);
//		}
//		long t_end = System.nanoTime();
//		System.out.printf("Java log1p: %.3fs\n", (t_end-t_start)/1e9);
//		t_start = System.nanoTime();
//		for(int i=0; i<N; i++){
//			res = FastMath.log1p(values[i % values.length]);
//		}
//		t_end = System.nanoTime();
//		System.out.printf("JNI log1p: %.3fs\n", (t_end-t_start)/1e9);
//		t_start = System.nanoTime();
//		for(int i=0; i<N; i++){
//			res = org.apache.commons.math3.util.FastMath.log1p(values[i % values.length]);
//		}
//		t_end = System.nanoTime();
//		System.out.printf("Apache log1p: %.3fs\n", (t_end-t_start)/1e9);
//		/*
//		 * Java log1p: 5.647s
//		 * JNI log1p: 3.826s
//		 * Apache log1p: 8.678s
//		 */
//		System.exit(0);

		boolean serializeModel = true;
		String timestamp = Calendar.getInstance().getTime().toString();
		String modelPath = timestamp+".model";
		String logPath = timestamp+".log";
		String brownPath = null;
		Algorithm algo = null;

		boolean trainUseDir = false;
		boolean testUseDir = false;
		boolean devUseDir = false;
		boolean doTraining = true;
		boolean doTesting = true;
		boolean doEvaluation = false;
		boolean doTuning = false;
		
		String trainSourcePath = null;
		String trainAnnotationPath = null;
		String trainSourceDir = null;
		String trainAnnotationDir = null;
		String testSourcePath = null;
		String testAnnotationPath = null;
		String testSourceDir = null;
		String testAnnotationDir = null;
		String devSourcePath = null;
		String devAnnotationPath = null;
		String devSourceDir = null;
		String devAnnotationDir = null;
		String resultDir = null;
		String umlsDir = null;
		OEInstance[] trainInstances = null;
		OEInstance[] testInstances = null;
		OEInstance[] devInstances = null;
		Map<String, String> brownMap = null;
		Map<String, String> umlsSemanticCategory = null;

		int maxLength = 0;
		int maxBodyCount = 0;
		boolean findMaxLength = true;
		boolean findMaxBodyCount = true;
		
		boolean writeModelText = false;
		String weightInit = "random";
		boolean overrideMentionPenalty = false;
		boolean overrideSeparateMentionPenalty = false;
		boolean findOptimalMentionPenalty = false;
		boolean findOptimalSeparatedMentionPenalty = false;
		
		boolean useRandomLabelSplit = false;
		int numRandomLabels = 0;
		boolean useMoreLabelTypes = false;
		int hierarchyLevel = 0;
		boolean ignoreNullLabel = false;

		int startSearchIdx = -4;
		int gridSearchSize = 20;
		double searchMult = 0.1;
		
		double mentionPenalty = 0.0;
		double contMP = 0.0;
		double discMP = 0.0;
		
		boolean useLBFGS = true;
		
		NetworkConfig.AVOID_DUPLICATE_FEATURES = true;
		NetworkConfig.CACHE_FEATURES_DURING_TRAINING = true;
		NetworkConfig.MODEL_TYPE = ModelType.CRF;
		NetworkConfig.USE_BATCH_TRAINING = false;
		NetworkConfig.BATCH_SIZE = 0;
		NetworkConfig.BUILD_FEATURES_FROM_LABELED_ONLY = false;
		NetworkConfig.PARALLEL_FEATURE_EXTRACTION = false;
		NetworkConfig.NUM_THREADS = 4;
		NetworkConfig.L2_REGULARIZATION_CONSTANT = 0.01;
		NetworkConfig.OBJTOL = 1e-6;
		NetworkConfig.NODE_COST = 1.0;
		NetworkConfig.EDGE_COST = 0.5;
		NetworkConfig.NORMALIZE_COST = false;
		NetworkConfig.MARGIN = 1.0;

		int maxNumIterations = 1000;
		int maxInstances = 100000;

		String[] features = new String[0];

		TokenizerMethod tokenizerMethod = TokenizerMethod.STANFORD;
		SplitterMethod splitterMethod = SplitterMethod.STANFORD;
		POSTaggerMethod posTaggerMethod = POSTaggerMethod.STANFORD;
		LemmatizerMethod lemmatizerMethod = LemmatizerMethod.NLP4J;
		LabelInterpretation labelInterpretation = LabelInterpretation.HEAD_BODY_LIMITED;
		NetworkInterpretation networkInterpretation = NetworkInterpretation.GENERATE_ENOUGH;
		InstanceFilter instanceFilter = InstanceFilter.ALL_INSTANCES;
		
		boolean normalize = true;
		boolean dataIsRaw = true;
		boolean hasPOS = true;
		boolean normalizeDateAndNumbers = false;
		boolean useSpecificIndicator = true;
		boolean ignoreOverlaps = false;
		boolean useBILOU = false;

		int numExamplesPrinted = 10;
		boolean printOnlyMistakes = false;

		int argIndex = 0;
		String[] moreArgs = new String[0];
		while(argIndex < args.length){
			String arg = args[argIndex];
			if(arg.charAt(0) == '-'){
				switch(arg.substring(1)){
				case "modelPath":
					serializeModel = true;
					modelPath = args[argIndex+1];
					argIndex += 2;
					break;
				case "writeModelText":
					writeModelText = true;
					argIndex += 1;
					break;
				case "trainPath":
					trainSourcePath = args[argIndex+1];
					trainAnnotationPath = args[argIndex+2];
					argIndex += 3;
					break;
				case "trainDir":
					trainSourceDir = args[argIndex+1];
					trainAnnotationDir = args[argIndex+2];
					argIndex += 3;
					break;
				case "testPath":
					testSourcePath = args[argIndex+1];
					if(argIndex+2 < args.length && !args[argIndex+2].startsWith("-")){
						testAnnotationPath = args[argIndex+2];
						argIndex += 1;
					}
					argIndex += 2;
					break;
				case "testDir":
					testSourceDir = args[argIndex+1];
					if(argIndex+2 < args.length && !args[argIndex+2].startsWith("-")){
						testAnnotationDir = args[argIndex+2];
						argIndex += 1;
					}
					argIndex += 2;
					break;
				case "devPath":
					devSourcePath = args[argIndex+1];
					devAnnotationPath = args[argIndex+2];
					argIndex += 3;
					break;
				case "devDir":
					devSourceDir = args[argIndex+1];
					devAnnotationDir = args[argIndex+2];
					argIndex += 3;
					break;
				case "dataIsRaw":
					dataIsRaw = Boolean.parseBoolean(args[argIndex+1]);
					argIndex += 2;
					break;
				case "hasPOS":
					hasPOS = Boolean.parseBoolean(args[argIndex+1]);
					argIndex += 2;
					break;
				case "resultDir":
					resultDir = args[argIndex+1];
					argIndex += 2;
					break;
				case "maxLength":
					maxLength = Integer.parseInt(args[argIndex+1]);
					findMaxLength = false;
					argIndex += 2;
					break;
				case "maxBodyCount":
					maxBodyCount = Integer.parseInt(args[argIndex+1]);
					findMaxBodyCount = false;
					argIndex += 2;
					break;
				case "nThreads":
					NetworkConfig.NUM_THREADS = Integer.parseInt(args[argIndex+1]);
					argIndex += 2;
					break;
				case "l2":
					NetworkConfig.L2_REGULARIZATION_CONSTANT = Double.parseDouble(args[argIndex+1]);
					argIndex += 2;
					break;
				case "weightInit":
					weightInit = args[argIndex+1];
					if(weightInit.equals("random")){
						NetworkConfig.RANDOM_INIT_WEIGHT = true;
					} else {
						NetworkConfig.RANDOM_INIT_WEIGHT = false;
						NetworkConfig.FEATURE_INIT_WEIGHT = Double.parseDouble(weightInit);
					}
					argIndex += 2;
					break;
				case "mentionPenalty":
					mentionPenalty = Double.parseDouble(args[argIndex+1]);
					overrideMentionPenalty = true;
					argIndex += 2;
					break;
				case "separatedMentionPenalty":
					contMP = Double.parseDouble(args[argIndex+1]);
					discMP = Double.parseDouble(args[argIndex+2]);
					overrideSeparateMentionPenalty = true;
					argIndex += 3;
					break;
				case "findOptimalMentionPenalty":
					findOptimalMentionPenalty = true;
					argIndex += 1;
					break;
				case "findOptimalSeparatedMentionPenalty":
					findOptimalSeparatedMentionPenalty = true;
					argIndex += 1;
					break;
				case "gridSearchOpts":
					gridSearchSize = Integer.parseInt(args[argIndex+1]);
					startSearchIdx = Integer.parseInt(args[argIndex+2]);
					searchMult = Double.parseDouble(args[argIndex+3]);
					argIndex += 4;
					break;
				case "objtol":
					NetworkConfig.OBJTOL = Double.parseDouble(args[argIndex+1]);
					argIndex += 2;
					break;
				case "maxIter":
					maxNumIterations = Integer.parseInt(args[argIndex+1]);
					argIndex += 2;
					break;
				case "logPath":
					logPath = args[argIndex+1];
					argIndex += 2;
					break;
				case "brownPath":
					brownPath = args[argIndex+1];
					argIndex += 2;
					break;
				case "umlsDir":
					umlsDir = args[argIndex+1];
					argIndex += 2;
					break;
				case "algo":
					try{
						algo = Algorithm.valueOf(args[argIndex+1].toUpperCase());
						if(algo.isEdgeBased()){
							NetworkConfig.NODE_COST = 1.0;
							NetworkConfig.EDGE_COST = 0.5;
						} else {
							NetworkConfig.NODE_COST = 1.0;
							NetworkConfig.EDGE_COST = 0.0;
						}
					} catch (IllegalArgumentException e){
						throw new IllegalArgumentException("Unrecognized algorithm: "+args[argIndex+1]+"\n"+Algorithm.helpString());
					}
					argIndex += 2;
					break;
				case "useSSVM":
					NetworkConfig.MODEL_TYPE = ModelType.SSVM;
					argIndex += 1;
					break;
				case "useSoftmaxMargin":
					NetworkConfig.MODEL_TYPE = ModelType.SOFTMAX_MARGIN;
					argIndex += 1;
					break;
				case "margin":
					NetworkConfig.MARGIN = Double.parseDouble(args[argIndex+1]);
					argIndex += 2;
					break;
				case "useGD":
					useLBFGS = false;
					argIndex += 1;
					break;
				case "batchSize":
					NetworkConfig.BATCH_SIZE = Integer.parseInt(args[argIndex+1]);
					if(NetworkConfig.BATCH_SIZE > 0){
						NetworkConfig.USE_BATCH_TRAINING = true;
					}
					argIndex += 2;
					break;
				case "tokenizer":
					tokenizerMethod = TokenizerMethod.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "useBILOU":
					useBILOU = true;
					argIndex += 1;
					break;
				case "no_normalize":
					normalize = false;
					argIndex += 1;
					break;
				case "splitter":
					splitterMethod = SplitterMethod.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "posTagger":
					posTaggerMethod = POSTaggerMethod.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "lemmatizer":
					lemmatizerMethod = LemmatizerMethod.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "instanceFilter":
					instanceFilter = InstanceFilter.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "labelInterpretation":
					labelInterpretation = LabelInterpretation.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "networkInterpretation":
					networkInterpretation = NetworkInterpretation.valueOf(args[argIndex+1].toUpperCase());
					argIndex += 2;
					break;
				case "features":
					features = args[argIndex+1].split(",");
					argIndex += 2;
					break;
				case "numExamplesPrinted":
					numExamplesPrinted = Integer.parseInt(args[argIndex+1]);
					argIndex += 2;
					break;
				case "printOnlyMistakes":
					printOnlyMistakes = true;
					argIndex += 1;
					break;
				case "parallelTouch":
					NetworkConfig.PARALLEL_FEATURE_EXTRACTION = true;
					argIndex += 1;
					break;
				case "touchLabeledOnly":
					NetworkConfig.BUILD_FEATURES_FROM_LABELED_ONLY = true;
					argIndex += 1;
					break;
				case "randomLabelSplit":
					useRandomLabelSplit = true;
					numRandomLabels = Integer.parseInt(args[argIndex+1]);
					argIndex += 2;
					break;
				case "useMoreLabelTypes":
					useMoreLabelTypes = true;
					argIndex += 1;
					break;
				case "hierarchyLevel":
					hierarchyLevel = Integer.parseInt(args[argIndex+1]);
					argIndex += 1;
					break;
				case "ignoreNullLabel":
					ignoreNullLabel = true;
					argIndex += 1;
					break;
				case "normalizeDateAndNumbers":
					normalizeDateAndNumbers = true;
					argIndex += 1;
					break;
				case "useSpecificIndicator":
					useSpecificIndicator = Boolean.parseBoolean(args[argIndex+1]);
					argIndex += 2;
					break;
				case "ignoreOverlaps":
					ignoreOverlaps = true;
					argIndex += 1;
					break;
				case "h":
				case "help":
					printHelp();
					System.exit(0);
				case "-":
					moreArgs = Arrays.copyOfRange(args, argIndex+1, args.length);
					argIndex = args.length;
					break;
				default:
					throw new IllegalArgumentException("Unrecognized argument: "+arg);
				}
			} else {
				throw new IllegalArgumentException("Error while parsing: "+arg);
			}
		}

		PrintStream outstream = null;
		if(logPath != null){
			outstream = new PrintStream(logPath, "UTF-8");
		}
		if(algo == null){
			OEUtil.print(Algorithm.helpString(), true, outstream, System.out);
			printHelp();
			System.exit(0);
		}
		if(((trainAnnotationDir == null) != (trainSourceDir == null)) ||
			((trainAnnotationPath == null) != (trainSourcePath == null))){
			OEUtil.print("Both source and annotation needs to be specified for training", true, outstream, System.out);
		}
		if(trainSourcePath != null && trainAnnotationPath != null){
			trainUseDir = false;
		} else if(trainSourceDir != null && trainAnnotationDir != null){
			trainUseDir = true;
		} else {
			doTraining = false;
		}
		if(testSourcePath != null){
			testUseDir = false;
			if(testAnnotationPath != null){
				doEvaluation = true;
			}
		} else if(testSourceDir != null){
			testUseDir = true;
			if(testAnnotationDir != null){
				doEvaluation = true;
			}
		} else {
			doTesting = false;
		}
		if(devSourcePath != null){
			devUseDir = false;
			if(devAnnotationPath != null){
				doTuning = true;
			}
		} else if(devSourceDir != null){
			devUseDir = true;
			if(devAnnotationDir != null){
				doTuning = true;
			}
		} else {
			doTuning = false;
		}
		doTuning |= findOptimalSeparatedMentionPenalty;
		if(resultDir == null){
			resultDir = DEFAULT_RESULT_DIR;
		}

		FeatureManager fm = null;
		NetworkCompiler compiler = null;
		NetworkModel model = null;
		Map<String, UMLSCategory> cuiToCatAsParam = null;
		if(brownPath != null){
			OEUtil.print("Reading Brown cluster info...", false, outstream, System.out);
			long start = System.nanoTime();
			brownMap = new HashMap<String, String>();
			Scanner input = new Scanner(new File(brownPath));
			while(input.hasNextLine()){
				String[] tokens = input.nextLine().split("\t");
				brownMap.put(tokens[1], tokens[0]);
			}
			input.close();
			long end = System.nanoTime();
			OEUtil.print(String.format("Done in %.3fs", (end-start)/1.0e9), true, outstream, System.out);
		}
		Map<String, UMLSCategory> cuiToCat = new HashMap<String, UMLSCategory>();
		cuiToCatAsParam = cuiToCat;
		if(!useMoreLabelTypes){
			cuiToCatAsParam = null;
		}
		if(umlsDir != null){
			OEUtil.print("Reading UMLS data...", false, outstream, System.out);
			long start = System.nanoTime();
			umlsSemanticCategory = new HashMap<String, String>();
			Scanner input;
			input = new Scanner(new File(umlsDir+File.separator+"MRSTY.RRF"));
			while(input.hasNextLine()){
				String[] tokens = input.nextLine().split("\\|");
				cuiToCat.put(tokens[0], new UMLSCategory(tokens[2], tokens[3]));
			}
			input.close();
			input = new Scanner(new File(umlsDir+File.separator+"MRXNS_ENG.RRF"));
			while(input.hasNextLine()){
				String[] tokens = input.nextLine().split("\\|");
				umlsSemanticCategory.put(tokens[1], cuiToCat.get(tokens[2]).name);
			}
			input.close();
			long end = System.nanoTime();
			OEUtil.print(String.format("Done in %.3fs", (end-start)/1.0e9), true, outstream, System.out);
		}
		if(doTraining){
			
			List<OEInstance> trainInstancesList = new ArrayList<OEInstance>();
			if(trainUseDir){
				trainInstancesList = Arrays.asList(OEUtil.readFromDirAndGetInstances(trainSourceDir, trainAnnotationDir, true, splitterMethod, tokenizerMethod, instanceFilter, cuiToCatAsParam, hierarchyLevel, ignoreNullLabel, useRandomLabelSplit, numRandomLabels));
			} else {
				OEInstance[] instanceArr = null;
				if(dataIsRaw){
					instanceArr = OEUtil.readFromPathAndGetInstances(trainSourcePath, trainAnnotationPath, true, splitterMethod, tokenizerMethod, instanceFilter, cuiToCatAsParam, hierarchyLevel, ignoreNullLabel, useRandomLabelSplit, numRandomLabels);
				} else {
					instanceArr = OEUtil.readFromFormattedData(trainSourcePath, true, hasPOS, normalizeDateAndNumbers);
				}
				trainInstancesList.addAll(Arrays.asList(instanceArr));
			}

			SpanLabel[] spanLabels = SpanLabel.LABELS.values().toArray(new SpanLabel[SpanLabel.LABELS.size()]);

			int totalEntities = 0;
			int totalIgnored = 0;
			for(int instanceIdx = trainInstancesList.size()-1; instanceIdx >= 0; instanceIdx--){
				OEInstance instance = trainInstancesList.get(instanceIdx);
				int size = -1;
				if(algo.requireTokenized()){
					instance.getInputTokenized(tokenizerMethod, dataIsRaw, normalize);
					instance.getOutputTokenized(tokenizerMethod, false, normalize, ignoreOverlaps, useBILOU);
					size = instance.getInputTokenized().size();
				} else {
					size = instance.getInput().length();
				}
				if(findMaxLength){
					maxLength = Math.max(maxLength, size);
				} else if(size > maxLength){
					System.err.println(String.format("Ignoring instance (ID=%d, length=%d) because it is longer than max length %d", instance.getInstanceId(), size, maxLength));
					trainInstancesList.remove(instanceIdx);
					continue;
				}
				List<EntitySpan> output = instance.output;
				if(findMaxBodyCount){ // Max body count is not set, set as the longest entity
					for(EntitySpan span: output){
						maxBodyCount = Math.max(maxBodyCount, span.spans.length);
					}
				} else { // Max body count is set, ignore those spans longer than that
					totalEntities += output.size();
					for(int i=output.size()-1; i>=0; i--){
						if(output.get(i).spans.length > maxBodyCount){
							output.remove(i);
							totalIgnored += 1;
						}
					}
					if(algo.requireTokenized()){
						instance.getOutputTokenized(tokenizerMethod, dataIsRaw, normalize, ignoreOverlaps, useBILOU);
					}
				}
			}
			if(totalIgnored > 0){
				System.err.println(String.format("Ignored %d/%d spans", totalIgnored, totalEntities));
			}

			trainInstances = trainInstancesList.toArray(new OEInstance[trainInstancesList.size()]);
			trainInstances = sublist(trainInstances, Math.min(trainInstances.length, maxInstances));

			NetworkConfig.TRAIN_MODE_IS_GENERATIVE = false;
			NetworkConfig.CACHE_FEATURES_DURING_TRAINING = true;
			
			GlobalNetworkParam gParam;
			if(!useLBFGS){
//				if(NetworkConfig.MODEL_TYPE == ModelType.SSVM){
//					gParam = new GlobalNetworkParam(OptimizerFactory.getGradientDescentFactoryUsingSmoothedAdaDeltaThenGD(1e-5, 0.95, 1e-7, 0.9));
//				} else {
					gParam = new GlobalNetworkParam(OptimizerFactory.getGradientDescentFactoryUsingSmoothedAdaDeltaThenStop(0.95, 1e-7, 0.9));
//				}
			} else {
				gParam = new GlobalNetworkParam(OptimizerFactory.getLBFGSFactory());
			}

			int size = trainInstances.length;

			print("Read.."+size+" instances.", true, outstream, System.err);

			WordLabel[] wordLabels = WordLabel.LABELS.values().toArray(new WordLabel[WordLabel.LABELS.size()]);
			switch(algo){
			case LINEAR_CRF:
				fm = new LinearCRFFeatureManager(gParam, posTaggerMethod, lemmatizerMethod, brownMap, umlsSemanticCategory, features, trainInstances, useSpecificIndicator, moreArgs);
				compiler = new LinearCRFNetworkCompiler(wordLabels, maxLength, labelInterpretation);
				((LinearCRFFeatureManager)fm).isClinicalTask = dataIsRaw;
				break;
			case MENTION_HYPERGRAPH:
				fm = new MentionHypergraphFeatureManager(gParam, posTaggerMethod, lemmatizerMethod, brownMap, umlsSemanticCategory, features, trainInstances, useSpecificIndicator, moreArgs);
				compiler = new MentionHypergraphNetworkCompiler(spanLabels, maxLength, maxBodyCount, networkInterpretation);
				((MentionHypergraphFeatureManager)fm).isClinicalTask = dataIsRaw;
				break;
			case MENTION_HYPERGRAPH_SPLIT:
				fm = new HeadSplitFeatureManager(gParam, posTaggerMethod, lemmatizerMethod, brownMap, umlsSemanticCategory, features, trainInstances, moreArgs);
				compiler = new HeadSplitNetworkCompiler(spanLabels, maxLength, maxBodyCount, tokenizerMethod, networkInterpretation);
				break;
			case MULTIGRAPH:
				fm = new MultigraphFeatureManager(gParam, posTaggerMethod, lemmatizerMethod, brownMap, umlsSemanticCategory, features, trainInstances, useSpecificIndicator, moreArgs);
				compiler = new MultigraphNetworkCompiler(spanLabels, maxLength, maxBodyCount, networkInterpretation);
				((MultigraphFeatureManager)fm).isClinicalTask = dataIsRaw;
				break;
			case MULTILINEAR:
				fm = new MultiLinearCRFFeatureManager(gParam, posTaggerMethod, lemmatizerMethod, brownMap, umlsSemanticCategory, features, trainInstances, useSpecificIndicator, moreArgs);
				compiler = new MultiLinearCRFNetworkCompiler(spanLabels, maxLength, networkInterpretation);
				((MultiLinearCRFFeatureManager)fm).isClinicalTask = dataIsRaw;
				break;
			case PARALINEAR:
				for(SpanLabel label: spanLabels){
					String[] prefixes;
					if(useBILOU){
						if(ignoreOverlaps){
							prefixes = new String[]{"B", "I", "L", "U"};
						} else {
							prefixes = new String[]{"B", "I", "L", "U", "BH", "IH", "UH", "LH"};
						}
					} else {
						if(ignoreOverlaps){
							prefixes = new String[]{"B", "I"};
						} else {
							prefixes = new String[]{"B", "I", "BH", "IH"};
						}
					}
					for(String prefix: prefixes){
						WordLabel.get(prefix+"-"+label.form);
					}
					WordLabel.get("O");
				}
				wordLabels = WordLabel.LABELS.values().toArray(new WordLabel[WordLabel.LABELS.size()]);
				fm = new ParallelLinearCRFFeatureManager(gParam, posTaggerMethod, lemmatizerMethod, brownMap, umlsSemanticCategory, features, trainInstances, useSpecificIndicator, moreArgs);
				compiler = new ParallelLinearCRFNetworkCompiler(wordLabels, spanLabels, maxLength, labelInterpretation, useBILOU, ignoreOverlaps);
				((ParallelLinearCRFFeatureManager)fm).isClinicalTask = dataIsRaw;
				break;
			}

			model = NetworkConfig.TRAIN_MODE_IS_GENERATIVE ? GenerativeNetworkModel.create(fm, compiler) : DiscriminativeNetworkModel.create(fm, compiler);

			/* ******************* *
			 * Main training phase *
			 * ******************* */
			model.train(trainInstances, maxNumIterations);

			if(serializeModel){
				print("Writing object...", false, outstream, System.out);
				long startTime = System.currentTimeMillis();
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelPath));
				oos.writeObject(model);
				oos.writeObject(WordLabel.LABELS);
				oos.writeObject(WordLabel.LABELS_INDEX);
				oos.writeObject(SpanLabel.LABELS);
				oos.writeObject(SpanLabel.LABELS_INDEX);
				oos.close();
				long endTime = System.currentTimeMillis();
				print(String.format("Done in %.3fs", (endTime-startTime)/1000.0), true, outstream, System.out);
			}
		} else {
			print("Reading object...", false, outstream, System.out);
			long startTime = System.currentTimeMillis();
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(modelPath));
			model = (NetworkModel)ois.readObject();
			@SuppressWarnings("unchecked")
			Map<String, WordLabel> word_labels = (Map<String, WordLabel>)ois.readObject();
			@SuppressWarnings("unchecked")
			Map<Integer, WordLabel> word_labelsIndex = (Map<Integer, WordLabel>)ois.readObject();
			@SuppressWarnings("unchecked")
			Map<String, SpanLabel> span_labels = (Map<String, SpanLabel>)ois.readObject();
			@SuppressWarnings("unchecked")
			Map<Integer, SpanLabel> span_labelsIndex = (Map<Integer, SpanLabel>)ois.readObject();
			ois.close();
			for(String key: word_labels.keySet()){
				WordLabel.LABELS.put(key, word_labels.get(key));
			}
			for(Integer key: word_labelsIndex.keySet()){
				WordLabel.LABELS_INDEX.put(key, word_labelsIndex.get(key));
			}
			for(String key: span_labels.keySet()){
				SpanLabel.LABELS.put(key, span_labels.get(key));
			}
			for(Integer key: span_labelsIndex.keySet()){
				SpanLabel.LABELS_INDEX.put(key, span_labelsIndex.get(key));
			}
			Field _fm = NetworkModel.class.getDeclaredField("_fm");
			_fm.setAccessible(true);
			fm = (FeatureManager)_fm.get(model);
			Field _compiler = NetworkModel.class.getDeclaredField("_compiler");
			_compiler.setAccessible(true);
			compiler = (NetworkCompiler)_compiler.get(model);
			if(compiler instanceof LinearCRFNetworkCompiler){
				((LinearCRFNetworkCompiler)compiler).labelInterpretation = labelInterpretation;
				((LinearCRFFeatureManager)fm).isClinicalTask = dataIsRaw;
			} else if(compiler instanceof MentionHypergraphNetworkCompiler){
				((MentionHypergraphNetworkCompiler)compiler).networkInterpretation = networkInterpretation;
				((MentionHypergraphNetworkCompiler)compiler).maxBodyCount = maxBodyCount;
				((MentionHypergraphFeatureManager)fm).isClinicalTask = dataIsRaw;
			} else if(compiler instanceof MultigraphNetworkCompiler){
				((MultigraphNetworkCompiler)compiler).networkInterpretation = networkInterpretation;
				((MultigraphNetworkCompiler)compiler).maxBodyCount = maxBodyCount;
				((MultigraphFeatureManager)fm).isClinicalTask = dataIsRaw;
			} else if(compiler instanceof ParallelLinearCRFNetworkCompiler){
				((ParallelLinearCRFNetworkCompiler)compiler).useBILOU = useBILOU;
				((ParallelLinearCRFNetworkCompiler)compiler).ignoreOverlaps = ignoreOverlaps;
				((ParallelLinearCRFFeatureManager)fm).isClinicalTask = dataIsRaw;
			}

			long endTime = System.currentTimeMillis();
			print(String.format("Done in %.3fs", (endTime-startTime)/1000.0), true, outstream, System.out);
		}
		if(writeModelText){
			PrintStream modelTextWriter = new PrintStream(modelPath+".txt");
			modelTextWriter.println("Algorithm: "+algo);
			modelTextWriter.println("Serialize?: "+serializeModel);
			modelTextWriter.println("Model path: "+modelPath);
			modelTextWriter.println("Train source dir: "+trainSourceDir);
			modelTextWriter.println("Train annotation dir: "+trainAnnotationDir);
			modelTextWriter.println("Test source dir: "+testSourceDir);
			modelTextWriter.println("Max length: "+maxLength);
			modelTextWriter.println("Max span: "+maxBodyCount);
			modelTextWriter.println("#Threads: "+NetworkConfig.NUM_THREADS);
			modelTextWriter.println("L2 param: "+NetworkConfig.L2_REGULARIZATION_CONSTANT);
			modelTextWriter.println("Weight init: "+weightInit);
			modelTextWriter.println("objtol: "+NetworkConfig.OBJTOL);
			modelTextWriter.println("Max iter: "+maxNumIterations);
			modelTextWriter.println("Splitter: "+splitterMethod);
			modelTextWriter.println("Tokenizer: "+tokenizerMethod);
			modelTextWriter.println("POS Tagger: "+posTaggerMethod);
			modelTextWriter.println("Lemmatizer: "+lemmatizerMethod);
			modelTextWriter.println("Features: "+Arrays.asList(features));
			modelTextWriter.println();
			modelTextWriter.println("Labels:");
			List<?> labelsUsed = new ArrayList<Object>();
			switch(algo){
			case LINEAR_CRF:
				labelsUsed = Arrays.asList(((LinearCRFNetworkCompiler)compiler)._labels);
				break;
			case MENTION_HYPERGRAPH:
				labelsUsed = Arrays.asList(((MentionHypergraphNetworkCompiler)compiler)._labels);
				break;
			case MENTION_HYPERGRAPH_SPLIT:
				labelsUsed = Arrays.asList(((HeadSplitNetworkCompiler)compiler)._labels);
				break;
			case MULTIGRAPH:
				labelsUsed = Arrays.asList(((MultigraphNetworkCompiler)compiler)._labels);
				break;
			case MULTILINEAR:
				labelsUsed = Arrays.asList(((MultiLinearCRFNetworkCompiler)compiler)._labels);
				break;
			case PARALINEAR:
				labelsUsed = Arrays.asList(((ParallelLinearCRFNetworkCompiler)compiler)._labels);
				break;
			}
			for(Object obj: labelsUsed){
				modelTextWriter.println(obj);
			}
			GlobalNetworkParam paramG = fm.getParam_G();
			modelTextWriter.println("Num features: "+paramG.countFeatures());
			modelTextWriter.println("Features:");
			HashMap<String, HashMap<String, HashMap<String, Integer>>> featureIntMap = paramG.getFeatureIntMap();
			for(String featureType: sorted(featureIntMap.keySet())){
				modelTextWriter.println(featureType);
				HashMap<String, HashMap<String, Integer>> outputInputMap = featureIntMap.get(featureType);
				for(String output: sorted(outputInputMap.keySet())){
					modelTextWriter.println("\t"+output);
					HashMap<String, Integer> inputMap = outputInputMap.get(output);
					for(String input: sorted(inputMap.keySet())){
						int featureId = inputMap.get(input);
						modelTextWriter.println("\t\t"+input+" "+featureId+" "+fm.getParam_G().getWeight(featureId));
					}
				}
			}
			modelTextWriter.close();
		}
		if(overrideMentionPenalty){
			HashMap<String, HashMap<String, Integer>> mpFeature = fm.getParam_G().getFeatureIntMap().get("MENTION_PENALTY");
			if(mpFeature != null){
				int mpId = mpFeature.get("").get("");
				double oldMP = fm.getParam_G().getWeight(mpId);
				OEUtil.print(String.format("Replacing mention penalty weight from %.3f to %.3f", oldMP, oldMP+mentionPenalty), true, outstream, System.out);
				fm.getParam_G().setWeight(mpId, oldMP+mentionPenalty);
			}
		}
		if(overrideSeparateMentionPenalty){
			HashMap<String, HashMap<String, Integer>> contMPFeature = fm.getParam_G().getFeatureIntMap().get("CONTIGUOUS_MENTION_PENALTY");
			HashMap<String, HashMap<String, Integer>> discMPFeature = fm.getParam_G().getFeatureIntMap().get("DISCONTIGUOUS_MENTION_PENALTY");
			if(contMPFeature != null && discMPFeature != null){
				int contMPId = contMPFeature.get("").get("");
				int discMPId = discMPFeature.get("").get("");
				double oldContMP = fm.getParam_G().getWeight(contMPId);
				double oldDiscMP = fm.getParam_G().getWeight(discMPId);
				OEUtil.print(String.format("Replacing separated mention penalty weights from (%.3f, %.3f) to (%.3f, %.3f)", oldContMP, oldDiscMP, oldContMP+contMP, oldDiscMP+discMP), true, outstream, System.out);
				fm.getParam_G().setWeight(contMPId, oldContMP+contMP);
				fm.getParam_G().setWeight(discMPId, oldDiscMP+discMP);
			}
		}
		
		if(doTuning){
			OEUtil.print("Performing tuning on development set: "+(devSourceDir == null ? devSourcePath : devSourceDir), true, outstream, System.out);
			if(devUseDir){
				devInstances = OEUtil.readFromDirAndGetInstances(devSourceDir, devAnnotationDir, false, splitterMethod, tokenizerMethod, instanceFilter, cuiToCatAsParam, hierarchyLevel, ignoreNullLabel, useRandomLabelSplit, numRandomLabels);
			} else {
				if(dataIsRaw){
					devInstances = OEUtil.readFromPathAndGetInstances(devSourcePath, devAnnotationPath, false, splitterMethod, tokenizerMethod, instanceFilter, cuiToCatAsParam, hierarchyLevel, ignoreNullLabel, useRandomLabelSplit, numRandomLabels);
				} else {
					devInstances = OEUtil.readFromFormattedData(devSourcePath, false, hasPOS, normalizeDateAndNumbers);
				}
			}
			devInstances = sublist(devInstances, Math.min(devInstances.length, maxInstances));
			if(algo.requireTokenized()){
				for(OEInstance instance: devInstances){
					instance.getInputTokenized(tokenizerMethod, dataIsRaw, normalize);
					instance.getOutputTokenized(tokenizerMethod, false, normalize, ignoreOverlaps, useBILOU);
				}
			}
			if(findOptimalMentionPenalty){
				HashMap<String, HashMap<String, Integer>> mentionPenaltyFeature = fm.getParam_G().getFeatureIntMap().get("MENTION_PENALTY");
				if(mentionPenaltyFeature != null){
					int mentionPenaltyId;
					try{
						mentionPenaltyId = mentionPenaltyFeature.get("MP").get("MP");
					} catch (NullPointerException e){
						mentionPenaltyId = mentionPenaltyFeature.get("").get("");
					}
					testWithAdjustedFeatureWeight(devInstances, outstream, fm, model, mentionPenaltyId, startSearchIdx, gridSearchSize, searchMult, ignoreOverlaps);
				} else if(algo == Algorithm.LINEAR_CRF){
					List<Integer> featureIdsList = new ArrayList<Integer>();
					HashMap<String, HashMap<String, Integer>> transitionFeatures = fm.getParam_G().getFeatureIntMap().get(LinearCRFFeatureManager.FeatureType.TRANSITION.name());
					for(String transitionFeature: transitionFeatures.keySet()){
						if(transitionFeature.matches(".*-[BU][HD]?.*")){
							featureIdsList.add(transitionFeatures.get(transitionFeature).get(""));
						}
					}
					int[] featureIds = new int[featureIdsList.size()];
					for(int i=0; i<featureIds.length; i++){
						featureIds[i] = featureIdsList.get(i);
					}
					testWithAdjustedFeatureWeight(devInstances, outstream, fm, model, featureIds, startSearchIdx, gridSearchSize, searchMult, ignoreOverlaps);
				} else {
					OEUtil.print("Requested to find optimal mention penalty, but no mention penalty feature is found.", true, outstream, System.out);
				}
			}
			if(findOptimalSeparatedMentionPenalty){
				HashMap<String, HashMap<String, Integer>> contMPFeature = fm.getParam_G().getFeatureIntMap().get("CONTIGUOUS_MENTION_PENALTY");
				HashMap<String, HashMap<String, Integer>> discMPFeature = fm.getParam_G().getFeatureIntMap().get("DISCONTIGUOUS_MENTION_PENALTY");
				if(contMPFeature != null && discMPFeature != null){
					int contMPId = contMPFeature.get("").get("");
					int discMPId = discMPFeature.get("").get("");
					testWithAdjustedFeatureWeights(devInstances, outstream, fm, model, contMPId, discMPId, startSearchIdx, gridSearchSize, searchMult, ignoreOverlaps);
				} else {
					OEUtil.print("Requested to find optimal separated mention penalty, but no separated mention penalty features are found.", true, outstream, System.out);
				}
			}
		}

		if(doTesting){
			OEUtil.print("Performing testing on test set: "+(testSourceDir == null ? testSourcePath : testSourceDir), true, outstream, System.out);
			if(testUseDir){
				testInstances = OEUtil.readFromDirAndGetInstances(testSourceDir, testAnnotationDir, false, splitterMethod, tokenizerMethod, instanceFilter, cuiToCatAsParam, hierarchyLevel, ignoreNullLabel, useRandomLabelSplit, numRandomLabels);
			} else {
				if(dataIsRaw){
					testInstances = OEUtil.readFromPathAndGetInstances(testSourcePath, testAnnotationPath, false, splitterMethod, tokenizerMethod, instanceFilter, cuiToCatAsParam, hierarchyLevel, ignoreNullLabel, useRandomLabelSplit, numRandomLabels);
				} else {
					testInstances = OEUtil.readFromFormattedData(testSourcePath, false, hasPOS, normalizeDateAndNumbers);
				}
			}
			testInstances = sublist(testInstances, Math.min(testInstances.length, maxInstances));
			if(algo.requireTokenized()){
				for(OEInstance instance: testInstances){
					instance.getInputTokenized(tokenizerMethod, dataIsRaw, normalize);
					instance.getOutputTokenized(tokenizerMethod, false, normalize, ignoreOverlaps, useBILOU);
				}
			}
			System.out.println("Number of sentences in test set: "+testInstances.length);
			int numWords = 0;
			for(OEInstance instance: testInstances){
				numWords += instance.getInputTokenized().size();
			}
			System.out.println("Number of words in test set: "+numWords);
			Instance[] predictions = null;
			long start = System.currentTimeMillis();
			predictions = model.decode(testInstances);
			long end = System.currentTimeMillis();
			System.out.printf("Time to decode test set: %.3fs\n", (end-start)/1000.0);
			System.out.printf("Number of sentences processed per second: %.3f sentences/s\n", testInstances.length*1000.0/(end-start));
			System.out.printf("Number of words processed per second: %.3f words/s\n", numWords*1000.0/(end-start));
			List<OEInstance> predictionsList = new ArrayList<OEInstance>();
			for(Instance instance: predictions){
				predictionsList.add((OEInstance)instance);
			}
			predictionsList.sort(Comparator.comparing(Instance::getInstanceId));
			File resultDirFile = new File(resultDir);
			if(!resultDirFile.exists()){
				resultDirFile.mkdirs();
			}
			Set<String> filenames = new HashSet<String>();
			for(OEInstance instance: predictionsList){
				String filename = null;
				if(dataIsRaw){
					filename = resultDir+File.separator+getResultFileName(instance.sourceDoc.filename);
				} else {
					filename = resultDir+File.separator+(testSourcePath.replace(File.separator, "_").replace(".", ""))+".result";
				}
				PrintStream result = null;
				if(filenames.contains(filename)){
					result = new PrintStream(new FileOutputStream(filename, true));
				} else {
					result = new PrintStream(new FileOutputStream(filename, false));
					filenames.add(filename);
				}
				if(dataIsRaw){
					String semevalString = instance.toSemEvalString();
					if(semevalString.length() > 0){
						result.println(instance.toSemEvalString());
					}
				} else {
					result.println(instance.toStandoffString()+"\n");
				}
				result.close();
			}
			if(doEvaluation){
				OEEvaluator.evaluate(predictions, outstream, numExamplesPrinted, printOnlyMistakes, false, false, ignoreOverlaps);
			}
			if(outstream != null){
				outstream.close();
			}
		}
	}
	
	private static void testWithAdjustedFeatureWeight(OEInstance[] instances, PrintStream outstream,
			FeatureManager fm, NetworkModel model, int[] featureIds, int startSearchIdx, int gridSearchSize, double searchMult, boolean ignoreOverlaps) throws InterruptedException {
		Instance[] predictions;
		double[] oldFeatureWeights = new double[featureIds.length];
		for(int fid=0; fid<oldFeatureWeights.length; fid++){
			oldFeatureWeights[fid] = fm.getParam_G().getWeight(featureIds[fid]);
		}
		double maxF1 = 0.0;
		double optimalWeightOffset = 10000.0;
		for(int i=startSearchIdx; i<=startSearchIdx+gridSearchSize; i++){
			double weightOffset = searchMult*i;
			OEUtil.print(String.format("Trying mention penalty offset = %.3f", weightOffset), true, outstream, System.out);
			for(int fid=0; fid<featureIds.length; fid++){
				fm.getParam_G().setWeight(featureIds[fid], oldFeatureWeights[fid]+weightOffset);
			}
//			fm.getParam_G().setWeight(featureId, weight);
			fm.getParam_G().setVersion(fm.getParam_G().getVersion()+1);
			predictions = model.decode(instances, true);
			Statistics[] scores = OEEvaluator.evaluate(predictions, null, 0, true, true, false, ignoreOverlaps);
			Statistics avgScore = new Statistics();
			double avgF1 = 0.0;
			for(Statistics score: scores){
//				avgF1 += score.calculateF1();
				avgScore.add(score);
			}
//			avgF1 /= scores.length;
			avgF1 = avgScore.calculateF1();
			if(avgF1 > maxF1 || (avgF1 == maxF1 && Math.abs(weightOffset) < Math.abs(optimalWeightOffset))){
				maxF1 = avgF1;
				optimalWeightOffset = weightOffset;
			}
			OEEvaluator.printDetailedScore(scores, outstream, System.out);
		}
		OEUtil.print(String.format("Replacing mention penalty weight from 0.0 with the optimal value 0.0 + %.3f = %.3f (f1 = %.2f%%)", optimalWeightOffset, optimalWeightOffset, maxF1*100), true, outstream, System.out);
		for(int fid=0; fid<featureIds.length; fid++){
			fm.getParam_G().setWeight(featureIds[fid], oldFeatureWeights[fid]+optimalWeightOffset);
		}
//		fm.getParam_G().setWeight(featureId, optimalWeight);
	}

	private static void testWithAdjustedFeatureWeight(OEInstance[] instances, PrintStream outstream,
			FeatureManager fm, NetworkModel model, int featureId, int startSearchIdx, int gridSearchSize, double searchMult, boolean ignoreOverlaps) throws InterruptedException {
		Instance[] predictions;
		double oldFeatureWeight = fm.getParam_G().getWeight(featureId);
		double maxF1 = 0.0;
		double optimalWeight = 0.0;
		for(int i=startSearchIdx; i<=startSearchIdx+gridSearchSize; i++){
			double weight = oldFeatureWeight + searchMult*i;
			OEUtil.print(String.format("Trying mention penalty = %.3f", weight), true, outstream, System.out);
			fm.getParam_G().setWeight(featureId, weight);
			fm.getParam_G().setVersion(fm.getParam_G().getVersion()+1);
			predictions = model.decode(instances, true);
			Statistics[] scores = OEEvaluator.evaluate(predictions, null, 0, true, true, false, ignoreOverlaps);
			Statistics avgScore = new Statistics();
			double avgF1 = 0.0;
			for(Statistics score: scores){
//				avgF1 += score.calculateF1();
				avgScore.add(score);
			}
//			avgF1 /= scores.length;
			avgF1 = avgScore.calculateF1();
			if(avgF1 > maxF1 || (avgF1 == maxF1 && Math.abs(weight-oldFeatureWeight) < Math.abs(optimalWeight-oldFeatureWeight))){
				maxF1 = avgF1;
				optimalWeight = weight;
			}
			OEEvaluator.printDetailedScore(scores, outstream, System.out);
		}
		OEUtil.print(String.format("Replacing mention penalty weight from %.3f with the optimal value %.3f + %.3f = %.3f (f1 = %.2f%%)", oldFeatureWeight, oldFeatureWeight, optimalWeight-oldFeatureWeight, optimalWeight, maxF1*100), true, outstream, System.out);
		fm.getParam_G().setWeight(featureId, optimalWeight);
	}
	
	private static void testWithAdjustedFeatureWeights(OEInstance[] instances, PrintStream outstream, FeatureManager fm,
			NetworkModel model, int contFeatureId, int discFeatureId, int startSearchIdx, int gridSearchSize, double searchMult, boolean ignoreOverlaps) throws InterruptedException {
		Instance[] predictions;
		double oldContFeatureWeight = fm.getParam_G().getWeight(contFeatureId);
		double oldDiscFeatureWeight = fm.getParam_G().getWeight(discFeatureId);
		double maxF1 = 0.0;
		double optimalContWeight = 0.0;
		double optimalDiscWeight = 0.0;
		for(int i=startSearchIdx; i<=startSearchIdx+gridSearchSize; i++){
			for(int j=startSearchIdx; j<=startSearchIdx+gridSearchSize; j++){
				double contWeight = oldContFeatureWeight + searchMult*i;
				double discWeight = oldDiscFeatureWeight + searchMult*j;
				OEUtil.print(String.format("Trying (contiguousMP, discontiguousMP) = (%.3f, %.3f)", contWeight, discWeight), true, outstream, System.out);
				fm.getParam_G().setWeight(contFeatureId, contWeight);
				fm.getParam_G().setWeight(discFeatureId, discWeight);
				predictions = model.decode(instances, true);
				Statistics[] scores = OEEvaluator.evaluate(predictions, null, 0, true, true, false, ignoreOverlaps);
				Statistics avgScore = new Statistics();
				double avgF1 = 0.0;
				for(Statistics score: scores){
//					avgF1 += score.calculateF1();
					avgScore.add(score);
				}
//				avgF1 /= scores.length;
				avgF1 = avgScore.calculateF1();
				if(avgF1 > maxF1 || (avgF1 == maxF1 && Math.abs(contWeight-oldContFeatureWeight)+Math.abs(discWeight-oldDiscFeatureWeight) < Math.abs(optimalContWeight-oldContFeatureWeight)+Math.abs(optimalDiscWeight-oldDiscFeatureWeight))){
					maxF1 = avgF1;
					optimalContWeight = contWeight;
					optimalDiscWeight = discWeight;
				}
				OEEvaluator.printDetailedScore(scores, outstream, System.out);
			}
		}
		OEUtil.print(String.format("Replacing weights from (%.3f, %.3f) with the optimal value (%.3f, %.3f) + ( %.3f , %.3f ) = (%.3f, %.3f) (f1 = %.2f%%)", oldContFeatureWeight, oldDiscFeatureWeight, oldContFeatureWeight, oldDiscFeatureWeight, optimalContWeight-oldContFeatureWeight, optimalDiscWeight-oldDiscFeatureWeight, optimalContWeight, optimalDiscWeight, maxF1*100), true, outstream, System.out);
		fm.getParam_G().setWeight(contFeatureId, optimalContWeight);
		fm.getParam_G().setWeight(discFeatureId, optimalDiscWeight);
	}

	private static String getResultFileName(String testSourcePath) {
		testSourcePath = new File(testSourcePath).getName();
		String result;
		int extensionIndex = testSourcePath.lastIndexOf('.');
		if(extensionIndex == -1){
			extensionIndex = testSourcePath.length();
		}
		result = testSourcePath.substring(0, extensionIndex)+".pipe"+testSourcePath.substring(extensionIndex);
		return result;
	}
	
	private static List<String> sorted(Set<String> coll){
		List<String> result = new ArrayList<String>(coll);
		Collections.sort(result, Comparator.nullsFirst(Comparator.naturalOrder()));
		return result;
	}
	
	protected static OEInstance[] sublist(OEInstance[] instances, int num){
		OEInstance[] result = new OEInstance[num];
		for(int i=0; i<num; i++){
			result[i] = instances[i];
		}
		return result;
	}

	private static void printHelp(){
		System.out.println("Options:\n"
				
				+ "-algo (LINEAR_CRF|MENTION_HYPERGRAPH|MENTION_HYPERGRAPH_SPLIT|MULTIGRAPH|MULTILINEAR|PARALINEAR)\n"
				+ "\tThe algorithm to be used:\n"
				+ "\t-LINEAR_CRF: Standard linear CRF\n"
				+ "\t-MENTION_HYPERGRAPH: Use modified head mention algorithm\n"
				+ "\t-MENTION_HYPERGRAPH_SPLIT: Use split head mention algorithm\n"
				+ "\t-MULTIGRAPH: Similar to MENTION_HYPERGRAPH, but with the true inside-outside score\n"
				+ "\t-MULTILINEAR: Similar to LINEAR_CRF, but uses entity separator instead of Head components\n"
				+ "\t-PARALINEAR: Similar to LINEAR_CRF, but support multiple types\n"
				
				+ "-modelPath <modelPath>\n"
				+ "\tSerialize model to <modelPath>\n"
				
				+ "-writeModelText\n"
				+ "\tWrite the model in text version for debugging purpose\n"
				
				+ "-trainPath <trainSourcePath> <trainAnnotationPath>\n"
				+ "\tTake training source file from <trainSourcePath> and annotations from <trainAnnotationPath>.\n"
				+ "\tIf this and -trainDir are not specified, no training is performed\n"
				+ "\tWill attempt to load the model if training path/dir is not specified but test is specified\n"
				
				+ "-trainDir <trainSourceDir> <trainAnnotationDir>\n"
				+ "\tTake training source files from <trainSourceDir> and annotations from <trainAnnotationDir>.\n"
				+ "\tIf this and -trainPath are not specified, no training is performed\n"
				+ "\tWill attempt to load the model if training path/dir is not specified but test is specified\n"
				
				+ "-testPath <testSourcePath> [<testAnnotationPath>]\n"
				+ "\tTake test source file from <testSourcePath> and annotations from <testAnnotationPath>.\n"
				+ "\tIf this and -testDir are not specified, no test is performed\n"
				+ "\tIf <testAnnotationPath> is not specified, no evaluation is performed.\n"
				
				+ "-testDir <testSourceDir> [<testAnnotationDir>]\n"
				+ "\tTake test source files from <testSourceDir> and annotations from <testAnnotationDir>.\n"
				+ "\tIf this and -testPath are not specified, no test is performed\n"
				+ "\tIf <testAnnotationDir> is not specified, no evaluation is performed.\n"

				+ "-devPath <devSourcePath> <devAnnotationPath>\n"
				+ "\tTake tuning source file from <devSourcePath> and annotations from <devAnnotationPath>.\n"
				+ "\tIf this and -devDir are not specified, no tuning is performed\n"
				
				+ "-devDir <devSourceDir> <devAnnotationDir>\n"
				+ "\tTake tuning source files from <devSourceDir> and annotations from <devAnnotationDir>.\n"
				+ "\tIf this and -devPath are not specified, no tuning is performed\n"
				
				+ "-resultDir <resultDir>\n"
				+ "\tPrint result from testing into <resultDir>. If not specified, it is based on the test name\n"
				+ "\tIf the directory does not exist, it will be created\n"
				
				+ "-brownPath <brownPath>\n"
				+ "\tThe path to the Brown clustering \"paths\" file.\n"
				
				+ "-umlsDir <umlsDir>\n"
				+ "\tThe path to directory containing MRSTY.RRF and MRXNS_ENG.RRF files\n"
				
				+ "-maxLength <n>\n"
				+ "\tSet the maximum input length that will be supported to <n>.\n"
				+ "\tDefault to maximum length in training set\n"
				
				+ "-maxBodyCount <n>\n"
				+ "\tSet the maximum body count to <n>. Default to maximum in training set\n"
				
				+ "-nThreads <n>\n"
				+ "\tSet the number of threads to <n>. Default to 4\n"
				
				+ "-l2 <value>\n"
				+ "\tSet the L2 regularization parameter weight to <value>. Default to 0.01\n"
				
				+ "-weightInit <\"random\" or value>\n"
				+ "\tWeight initialization. If \"random\", the weights will be randomly assigned values between\n"
				+ "\t-0.05 to 0.05 (uniform distribution). Otherwise, it will be set to the value provided.\n"
				+ "\tDefault to random\n"
				
				+ "-mentionPenalty <value>\n"
				+ "\tSet the mention penalty. The higher the value is, the more likely the network to predict more entities."
				+ "\tDefault to the value learned during training\n"
				
				+ "-separatedMentionPenalty <value>\n"
				+ "\tSet the contiguous and discontiguous mention penalty. The higher the value is, the more likely the network to predict more entities."
				+ "\tDefault to the value learned during training\n"
				
				+ "-findOptimalMentionPenalty\n"
				+ "\tTune the mention penalty in development set\n"
				+ "\tThe range can be configured through -gridSearchOpts\n"
				
				+ "-findOptimalSeparatedMentionPenalty\n"
				+ "\tTune the contiguous and discontiguous mention penalty in development set\n"
				+ "\tThe range can be configured through -gridSearchOpts\n"
				
				+ "-gridSearchOpts <gridSearchSize> <startSearchIndex> <multiplier>\n"
				+ "\tSpecify the grid search configuration\n"
				+ "\t<gridSearchSize> defines the number of values to be tested\n"
				+ "\t<startSearchIndex> defines the starting index to be tested\n"
				+ "\t<multiplier> defines the index multiplier as the value to be tested\n"
				+ "\tThe values tested will then range from <startSearchIndex>*<multiplier> to (<startSearchIndex>+<gridSearchSize>)*<multiplier>\n"
				
				+ "-objtol <value>\n"
				+ "\tStop when the improvement of objective function is less than <value>. Default to 0.01\n"
				+ "\tNote that the training will also stop when the ratio of change is\n"
				+ "\tless than 0.01% for 3 consecutive iterations\n"
				
				+ "-maxIter <n>\n"
				+ "\tSet the maximum number of iterations to <n>. Default to 5000\n"
				
				+ "-logPath <logPath>\n"
				+ "\tPrint output and evaluation result to file at <logPath>.\n"
				+ "\tNote that the output will still be printed to STDOUT\n"
				
				+ "-dataIsRaw (true|false)\n"
				+ "\tWhether the data is raw (true) or already formatted one instance per three lines\n"

				+ "-useSSVM\n"
				+ "\tWhether to use SSVM instead of CRF\n"
				
				+ "-useSoftmaxMargin\n"
				+ "\tWhether to use softmax margin instead of CRF\n"
				
				+ "-batchSize <n>\n"
				+ "\tThe batch size for SGD. If 0, then no batch is used\n"
				
				+ "-tokenizer (regex|stanford|whitespace)\n"
				+ "\tThe tokenizer method to be used: regex, stanford, or whitespace. Default to stanford\n"
				
				+ "-splitter (nlp4j|stanford)\n"
				+ "\tThe sentence splitter method to be used: nlp4j, stanford. Default to stanford\n"
				
				+ "-posTagger (stanford)\n"
				+ "\tThe POS Tagger method to be used: stanford. Default to stanford\n"
				
				+ "-labelInterpretation (head_all|head_limited|head_body_limited|nested_only)\n"
				+ "\tThe label interpretation method to be used for LINEAR_CRF: head_all, head_limited, or head_body_limited. Default to head_body_limited\n"
				
				+ "-networkInterpretation (generate_all|generate_enough)\n"
				+ "\tThe network interpretation method to be used for MENTION_HYPERGRAPH and MULTIGRAPH: generate_all or generate_enough. Default to generate_enough\n"
				
				+ "-numExamplesPrinted <n>\n"
				+ "\tSpecify the number of examples printed during evaluation. Default to 10\n"
				
				+ "-parallelTouch\n"
				+ "\tA flag whether the feature extraction should be parallel. By default it is sequential\n"
				
				+ "-touchLabeledOnly\n"
				+ "\tA flag whether the features should be created based on only the labeled networks.\n"
				+ "\tBy default features will be created based on unlabeled networks also\n"
				
				+ "-randomLabelSplit <n>\n"
				+ "\tSplit labels randomly into n distinct labels\n"
				
				+ "-useSpecificIndicator (true|false)\n"
				+ "\tWhether to pair input features with transition (second-order) output features\n"
				
				+ "-ignoreOverlaps\n"
				+ "\tWhether to ignore overlaps in the dataset by removing the shorter one in case of overlap\n"
				
				+ "-useBILOU\n"
				+ "\tWhether to use BILOU encoding (by default it's BIO)\n"
				);
	}

}


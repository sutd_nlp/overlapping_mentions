if [ -z ${expDir+x} ]; then
    expDir="collapsed_model_ace2004"
fi
if [ -z ${outDir+x} ]; then
    outDir="evaluation_result"
fi
if [ -z ${l2Opts+x} ]; then
    l2Opts=(0.0 0.001 0.01 0.1 1.0)
fi
if [ -z ${algo+x} ]; then
    echo "Please specify the algorithm:"
    echo "-MENTION_HYPERGRAPH"
    echo "-MULTIGRAPH"
    echo "-MULTILINEAR"
    echo "-PARALINEAR"
    echo "-LINEAR_CRF"
    exit 1
fi
if [ -z ${trainPath+x} ]; then
    echo "Please specify training data path with trainPath=<path> before the command"
    exit 1
fi
if [ -z ${testPath+x} ]; then
    echo "Please specify test data path with testPath=<path> before the command"
    exit 1
fi
if [ $# -ge 1 ]; then
    data=$1
    scorer="get_scores_${data}.py"
else
    scorer="get_scores.py"
fi
python3 ${scorer} "${outDir}" "${expDir}" "${algo}" "${trainPath}" "${testPath}" "${l2Opts[@]}"

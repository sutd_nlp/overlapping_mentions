if [ -z ${outDir+x} ]; then
    outDir="evaluation_result"
fi
echo -n "Linear CRF (single chain)"
expDir="linear_model_ace2004" outDir="${outDir}" algo="LINEAR_CRF" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
expDir="linear_model_ace2005" outDir="${outDir}" algo="LINEAR_CRF" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
echo
echo -n "Linear CRF (multi chains)"
expDir="paralinear_model_ace2004.nooverlap" outDir="${outDir}" algo="PARALINEAR" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
expDir="paralinear_model_ace2005.nooverlap" outDir="${outDir}" algo="PARALINEAR" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
echo
echo -n "Tang et al. (2013)"
expDir="paralinear_model_ace2004.fixed.old" outDir="${outDir}" algo="PARALINEAR" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
expDir="paralinear_model_ace2005.fixed.old" outDir="${outDir}" algo="PARALINEAR" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
echo
echo -n "Mention Hypergraph"
expDir="hypergraph_model_ace2004" outDir="${outDir}" algo="MENTION_HYPERGRAPH" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
expDir="hypergraph_model_ace2005" outDir="${outDir}" algo="MENTION_HYPERGRAPH" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
echo
echo -n "State-based MS"
expDir="multilinear_model_ace2004" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
expDir="multilinear_model_ace2005" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
echo
echo -n "Edge-based MS"
expDir="multigraph_model_ace2004" outDir="${outDir}" algo="MULTIGRAPH" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
expDir="multigraph_model_ace2005" outDir="${outDir}" algo="MULTIGRAPH" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
#echo
#echo -n "Multilinear No Transition"
#expDir="multilinear_notrans_model_ace2004" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
#expDir="multilinear_notrans_model_ace2005" outDir="${outDir}" algo="MULTILINEAR" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
#echo
#echo -n "Paralinear"
#expDir="paralinear_model_ace2004" outDir="${outDir}" algo="PARALINEAR" trainPath="data/ACE2004/train.data" testPath="data/ACE2004/test.data" bash get_results.bash
#expDir="paralinear_model_ace2005" outDir="${outDir}" algo="PARALINEAR" trainPath="data/ACE2005/train.data" testPath="data/ACE2005/test.data" bash get_results.bash
